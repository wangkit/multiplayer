class AppConfig {
  static const listAnimationDuration = const Duration(milliseconds: 375);
  static const slideAnimationVerticalOffset = 50.0;
}