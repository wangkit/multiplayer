import 'dart:io';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/page_route/global_route.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/values/pref_key.dart';
import 'apps/splash/splash_page.dart';

class StoryTellerRoot extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    ThemeData _getTheme() {
      return ThemeData(
        appBarTheme: AppBarTheme(
          color: appBgColor,
        ),
        scaffoldBackgroundColor: appBgColor,
        backgroundColor: appBgColor,
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: appBgColor,
          elevation: 0
        ),
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: appIconColor,
          selectionHandleColor: appIconColor,
        ),
        primarySwatch: CustomWhite,
        floatingActionButtonTheme: FloatingActionButtonThemeData(
          backgroundColor: appIconColor,
        ),
        toggleableActiveColor: appIconColor,
        fontFamily: GoogleFonts.poppins().fontFamily,
      );
    }

    return WillPopScope(
      onWillPop: () async {
        if (kIsWeb) {
          getRoute.truePop();
          return false;
        }
        return true;
      },
      child: GestureDetector(
        onTap: () {
          utils.quitFocus();
        },
        child: RefreshConfiguration(
          headerBuilder: () => WaterDropMaterialHeader(
            color: mainColor,
            backgroundColor: littleGrey,
          ),
          footerBuilder: () => CustomFooter(
            builder: (BuildContext context, LoadStatus status) {
              switch (status) {
                case LoadStatus.loading:
                  return Center(
                    child: Padding(
                      padding: EdgeInsets.only(top: 20.0),
                      child: CupertinoActivityIndicator(),
                    ),
                  );
                default:
                  return Container();
              }
            },
          ),
          footerTriggerDistance: 80.0,
          headerTriggerDistance: 40.0,
          springDescription: SpringDescription(stiffness: 170, damping: 16, mass: 1.9),
          maxOverScrollExtent : 100,
          maxUnderScrollExtent: 100,
          enableScrollWhenRefreshCompleted: true,
          enableLoadingWhenFailed : true,
          hideFooterWhenNotFull: false,
          enableBallisticLoad: true,
          child: FeatureDiscovery(
            child: GetMaterialApp(
              navigatorKey: GlobalVariable.navState,
              debugShowCheckedModeBanner: false,
              title: myAppName,
              darkTheme: _getTheme(),
              theme: _getTheme(),
              home: SplashPage(),
            ),
          ),
        ),
      ),
    );
  }
}

