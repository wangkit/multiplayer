import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/text.dart';
import 'binary_dialog.dart';

void getBlockDialog(UserProfile _thisUser) {

  /// We only use
  /// 1. id
  /// 2. username
  /// of the given parameter

  getBinaryDialog("Block ${_thisUser.username}", "Are you sure you want to block ${_thisUser.username}?", () async {
    utils.loadToast();
    await apiManager.block(_thisUser);
    getRoute.pop();
    getRoute.pop();
  }, positiveColor: errorColor, positiveText: TextData.block);
}