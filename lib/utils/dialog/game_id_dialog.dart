import 'dart:io';

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:storyteller/configs/dialog_configs.dart';
import 'package:soy_common/utils/string_extensions.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/widgets/button/dialog_negative_button.dart';
import 'package:storyteller/widgets/button/dialog_positive_button.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/dialog/custom_alert_dialog.dart';
import 'package:storyteller/widgets/textfield/generic_textfield.dart';
import 'package:storyteller/widgets/webview/custom_web_view.dart';

Future<void> getUpdateGameIdDialog(String targetPlatform) async {

  String _getDefaultId() {
    String _default = "";
    switch (targetPlatform) {
      case UserCollection.steamFriendCode:
        return qp.steamFriendCode!;
      case UserCollection.psn:
        return qp.psn!;
      case UserCollection.xboxGamerTag:
        return qp.xboxGamerTag!;
      case UserCollection.nintendoFriendCode:
        return qp.nintendoFriendCode!;
    }
    return _default;
  }

  TextEditingController _controller = TextEditingController(text: _getDefaultId());
  GlobalKey<FormState> _formKey = GlobalKey();
  String targetName = "";

  switch (targetPlatform) {
    case UserCollection.steamFriendCode:
      targetName = "steam friend code";
      break;
    case UserCollection.psn:
      targetName = "playstation online ID";
      break;
    case UserCollection.xboxGamerTag:
      targetName = "gamertag";
      break;
    case UserCollection.nintendoFriendCode:
      targetName = "friend code";
      break;
  }

  await showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(
      isBarrierDismissible: true,
    ),
    builder: (BuildContext context) {
      return CustomAlertDialog(
        title: Text(
          targetName.titleCase,
          textAlign: TextAlign.center,
          style: DialogConfig.dialogTitleStyle,
        ),
        shape: DialogConfig.dialogShape(),
        content: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "What is your $targetName?",
                  style: DialogConfig.dialogMessageStyle,
                ),
                SizedBox(
                  height: 10,
                ),
                GenericTextField(
                  controller: _controller,
                  hint: targetName.titleCase,
                  textCapitalization: TextCapitalization.none,
                  inputFormatter: targetPlatform == UserCollection.nintendoFriendCode || targetPlatform == UserCollection.steamFriendCode ? [
                    FilteringTextInputFormatter.digitsOnly,
                  ] : null,
                  textInputType: targetPlatform == UserCollection.nintendoFriendCode || targetPlatform == UserCollection.steamFriendCode ? TextInputType.number : null,
                  validation: (String? current) {
                    if (current!.isEmpty) {
                      return null;
                    }
                    if ((UserCollection.psn == targetPlatform || UserCollection.steamFriendCode == targetPlatform || targetPlatform == UserCollection.nintendoFriendCode) && current.contains(" ")) {
                      return TextData.noSpacesAllowedUseUnderscore;
                    }
                    return null;
                  },
                ),
                TextButton(
                  onPressed: () {
                    String _helpLink = "";
                    switch (targetPlatform) {
                      case UserCollection.steamFriendCode:
                        _helpLink = steamProfileLink;
                        break;
                      case UserCollection.psn:
                        _helpLink = psnIdHelpLink;
                        break;
                      case UserCollection.xboxGamerTag:
                        _helpLink = xboxHelpLink;
                        break;
                      case UserCollection.nintendoFriendCode:
                        _helpLink = nintendoHelpLink;
                        break;
                    }
                    getRoute.navigateTo(CustomWebView(link: _helpLink));
                  },
                  child: Text(
                    "More about $targetName",
                    style: TextStyle(
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
                Platform.isIOS && targetPlatform == UserCollection.psn ?  Text(
                  TextData.safariBug,
                  style: TextStyle(
                    color: unfocusedColor,
                    fontSize: 15.0
                  ),
                ) : CommonWidgets.emptyBox(),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          DialogNegativeButton(
            buttonCancelText: TextData.cancel,
          ),
          DialogPositiveButton(
            onOkTap: () async {
              if (_formKey.currentState!.validate()) {
                String _newValue = _controller.text.trim();
                switch (targetPlatform) {
                  case UserCollection.steamFriendCode:
                    qp.steamFriendCode = _newValue;
                    break;
                  case UserCollection.psn:
                    qp.psn = _newValue;
                    break;
                  case UserCollection.xboxGamerTag:
                    qp.xboxGamerTag = _newValue;
                    break;
                  case UserCollection.nintendoFriendCode:
                    qp.nintendoFriendCode = _newValue;
                    break;
                }
                getRoute.pop();
                await apiManager.updateGameId(targetPlatform, _newValue);

              }
            },
            buttonOkText: TextData.submit,
          ),
        ],
      );
    },
  );
}