import 'package:animations/animations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:storyteller/configs/dialog_configs.dart';

import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/widgets/button/dialog_negative_button.dart';
import 'package:storyteller/widgets/button/dialog_positive_button.dart';
import 'package:storyteller/widgets/dialog/custom_alert_dialog.dart';
import 'package:storyteller/widgets/textfield/generic_textfield.dart';

Future<dynamic> getUsernameDialog(String presetUsername) async {

  TextEditingController _controller = TextEditingController(
    text: presetUsername.toLowerCase().replaceAll(" ", ""),
  );
  GlobalKey<FormState> _formKey = GlobalKey();
  bool _submitted = false;

  await showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(
      isBarrierDismissible: false,
    ),
    builder: (BuildContext context) {
      return CustomAlertDialog(
        title: Text(
          TextData.username,
          textAlign: TextAlign.center,
          style: DialogConfig.dialogTitleStyle,
        ),
        shape: DialogConfig.dialogShape(),
        content: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "Pick a username. You might not change it afterwards.",
                  style: DialogConfig.dialogMessageStyle,
                ),
                SizedBox(
                  height: 10,
                ),
                GenericTextField(
                  controller: _controller,
                  borderRadius: commonRadius,
                  hint: TextData.username,
                  textCapitalization: TextCapitalization.none,
                  textInputType: TextInputType.text,
                  inputFormatter: [
                    FilteringTextInputFormatter.allow(RegExp("[a-z0-9]")),
                  ],
                  canSpace: false,
                ),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          DialogNegativeButton(
            onCancelTap: () {
              _controller.text = "";
              getRoute.pop();
            },
          ),
          DialogPositiveButton(
            onOkTap: () async {
              utils.closeKeyboard();
              if (_formKey.currentState!.validate()) {
                utils.loadToast();
                QuerySnapshot _usernameResult = await firestore.collection(FirestoreCollections.users).where(UserCollection.username, isEqualTo: _controller.text.trim()).get();
                getRoute.pop();
                if (_usernameResult.size > 0) {
                  utils.toast(TextData.duplicateUsername, isWarning: true);
                } else {
                  _submitted = true;
                  getRoute.pop();
                }
              }
            },
            buttonOkText: TextData.submit,
          ),
        ],
      );
    },
  );
  return [_controller.text.trim(), _submitted];
}