import 'dart:io';
import 'package:animate_do/animate_do.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:lottie/lottie.dart';
import 'package:storyteller/apps/add_game/platform_page.dart';
import 'package:storyteller/apps/login/login_page.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/status/status_code.dart';
import 'package:storyteller/resources/values/social_key.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/dialog/username_dialog.dart';
import 'package:storyteller/utils/extensions/string_extensions.dart';
import 'package:storyteller/widgets/button/custom_argon_button.dart';
import 'package:storyteller/widgets/button/long_elevated_button.dart';
import 'package:storyteller/widgets/textfield/email_textfield.dart';
import 'package:storyteller/widgets/textfield/generic_textfield.dart';
import 'package:storyteller/widgets/textfield/password_textfield.dart';

void getSignEmailBottomSheet() {

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nicknameController = TextEditingController();
  FocusNode nickNode = FocusNode();
  FocusNode emailNode = FocusNode();
  FocusNode passwordNode = FocusNode();
  GlobalKey<FormState> formKey = GlobalKey();

  Future<void> _createAccount() async {
    /// Register
    if (formKey.currentState!.validate()) {
      utils.closeKeyboard();
      await apiManager.createAccount(nicknameController.text.trim(), emailController.text.replaceAll(" ", ""), passwordController.text.replaceAll(" ", ""));
    }
  }

  showModalBottomSheet(
    backgroundColor: appBgColor,
    isScrollControlled: true,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topRight: Radius.circular(commonRadius),
        topLeft: Radius.circular(commonRadius),
      ),
    ),
    context: getRoute.getContext(),
    builder: (BuildContext ctx) {
      return SafeArea(
        child: FadeIn(
          child: SingleChildScrollView(
            child: Form(
              key: formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 12.0, top: 12.0),
                        child: IconButton(
                          onPressed: () {
                            getRoute.pop();
                          },
                          icon: Icon(
                            Icons.clear,
                            color: unfocusedColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Image.asset(
                    "assets/icon/queue.png",
                    width: 80,
                    height: 80,
                  ),
                  SizedBox(height: 20,),
                  Text(
                    "Sign up now to join this big party!",
                  ),
                  SizedBox(height: 20,),
                  EmailTextField(
                    controller: emailController,
                    focusNode: emailNode,
                    nextFocusNode: nickNode,
                    borderRadius: commonRadius,
                  ),
                  SizedBox(height: 5,),
                  GenericTextField(
                    controller: nicknameController,
                    focusNode: nickNode,
                    nextFocusNode: passwordNode,
                    borderRadius: commonRadius,
                    hint: TextData.username,
                    textCapitalization: TextCapitalization.none,
                    textInputType: TextInputType.text,
                    inputFormatter: [
                      FilteringTextInputFormatter.allow(RegExp("[a-z0-9]")),
                    ],
                    canSpace: false,
                  ),
                  SizedBox(height: 5,),
                  PasswordTextField(
                    controller: passwordController,
                    focusNode: passwordNode,
                    nextFocusNode: emailNode,
                    borderRadius: commonRadius,
                  ),
                  SizedBox(height: 20,),
                  LongElevatedButton(
                    onPressed: () async {
                      await _createAccount();
                    },
                    title: TextData.create,
                  ),
                  SizedBox(height: 20,),
                  GestureDetector(
                    onTap: () {
                      getRoute.pop();
                      getRoute.navigateTo(LoginPage());
                    },
                    child: Text(
                      TextData.signInInstead,
                      style: TextStyle(
                        color: Colors.blue,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),
                ],
              ),
            ),
          ),
        ),
      );
    },
  );
}