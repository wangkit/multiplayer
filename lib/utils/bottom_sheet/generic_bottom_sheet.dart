import 'package:flutter/material.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';

void getGenericBottomSheet(List<Widget> listOfWidget, {bool needTopIndicator = true, Function? onCompleted, bool needOverlay = true, bool fullScreen = false}) {

  List<Widget> columnChildren;
  if (needTopIndicator) {
    columnChildren = [CommonWidgets.bottomSheetPullIndicator()] + listOfWidget;
  } else {
    columnChildren = listOfWidget;
  }

  showModalBottomSheet(
    barrierColor: needOverlay ? null : Colors.white.withOpacity(0),
    backgroundColor: appBgColor,
    isScrollControlled: fullScreen,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(topLeft: Radius.circular(commonRadius), topRight: Radius.circular(commonRadius)),
    ),
    context: getRoute.getContext(),
    builder: (BuildContext ctx) {
      return SafeArea(
        child:  SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: columnChildren,
          ),
        ),
      );
    },
  ).whenComplete(() {
    if (onCompleted != null) {
      onCompleted();
    }
  });
}