import 'dart:io';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:storyteller/apps/login/login_page.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/textfield/email_textfield.dart';
import 'package:storyteller/widgets/textfield/generic_textfield.dart';
import 'package:storyteller/widgets/textfield/password_textfield.dart';
import 'package:storyteller/constants/const.dart';

void getSignUpRoutingSheet() {

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nicknameController = TextEditingController();
  FocusNode nickNode = FocusNode();
  FocusNode emailNode = FocusNode();
  FocusNode passwordNode = FocusNode();
  GlobalKey<FormState> formKey = GlobalKey();
  EdgeInsets _padding = EdgeInsets.symmetric(horizontal: 30, vertical: 13);
  ShapeBorder _shape = RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(commonRadius)
  );

  Future<void> _createAccount() async {
    /// Register
    if (formKey.currentState!.validate()) {
      utils.closeKeyboard();
      await apiManager.createAccount(nicknameController.text.trim(), emailController.text.replaceAll(" ", ""), passwordController.text.replaceAll(" ", ""));
    }
  }

  showModalBottomSheet(
    backgroundColor: appBgColor,
    isScrollControlled: true,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topRight: Radius.circular(commonRadius),
        topLeft: Radius.circular(commonRadius),
      ),
    ),
    context: getRoute.getContext(),
    builder: (BuildContext ctx) {
      return SafeArea(
        child: FadeIn(
          child: SingleChildScrollView(
            child: Form(
              key: formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 20,),
                  Text(
                    "Account",
                    style: Theme.of(getRoute.getContext()).textTheme.headline6,
                  ),
                  SizedBox(height: 10,),
                  EmailTextField(
                    controller: emailController,
                    focusNode: emailNode,
                    nextFocusNode: nickNode,
                    borderRadius: commonRadius,
                  ),
                  SizedBox(height: 5,),
                  GenericTextField(
                    controller: nicknameController,
                    focusNode: nickNode,
                    nextFocusNode: passwordNode,
                    borderRadius: commonRadius,
                    hint: TextData.username,
                    textCapitalization: TextCapitalization.none,
                    textInputType: TextInputType.text,
                    inputFormatter: [
                      FilteringTextInputFormatter.allow(RegExp("[a-zA-Z0-9]")),
                    ],
                    canSpace: false,
                  ),
                  SizedBox(height: 5,),
                  PasswordTextField(
                    controller: passwordController,
                    focusNode: passwordNode,
                    nextFocusNode: emailNode,
                    borderRadius: commonRadius,
                  ),
                  SizedBox(height: 10,),
                  SignInButtonBuilder(
                    text: TextData.createAccount,
                    icon: Icons.create_outlined,
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 11),
                    shape: _shape,
                    textColor: Colors.white,
                    backgroundColor: appIconColor,
                    onPressed: () async {
                      await _createAccount();
                    },
                  ),
                  Container(
                    height: 50,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Expanded(
                          flex: 2,
                          child: Divider(
                            height: 1,
                            thickness: 1,
                            indent: 20,
                            endIndent: 20,
                            color: unfocusedColor,
                          ),
                        ),
                        Flexible(
                          child: Text(
                            TextData.or.toUpperCase(),
                            style: Theme.of(getRoute.getContext()).textTheme.subtitle1!.merge(
                              TextStyle(
                                color: unfocusedColor,
                              )
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Divider(
                            height: 1,
                            thickness: 1,
                            indent: 20,
                            endIndent: 20,
                            color: unfocusedColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Platform.isIOS ? SignInButton(
                    Buttons.AppleDark,
                    padding: _padding,
                    shape: _shape,
                    onPressed: () async {
                      getRoute.pop();
                      await apiManager.signInUpWithApple();
                    },
                  ) : CommonWidgets.emptyBox(),
                  SizedBox(height: Platform.isIOS ? 10 : 0, width: 0,),
                  SignInButtonBuilder(
                    text: 'Sign in with Google',
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 4.4),
                    shape: _shape,
                    textColor: Color.fromRGBO(0, 0, 0, 0.54),
                    backgroundColor: Color(0xFFFFFFFF),
                    image: ClipRRect(
                      child: Image(
                        image: AssetImage(
                          "assets/logos/google_light.png",
                          package: 'flutter_signin_button',
                        ),
                        height: 36.0,
                      ),
                    ),
                    onPressed: () async {
                      getRoute.pop();
                      await apiManager.signInUpByGoogle();
                    },
                  ),
                  SizedBox(height: 10,),
                  SignInButton(
                    Buttons.FacebookNew,
                    padding: _padding,
                    shape: _shape,
                    onPressed: () async {
                      await apiManager.signInUpByFacebook();
                    },
                  ),
                  SizedBox(height: 10,),
                  SignInButtonBuilder(
                    text: 'Sign in with $appName',
                    icon: Icons.email,
                    padding: _padding,
                    shape: _shape,
                    onPressed: () {
                      getRoute.pop();
                      getRoute.navigateTo(LoginPage());
                    },
                    backgroundColor: Colors.blueGrey[700]!,
                  ),
                  SizedBox(height: 12,),
                  GestureDetector(
                    onTap: () {
                      getRoute.pop();
                    },
                    child: Text(
                      TextData.cancel,
                    ),
                  ),
                  SizedBox(height: 10,),
                ],
              ),
            ),
          ),
        ),
      );
    },
  );
}