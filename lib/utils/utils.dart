import 'dart:collection';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:animate_do/animate_do.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:device_calendar/device_calendar.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';
import 'package:storyteller/apps/games/game_details_page.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/color/hex.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/status/gender_status.dart';
import 'package:storyteller/resources/status/preferred_relationship_status.dart';
import 'package:storyteller/resources/values/analysis.dart';
import 'package:storyteller/resources/values/data_key.dart';
import 'package:storyteller/resources/values/pref_key.dart';
import 'package:storyteller/resources/values/remote_config_key.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_with_email.dart';
import 'package:storyteller/utils/dialog/block_dialog.dart';
import 'package:storyteller/utils/dialog/report_dialog.dart';
import 'package:storyteller/utils/extensions/string_extensions.dart';
import 'package:storyteller/utils/steam/steam_utils.dart';
import 'package:storyteller/widgets/button/bottom_sheet_button.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/webview/custom_web_view.dart';
import 'package:tinycolor/tinycolor.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:uuid/uuid.dart';

import 'bottom_sheet/sign_up_routing_bottom_sheet.dart';
import 'dialog/binary_dialog.dart';

class Utils {

  /// Quit focus on text field and close keyboard
  void quitFocus({bool createNewFocusToRemoveOld = false}) {
    FocusScopeNode currentFocus = FocusScope.of(getRoute.getContext());
    if (!currentFocus.hasPrimaryFocus) {
      if (createNewFocusToRemoveOld) {
        currentFocus.requestFocus(FocusNode());
      } else {
        currentFocus.unfocus();
      }
    }
    closeKeyboard();
  }

  void logoutClearData() {
    prefs!.remove(PrefKey.email);
    prefs!.remove(PrefKey.id);
    prefs!.remove(PrefKey.authPwKey);
    prefs!.remove(PrefKey.username);
    prefs!.remove(PrefKey.hasCalledFCMRegistration);
    qp = UserProfile(
      id: "",
      email: "",
      authPw: "",
      username: "",
      photoUrl: "",
    );
    firebaseAuth.signOut();
    firebaseUser = null;
  }

  String getProfileThumbnail(List<String> imageList) {
    if (imageList.isEmpty) return defaultProfilePictureLink;
    return imageList.first;
  }

  double getProfileMediaHeight() {
    return deviceHeight - kToolbarHeight - kToolbarHeight - (deviceHeight * 0.15);
  }

  fieldFocusChange(FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(getRoute.getContext()).requestFocus(nextFocus);
  }

  String getHexCodeByColor(Color color) {
    return color.value.toRadixString(16);
  }

  void closeApp() {
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  }

  Color getVisibleColor(String colorCode) {
    return getColorByHex(colorCode).withRed(getColorByHex(colorCode).red ~/ 2).withBlue(getColorByHex(colorCode).blue ~/ 2).withGreen(getColorByHex(colorCode).green ~/ 2);
  }

  /// Close the keyboard.
  void closeKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  /// Open the keyboard.
  void openKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.show');
  }

  bool isDarkColor(String color) {
    return ThemeData.estimateBrightnessForColor(utils.getColorByHex(color)) == Brightness.dark;
  }

  void postLongPress(Post post) {

    bool _isMyself = qp.id! == post.creatorId;

    getGenericBottomSheet([
      BottomSheetButton(
        onTap: () {
          utils.copyToClipboard(post.title);
          utils.toast(TextData.copiedSuccessfully);
        },
        iconData: Icons.copy_outlined,
        label: "Copy Title",
      ),
      BottomSheetButton(
        onTap: () {
          utils.copyToClipboard(post.content);
          utils.toast(TextData.copiedSuccessfully);
        },
        iconData: Icons.copy_outlined,
        label: "Copy Content",
      ),
      BottomSheetButton(
        onTap: () async {
          await utils.shareShortUrl("https://multiplayer.com/forum/?${PostCollection.postId}=${post.postId}");
        },
        iconData: Icons.ios_share_outlined,
        label: TextData.share,
      ),
      _isMyself && utils.hasLoggedIn() ? CommonWidgets.emptyBox() : BottomSheetButton(
        onTap: () {
          getReportDialog(UserProfile(
            id: post.creatorId,
            username: post.creatorUsername,
          ));
        },
        iconData: Icons.report_outlined,
        label: TextData.report,
      ),
      _isMyself && utils.hasLoggedIn() ? CommonWidgets.emptyBox() : BottomSheetButton(
        onTap: () {
          getBlockDialog(UserProfile(
            id: post.creatorId,
            username: post.creatorUsername,
          ));
        },
        iconData: Icons.block_outlined,
        label: TextData.block,
      ),
    ]);
  }

  Color getColorByHex(String? colorCode, {Color? nullColor}) {
    if (colorCode!.isNullOrEmpty) {
      return nullColor == null ? unfocusedColor : nullColor;
    } else {
      return HexColor(colorCode);
    }
  }

  IconData? getGamePlatformIcon(String target) {
    if (psPlatforms.contains(target)) {
      return MdiIcons.sonyPlaystation;
    } else if (xboxPlatforms.contains(target)) {
      return MdiIcons.microsoftXbox;
    } else if (nintendoPlatforms.contains(target)) {
      return MdiIcons.nintendoSwitch;
    }
    switch (target) {
      case ConfigCollection.steam:
        return MdiIcons.steam;
      case ConfigCollection.android:
        return MdiIcons.android;
      case ConfigCollection.iOS:
        return MdiIcons.appleIos;
      case ConfigCollection.pc:
        return MdiIcons.laptop;
      case ConfigCollection.mobile:
        return Icons.phone_android_rounded;
      case ConfigCollection.playstation:
        return MdiIcons.sonyPlaystation;
      case ConfigCollection.nintendo:
        return MdiIcons.nintendoSwitch;
      case ConfigCollection.xbox:
        return MdiIcons.microsoftXbox;
    }
    return null;
  }

  DynamicLinkParameters _getDynamicLink(String link) {
    return DynamicLinkParameters(
      uriPrefix: 'https://spinnerofyarns.page.link',
      link: Uri.parse(link),
      androidParameters: AndroidParameters(
        packageName: 'com.soy.storyteller',
        minimumVersion: 1,
      ),
      iosParameters: IosParameters(
        bundleId: 'com.soy.storyteller',
        minimumVersion: '1.0.0',
        appStoreId: '1562804586',
      ),
      socialMetaTagParameters: SocialMetaTagParameters(
        imageUrl: Uri.parse(remoteConfig!.getValue(RemoteConfigKey.socialImage).asString()),
        title: remoteConfig!.getValue(RemoteConfigKey.socialTitle).asString(),
        description: remoteConfig!.getValue(RemoteConfigKey.socialSubtitle).asString(),
      ),
    );
  }

  Future<void> shareShortUrl(String link) async {
    final ShortDynamicLink dynamicUrl = await _getDynamicLink(link).buildShortLink();
    await Share.share(dynamicUrl.shortUrl.toString());
  }

  void addFriendIntegration(String targetPlatform, String targetId) {
    switch (targetPlatform) {
      case ConfigCollection.nintendo:
        utils.toast(TextData.noNintendoSupport, isWarning: true);
        break;
      case ConfigCollection.playstation:
        /// No spaces for psn name
        /// Do not use in-app webview for playstation page since this page does not work on in-app webview
        if (Platform.isIOS) {
          getBinaryDialog(
            TextData.safari,
            TextData.safariBug,
            () {
              getRoute.pop();
              launchURL(Uri.parse(psnAddFriendBaseLink + targetId.replaceAll(" ", "")).toString());
            },
            positiveText: TextData.go,
          );
        } else {
          /// Android webview can work
          getRoute.navigateTo(CustomWebView(link: psnAddFriendBaseLink + targetId.replaceAll(" ", "")));
        }
        break;
      case ConfigCollection.xbox:
        /// A space is allowed is between gamertag, so just trim left right spaces
        getRoute.navigateTo(CustomWebView(link: Uri.parse(xboxAddFriendBaseLink + targetId.trim()).toString()));
        break;
      default:
        if (targetPlatform == ConfigCollection.steam) {
          /// Steam invitation link is already a complete link
          /// Calculate steam 64 id from 32 id
          getRoute.navigateTo(CustomWebView(link: Uri.parse(steamProfileLink).toString() + SteamUtils.from32To64(targetId)));
        }
    }
  }

  void copyToClipboard(String target) {
    Clipboard.setData(ClipboardData(text: target));
  }

  Future<void> launchURL(String url, {String? fallbackUrl, bool isHttp = true}) async {
    if (fallbackUrl == null) fallbackUrl = url;
    if (!url.contains("http") && !url.contains("mailto") && isHttp) {
      url = "https://" + url;
    }
    try {
      await canLaunch(url) ? launch(url, forceSafariVC: false) : launch(fallbackUrl, forceSafariVC: false);
    } on Exception catch(e) {
      toast(TextData.cantOpenUrl);
    }
  }

  String generatePassword({bool isWithLetters = true, bool isWithUppercase = true, bool isWithNumbers = true, bool isWithSpecial = false, double numberCharPassword = 14}) {

    //Define the allowed chars to use in the password
    String _lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
    String _upperCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    String _numbers = "0123456789";
    String _special = "@#=+!£\$%&?[](){}";

    //Create the empty string that will contain the allowed chars
    String _allowedChars = "";

    //Put chars on the allowed ones based on the input values
    _allowedChars += (isWithLetters ? _lowerCaseLetters : '');
    _allowedChars += (isWithUppercase ? _upperCaseLetters : '');
    _allowedChars += (isWithNumbers ? _numbers : '');
    _allowedChars += (isWithSpecial ? _special : '');

    int i = 0;
    String _result = "";

    //Create password
    while (i < numberCharPassword.round()) {
      //Get random int
      int randomInt = Random.secure().nextInt(_allowedChars.length);
      //Get random char and append it to the password
      _result += _allowedChars[randomInt];
      i++;
    }

    return _result;
  }

  void addEmoji(TextEditingController controller, String emoji) {
    int offsetStart;
    int originalOffsetStart;
    String currentText;
    Runes sRunes;
    int emojiCount;
    var beforeInsertionText;
    var beforeInsertionRunes;
    int emojiLength = emoji.length;
    try {
      /// Sometimes, some weird emoji that can't be understood is passed into this function as an argument
      /// To filter these unwanted emojis, make sure the length of the emoji string is 2
      if (emojiLength == 2) {
        if (controller.selection.start < 0) {
          controller.text = controller.text + emoji;
        } else {
          /// Need to turn string into runes since emoji will crash app
          /// Calculate number of emoji in text before insertion point first
          /// then, since each emoji's length is 2 instead of 1 of normal character
          /// Deduct the offset by the number of emoji to get the correct offset of runes
          /// Emoji utf16 code are all larger than 125000, but i am not sure if there are any normal characters beyond 125000, so this might be a problem
          emojiCount = 0;
          offsetStart = controller.selection.start;
          originalOffsetStart = offsetStart;
          currentText = controller.text;
          sRunes = currentText.runes;
          beforeInsertionText = currentText.substring(0, offsetStart);
          beforeInsertionRunes = beforeInsertionText.runes;
          beforeInsertionRunes.forEach((int rune) {
            if (rune > 125000 && rune < 130000) {
              emojiCount++;
            }
          });
          offsetStart = offsetStart - emojiCount;
          controller.text = String.fromCharCodes(sRunes, 0, offsetStart)
              + emoji + String.fromCharCodes(sRunes, offsetStart, sRunes.length);
          if (Platform.isAndroid) {
            controller.value = controller.value.copyWith(
              text: controller.text,
              selection: TextSelection(baseOffset: originalOffsetStart + 2, extentOffset: originalOffsetStart + 2),
              composing: TextRange.empty,
            );
          } else {
            /// This delay is needed for ios to allow jumping of cursor in text field
            /// Any milliseconds smaller than 30 will cause unstable performance
            Future.delayed(Duration(milliseconds: 30), () {
              controller.value = controller.value.copyWith(
                text: controller.text,
                selection: TextSelection(baseOffset: originalOffsetStart + 2, extentOffset: originalOffsetStart + 2),
                composing: TextRange.empty,
              );
            });
          }
        }
      }
    } on Exception catch (e) {
      print("Add emoji error: $e");
    }
  }

  void emailToSpinnerOfYarns({String subject = "Contacting Spinner of Yarns", String? body}) async {
    if (Platform.isAndroid) {
      final emailUrl = body == null ? "mailto:$companyEmail?subject=$subject" : "mailto:$companyEmail?subject=$subject&body=$body";
      await launchURL(emailUrl);
    } else if (Platform.isIOS) {
      final emailUrl = Uri.encodeFull(body == null ? "mailto:$companyEmail?subject=$subject" : "mailto:$companyEmail?subject=$subject&body=$body");
      await launchURL(emailUrl);
    } else {
      toast(TextData.notSupportedOs, isWarning: true);
    }
  }

  Future<void> openFacebook(String fbId) async {
    var fallbackUrl = "https://www.facebook.com/$fbId";
    var fbProtocolUrl = Platform.isAndroid ? "fb.me/$fbId" : Platform.isIOS ? fallbackUrl : fallbackUrl;
    await launchURL(fbProtocolUrl, fallbackUrl: fallbackUrl);
  }

  Future<void> openTwitter(String twitterId) async {
    var fallbackUrl = "https://twitter.com/$twitterId";
    await launchURL(fallbackUrl, fallbackUrl: fallbackUrl);
  }

  Future<void> openYoutube() async {
    var fallbackUrl = "https://www.youtube.com/channel/UCdGAjAYzhEtB6wuawH0dvaA";
    await launchURL(fallbackUrl, fallbackUrl: fallbackUrl);
  }

  Future<void> launchCaller(BuildContext context, String phoneNumber) async {
    String url = "tel:$phoneNumber";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
//      toast(TextData.phoneDialFail, bgColor: warning, iconData: warningIcon);
    }
  }

  String generateTt() {
    return "";
  }

  String getRandomString() {
    const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    Random _rnd = Random();
    return String.fromCharCodes(Iterable.generate(102, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
  }

  Future<void> loadPref() async {
    hasCalledFCMRegistration = prefs?.getBool(PrefKey.hasCalledFCMRegistration) ?? false;
    addGameToCalendar = prefs?.getBool(PrefKey.addGameToCalendar) ?? true;
    isVideoStartMuted = prefs?.getBool(PrefKey.isVideoStartMuted) ?? false;
    qp.id = prefs?.getString(PrefKey.id) ?? "";
    qp.email = prefs?.getString(PrefKey.email) ?? "";
    qp.authPw = prefs?.getString(PrefKey.authPwKey) ?? "";
    qp.username = prefs?.getString(PrefKey.username) ?? "";
    if (qp.id!.isNullOrEmpty || qp.email!.isNullOrEmpty || qp.authPw!.isNullOrEmpty || qp.username!.isNullOrEmpty) {
      prefs?.remove(PrefKey.username);
      prefs?.remove(PrefKey.id);
      prefs?.remove(PrefKey.email);
      prefs?.remove(PrefKey.authPwKey);
      qp = UserProfile(
        id: "",
        email: "",
        authPw: "",
        username: "",
      );
    }
  }

  String shortStringForLongInt(int? value) {
    if (value == null) {
      return "0";
    }
    if (value < 0) {
      return "0";
    }
    if (value > 999) {
      var _formattedNumber = NumberFormat.compactCurrency(
        symbol: "",
        decimalDigits: 1,
      ).format(value);
      return _formattedNumber.toLowerCase();
    } else {
      return value.toString();
    }
  }

  String determineNeedS(int number, String text, {bool showNumber = true}) {
    if (number > 1) {
      if (showNumber) {
        return shortStringForLongInt(number) + " " + text + "s";
      } else {
        return text + "s";
      }
    } else {
      if (showNumber) {
        return shortStringForLongInt(number) + " " + text;
      } else {
        return text;
      }
    }
  }

  Future<void> deleteEvents(Game game) async {
    if (ourCalendar != null) {
      Uri uri = Uri.dataFromString("custom://${game.gameId}");
      final eventResults = await deviceCalendarPlugin.retrieveEvents(ourCalendar!.id, RetrieveEventsParams(
        startDate: game.startTime.subtract(Duration(days: 1)),
        endDate: game.startTime.add(Duration(days: 1)),
      ));
      for (Event event in eventResults.data) {
        if (event.url == uri) {
          await deviceCalendarPlugin.deleteEvent(ourCalendar!.id, event.eventId);
          break;
        }
      }
    }
  }

  Future<void> createEvents(Game game) async {
    if (ourCalendar != null) {
      print("create events");
      Uri uri = Uri.dataFromString("custom://${game.gameId}");
      final eventResults = await deviceCalendarPlugin.retrieveEvents(ourCalendar!.id, RetrieveEventsParams(
        startDate: game.startTime.subtract(Duration(days: 1)),
        endDate: game.startTime.add(Duration(days: 1)),
      ));
      bool _alreadyExist = false;
      for (Event event in eventResults.data) {
        if (event.url == uri) {
          _alreadyExist = true;
          break;
        }
      }
      if (!_alreadyExist) {
        Event _event =  Event(
          ourCalendar!.id,
          title: game.gameName,
          start: game.startTime,
          end: game.endTime,
          description: "Play ${game.gameName} on ${game.platformProduct}",
        );
        _event.url = uri;
        await deviceCalendarPlugin.createOrUpdateEvent(_event);
      }
    }
  }

  Future<bool> hasPermissions() async {
    var permissionsGranted = await deviceCalendarPlugin.hasPermissions();
    if (permissionsGranted.isSuccess && !permissionsGranted.data) {
      permissionsGranted = await deviceCalendarPlugin.requestPermissions();
      if (!permissionsGranted.isSuccess || !permissionsGranted.data) {
        return false;
      } else {
        return true;
      }
    } else {
      return permissionsGranted.data;
    }
  }

  Future<void> buildCalendars() async {

    if (await hasPermissions()) {
      final calendarsResult = await deviceCalendarPlugin.retrieveCalendars();
      bool _needBuildCalendars = true;
      for (Calendar calendar in calendarsResult.data) {
        if (calendar.name == MultiplayerCalendar.calendarName) {
          ourCalendar = calendar;
          _needBuildCalendars = false;
          break;
        }
      }
      if (_needBuildCalendars) {
        var result = await deviceCalendarPlugin.createCalendar(
          MultiplayerCalendar.calendarName,
          calendarColor: appIconColor,
          localAccountName: qp.username!,
        );
        final calendarsResult = await deviceCalendarPlugin.retrieveCalendars();
        for (Calendar calendar in calendarsResult.data) {
          if (calendar.name == MultiplayerCalendar.calendarName) {
            ourCalendar = calendar;
            break;
          }
        }
      }
    }
  }

  Future<File> uintToFile(Uint8List data) async {
    final tempDir = await getTemporaryDirectory();
    final _tempFile = await File('${tempDir.path}/${Uuid().v4()}.jpg').create();
    _tempFile.writeAsBytesSync(data);
    return _tempFile;
  }

  bool hasLoggedIn() {
    return qp.id != null && qp.id!.isNotEmpty && firebaseUser != null;
  }

  String getAppleEmail(String id) {
    return "${commonUtils.encodeString(id)}@apple.com";
  }

  Future<void> joinQuitGame(Game _thisGame, Function refreshThisPage, {bool showLoading = true, Function? onQuit}) async {

    void _quitGame(Game game, {bool isThisGame = true}) {
      apiManager.quitGame(game.gameId);
      if (isThisGame) {
        refreshThisPage();
      }
      if (onQuit != null) {
        onQuit();
      }
      utils.deleteEvents(game);
    }

    void _joinGame(Game game) {
      apiManager.joinGame(_thisGame.gameId);
      refreshThisPage();
      if (addGameToCalendar) {
        /// Add schedule, doesnt work on iOS
        utils.buildCalendars();
        utils.createEvents(_thisGame);
      }
    }
    
    if (!hasLoggedIn()) {
      getSignUpRoutingSheet();
      return;
    }

    if (_thisGame.startTime.isBefore(DateTime.now())) {
      utils.toast(TextData.gameAlreadyStarted, isWarning: true);
      return;
    }
    if (_thisGame.playerIds.contains(qp.id!)) {
      /// If already in game, then quit game
      _quitGame(_thisGame);
    } else if (_thisGame.maxPlayers > _thisGame.players.length) {
      if (showLoading) {
        utils.loadToast(msg: TextData.joining);
      }
      /// If game has vacancy, join game
      Game? _hasNoGame = await apiManager.hasNoGameInThisPeriodOrIsBlocked(_thisGame);
      if (showLoading) {
        getRoute.pop();
      }
      /// Will return null if no conflict and vice versa
      if (_hasNoGame == null) {
        /// Check if user has game in that same period
        _joinGame(_thisGame);
      } else {
        if (_hasNoGame.gameId.isEmpty) {
          utils.toast(TextData.youAreBlocked, isWarning: true);
          return;
        }
        if (_hasNoGame.startTime.isBefore(DateTime.now()) && _hasNoGame.endTime.isAfter(DateTime.now())) {
          utils.toast(TextData.youAreCurrentlyPlayingAnotherGame, isWarning: true);
          return;
        }
        /// Proceed to ask if want quit game and join this game if user has scheduled a game that conflicts with this
        getBinaryDialog(TextData.scheduleConflict, "Are you sure you want to join this game instead of ${_hasNoGame.gameName} on ${timeUtils.getYearMonthDay(timeUtils.getParsedDateTime(_hasNoGame.startTime))}?", () {
          /// This has to change the status another game bubble, if necessary, and this cant be done in this widget of this game
          getRoute.pop();
          apiManager.logAnalysis(Analysis.conflictedQuit);
          _joinGame(_thisGame);
          _quitGame(_hasNoGame, isThisGame: false);
        });
      }
    } else {
      utils.toast(TextData.gameFull, isWarning: true);
    }
  }

  Color convertStrToColor(String text, {bool needOpacity = true, bool isDarken = false}){
    var hash = 0;
    for (var i = 0; i < text.length; i++) {
      hash = text.codeUnitAt(i)+ ((hash << 5) - hash);
    }
    final finalHash = hash.abs() % (256*256*256);
    final red = ((finalHash & 0xFF0000) >> 16);
    final blue = ((finalHash & 0xFF00) >> 8);
    final green = ((finalHash & 0xFF));
    final color = Color.fromRGBO(red, green, blue, 1);
    if (isDarken) {
      if (TinyColor(color).darken(35).color == Colors.black) {
        return TinyColor(color).color.withOpacity(needOpacity ? 0.5 : 1);
      }
      return TinyColor(color).darken(35).color.withOpacity(needOpacity ? 0.8 : 1);
    } else {
      if (TinyColor(color).lighten(35).color == Colors.white) {
        return TinyColor(color).color.withOpacity(needOpacity ? 0.5 : 1);
      }
      return TinyColor(color).lighten(35).color.withOpacity(needOpacity ? 0.8 : 1);
    }
  }

  loadToast({String? msg}) {
    if (kIsWeb) {

    } else {
      Flushbar(
        message: msg != null ? msg : "Loading...",
        flushbarPosition: FlushbarPosition.TOP,
        showProgressIndicator: true,
        blockBackgroundInteraction: true,
        routeBlur: 3,
        isDismissible: false,
        progressIndicatorBackgroundColor: unfocusedColor,
      )..show(getRoute.getContext());
    }
  }

  toast(String msg, {int duration = 3, String? title, bool isDismissible = true, bool isWarning = false, bool blockBackgroundInteraction = false}) {
    if (kIsWeb) {
      Get.rawSnackbar(
        message: msg,
        leftBarIndicatorColor: isWarning ? warning : success,
        isDismissible: isDismissible,
        reverseAnimationCurve: Curves.fastOutSlowIn,
      );
    } else {
      Flushbar(
        title: title,
        message: msg,
        blockBackgroundInteraction: blockBackgroundInteraction,
        routeBlur: blockBackgroundInteraction ? 3 : 0,
        duration: Duration(seconds: duration),
        reverseAnimationCurve: Curves.fastOutSlowIn,
        flushbarStyle: FlushbarStyle.FLOATING,
        margin: EdgeInsets.only(bottom: 10, left: 5, right: 5),
        borderRadius: BorderRadius.circular(8),
        flushbarPosition: FlushbarPosition.BOTTOM,
        leftBarIndicatorColor: isWarning ? warning : success,
        isDismissible: isDismissible,
      )..show(getRoute.getContext());
    }
  }

  saveStr(String key, String value) {
    prefs!.setString(key, value);
  }

  saveStrList(String key, List<String> value) {
    prefs!.setStringList(key, value);
  }

  saveInt(String key, int value) {
    prefs!.setInt(key, value);
  }

  saveBoo(String key, bool value) {
    prefs!.setBool(key, value);
  }

  saveDouble(String key, double value) {
    prefs!.setDouble(key, value);
  }
}
