import 'package:storyteller/constants/const.dart';

class SteamUtils {

  static String from32To64(String id32) {
    if (int.tryParse(id32) == null) {
      return id32;
    }
    return '765' + (int.parse(id32) + steamCalculateFactor).toString();
  }

  static String from64To32(String id64) {
    if (int.tryParse(id64) == null) {
      return id64;
    }
    return (int.parse(id64.substring(3)) - steamCalculateFactor).toString();
  }
}