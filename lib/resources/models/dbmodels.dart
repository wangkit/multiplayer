class UserCollection {
  static const String username = "username";
  static const String id = "id";
  static const String firebaseUserId = "firebaseUserId";
  static const String password = "password";
  static const String authPw = "authPw";
  static const String createdAt = "createdAt";
  static const String email = "email";
  static const String deviceId = "deviceId";
  static const String deviceModel = "deviceModel";
  static const String deviceVersion = "deviceVersion";
  static const String photoUrl = "photoUrl";
  static const String xboxGamerTag = "xboxGamerTag";
  static const String steamFriendCode = "steamFriendCode";
  static const String psn = "psn";
  static const String nintendoFriendCode = "nintendoFriendCode";
  static const String social = "social";
}

class BlockCollection {
  static const String id = "id";
  static const String blockId = "blockId";
  static const String blockedId = "blockedId";
  static const String createdAt = "createdAt";
}

class ReportCollection {
  static const String id = "id";
  static const String reportId = "reportId";
  static const String reportedId = "reportedId";
  static const String reason = "reason";
  static const String createdAt = "createdAt";
}

class ChatCollection {
  static const String gameId = "gameId";
  static const String senderId = "senderId";
  static const String senderName = "senderName";
  static const String message = "message";
  static const String messageId = "messageId";
  static const String createdAt = "createdAt";
}

class FriendsCollection {
  /// To be done
  static const String friendId = "friendId";
}

class MessagesCollection {
  static const String gameId = "gameId";
  static const String users = "users";
  static const String endTime = "endTime";
  static const String createdAt = "createdAt";
}

class FcmTokenCollection {
  static const String email = "email";
  static const String id = "id";
  static const String token = "token";
  static const String createdAt = "createdAt";
  static const String updatedAt = "updatedAt";
}

class ReplyCollection {
  static const postId = "postId";
  static const content = "content";
  static const firebaseUserId = "firebaseUserId";
  static const replyId = "replyId";
  static const creatorId = "creatorId";
  static const creatorUsername = "creatorUsername";
  static const createdAt = "createdAt";
}

class PostCollection {
  static const postId = "postId";
  static const content = "content";
  static const firebaseUserId = "firebaseUserId";
  static const replyCount = "replyCount";
  static const creatorId = "creatorId";
  static const dislikeIds = "dislikeIds";
  static const likeIds = "likeIds";
  static const title = "title";
  static const creatorUsername = "creatorUsername";
  static const createdAt = "createdAt";
}

class PostLikesCollection {
  static const firebaseUserId = "firebaseUserId";
  static const postId = "postId";
  static const userId = "userId";
  static const createdAt = "createdAt";
  static const isLiked = "isLiked";
}

class ConfigCollection {
  static const gamingPlatforms = "gaming_platforms";
  static const pcPlatforms = "pc_platforms";
  static const mobilePlatforms = "mobile_platforms";
  static const xboxPlatforms = "xbox_platforms";
  static const psPlatforms = "ps_platforms";
  static const nintendoPlatforms = "nintendo_platforms";
  static const nintendo = "Nintendo";
  static const steam = "Steam";
  static const pc = "PC";
  static const mobile = "Mobile";
  static const android = "Android";
  static const iOS = "iOS";
  static const xbox = "Xbox";
  static const playstation = "Playstation";
}

class NewsCollection {
  static const name = "name";
  static const description = "description";
  static const date = "date";
  static const link = "link";
  static const createdAt = "createdAt";
}

class PopularCollection {
  static const gameCover = "gameCover";
  static const gameVideo = "gameVideo";
  static const gameSummary = "gameSummary";
  static const gameLink = "gameLink";
  static const gameTheme = "gameTheme";
  static const gameGenre = "gameGenre";
  static const gamePlatforms = "gamePlatforms";
  static const gameName = "gameName";
  static const releaseDate = "releaseDate";
  static const createdAt = "createdAt";

}
class GameCollection {
  static const int limit = 30;
  static const String gameSummary = "gameSummary";
  static const String firebaseUserId = "firebaseUserId";
  static const String gameId = "gameId";
  static const String creatorId = "creatorId";
  static const String creatorUsername = "creatorUsername";
  static const String createdAt = "createdAt";
  static const String gameCover = "gameCover";
  static const String gameName = "gameName";
  static const String maxPlayers = "maxPlayers";
  static const String players = "players";
  static const String playerIds = "playerIds";
  static const String gamePlatform = "gamePlatform";
  static const String platformProduct = "platformProduct";
  static const String startTime = "startTime";
  static const String endTime = "endTime";
  static const String gameVideo = "gameVideo";
  static const String gameGenre = "gameGenre";
  static const String gameTheme = "gameTheme";
}