import 'package:storyteller/constants/const.dart';
import 'dbmodels.dart';

class MultiplayerCalendar {
  static const calendarName = myAppName;
}

class FirestoreCollections {
  static const users = "users";
  static const games = "games";
  static const fcmToken = "fcm_token";
  static const messages = "messages";
  static const chats = "chats";
  static const report = "reports";
  static const config = "config";
  static const news = "news";
  static const popularGames = "popular_games";
  static const friends = "friends";
  static const block = "block";
  static const gameDetails = "game_details";
  static const posts = "posts";
  static const postLikes = "post_likes";
  static const replies = "replies";
}

class Reply {
  String replyId;
  String postId;
  String content;
  String creatorId;
  String creatorUsername;
  DateTime createdAt;

  Reply({
    required this.postId,
    required this.content,
    required this.creatorId,
    required this.creatorUsername,
    required this.replyId,
    required this.createdAt,
  });

  static Reply fromMapToReply(Map map) {
    return Reply(
      postId: map[ReplyCollection.postId],
      content: map[ReplyCollection.content],
      creatorId: map[ReplyCollection.creatorId],
      creatorUsername: map[ReplyCollection.creatorUsername],
      replyId: map[ReplyCollection.replyId],
      createdAt: timeUtils.getDateTimeFromTimeStamp(map[ReplyCollection.createdAt]).toLocal(),
    );
  }
}

class Post {
  String postId;
  String content;
  String title;
  String creatorId;
  String creatorUsername;
  List<String> likeIds;
  List<String> dislikeIds;
  int replyCount;
  DateTime createdAt;

  Post({
    required this.postId,
    required this.content,
    required this.creatorId,
    required this.title,
    required this.dislikeIds,
    required this.likeIds,
    required this.creatorUsername,
    required this.replyCount,
    required this.createdAt,
  });

  static Post fromMapToPost(Map map) {
    return Post(
      postId: map[PostCollection.postId],
      content: map[PostCollection.content],
      title: map[PostCollection.title],
      creatorId: map[PostCollection.creatorId],
      creatorUsername: map[PostCollection.creatorUsername],
      replyCount: map[PostCollection.replyCount],
      createdAt: timeUtils.getDateTimeFromTimeStamp(map[PostCollection.createdAt]).toLocal(),
      likeIds: map[PostCollection.likeIds].cast<String>(),
      dislikeIds: map[PostCollection.dislikeIds].cast<String>(),
    );
  }
}

class UserProfile {
  String? id;
  String? authPw;
  String? email;
  String? username;
  String? photoUrl;
  String? xboxGamerTag;
  String? nintendoFriendCode;
  String? steamFriendCode;
  String? psn;
  DateTime? createdAt;

  static const idKey = "id";
  static const photoKey = "photo";
  static const emailKey = "email";
  static const usernameKey = "username";

  UserProfile({
    this.authPw,
    this.id,
    this.email,
    this.username,
    this.photoUrl = defaultProfilePictureLink,
    this.xboxGamerTag = "",
    this.nintendoFriendCode = "",
    this.steamFriendCode = "",
    this.psn = "",
    this.createdAt,
  });

  static UserProfile fromMapToProfile(Map map) {
    return UserProfile(
      authPw: map[UserCollection.authPw],
      username: map[UserCollection.username],
      email: map[UserCollection.email],
      photoUrl: map[UserCollection.photoUrl],
      id: map[UserCollection.id],
      xboxGamerTag: map[UserCollection.xboxGamerTag],
      psn: map[UserCollection.psn],
      nintendoFriendCode: map[UserCollection.nintendoFriendCode],
      steamFriendCode: map[UserCollection.steamFriendCode],
      createdAt: timeUtils.getDateTimeFromTimeStamp(map[UserCollection.createdAt]).toLocal(),
    );
  }
}

class UpcomingGame {
  String gameCover;
  String gameVideo;
  String gameSummary;
  String gameName;
  String gameLink;
  List<String> gameGenre;
  List<String> gamePlatforms;
  List<String> gameTheme;
  DateTime releaseDate;
  DateTime createdAt;

  UpcomingGame({
    required this.gameCover,
    required this.gameVideo,
    required this.gameSummary,
    required this.gameTheme,
    required this.gameGenre,
    required this.gameName,
    required this.gameLink,
    required this.gamePlatforms,
    required this.createdAt,
    required this.releaseDate,
  });

  static UpcomingGame fromMapToUpcomings(Map map) {
    return UpcomingGame(
      gameCover: map[PopularCollection.gameCover],
      gameVideo: map[PopularCollection.gameVideo],
      gameSummary: map[PopularCollection.gameSummary],
      gameName: map[PopularCollection.gameName],
      gameLink: map[PopularCollection.gameLink],
      gameGenre: map[PopularCollection.gameGenre].cast<String>(),
      gamePlatforms: map[PopularCollection.gamePlatforms].cast<String>(),
      gameTheme: map[PopularCollection.gameTheme].cast<String>(),
      createdAt: timeUtils.getDateTimeFromTimeStamp(map[PopularCollection.createdAt]).toLocal(),
      releaseDate: timeUtils.getDateTimeFromTimeStamp(map[PopularCollection.releaseDate]).toLocal(),
    );
  }
}

class News {
  String name;
  String description;
  String link;
  DateTime date;
  DateTime createdAt;

  News({
    required this.name,
    required this.description,
    required this.link,
    required this.createdAt,
    required this.date,
  });

  static News fromMapToNews(Map map) {
    return News(
      name: map[NewsCollection.name],
      description: map[NewsCollection.description],
      link: map[NewsCollection.link],
      date: timeUtils.getDateTimeFromTimeStamp(map[NewsCollection.date]).toLocal(),
      createdAt: timeUtils.getDateTimeFromTimeStamp(map[NewsCollection.createdAt]).toLocal(),
    );
  }
}

class Game {
  String gameId;
  String creatorId;
  String creatorUsername;
  DateTime createdAt;
  String gameName;
  int maxPlayers;
  List<Map> players;
  List<String> playerIds;
  String gamePlatform;
  String platformProduct;
  DateTime startTime;
  DateTime endTime;
  List<String>? gameCover;
  String? gameVideo;
  String? gameSummary;
  List<String>? gameTheme;
  List<String>? gameGenre;

  Game({
    required this.gameId,
    required this.creatorId,
    required this.creatorUsername,
    required this.createdAt,
    required this.gameName,
    required this.maxPlayers,
    required this.gamePlatform,
    required this.platformProduct,
    required this.startTime,
    required this.playerIds,
    required this.players,
    required this.endTime,
    this.gameCover = const [],
    this.gameVideo = "",
    this.gameSummary = "",
    this.gameGenre = const [],
    this.gameTheme = const [],
  });

  static Map fromProfileToMap(UserProfile profile) {
    return {
      UserProfile.idKey: profile.id,
      UserProfile.usernameKey: profile.username,
    };
  }

  static Game fromMapToGame(Map map) {
    return Game(
      gameId: map[GameCollection.gameId],
      gameName: map[GameCollection.gameName],
      gamePlatform: map[GameCollection.gamePlatform],
      platformProduct: map[GameCollection.platformProduct],
      creatorId: map[GameCollection.creatorId],
      creatorUsername: map[GameCollection.creatorUsername],
      playerIds: map[GameCollection.playerIds].cast<String>(),
      players: map[GameCollection.players].cast<Map>(),
      maxPlayers: map[GameCollection.maxPlayers],
      createdAt: timeUtils.getDateTimeFromTimeStamp(map[GameCollection.createdAt]).toLocal(),
      startTime: timeUtils.getDateTimeFromTimeStamp(map[GameCollection.startTime]).toLocal(),
      endTime: timeUtils.getDateTimeFromTimeStamp(map[GameCollection.endTime]).toLocal(),
      gameCover: map[GameCollection.gameCover].cast<String>(),
      gameVideo: map[GameCollection.gameVideo],
      gameSummary: map[GameCollection.gameSummary],
      gameGenre: map[GameCollection.gameGenre].cast<String>(),
      gameTheme: map[GameCollection.gameTheme].cast<String>(),
    );
  }
}

