class StatusCode {
  static const ok = 200;
  static const alreadyGreeted = 210;
  static const exceedDiscoverLike = 211;
  static const wrongToken = 409;
  static const exception = 405;
  static const blacklisted = 555;
  static const duplicateFcmToken = 501;
  static const duplicateUsername = 502;
  static const hasProfile = 201;
  static const noProfile = 199;
  static const tooManyEmail = 410;
  static const duplicateEmail = 444;
  static const hasPreference = 202;
  static const noPreference = 408;
}