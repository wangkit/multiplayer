class GenderStatus {
  static const male = 0;
  static const female = 1;
  static const transgender = 2;
  static const genderQueer = 3;
  static const agender = 4;

  static Map<int, String> genderMap = {
    male: "Male",
    female: "Female",
    transgender: "Transgender",
    genderQueer: "Genderqueer",
    agender: "Agender",
  };
}