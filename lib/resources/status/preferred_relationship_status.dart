class PreferredRelationshipStatus {
  static const marriage = 0;
  static const longTerm = 1;
  static const causal = 2;
  static const noPreference = 3;

  static Map<int, String> relationshipMap = {
    marriage: "Marriage",
    longTerm: "A Long-Term Relationship",
    causal: "A Causal Relationship",
    noPreference: "No Preference",
  };
}