import 'dart:core';

class DiscoveryValue {
  static const addGameFab = "add_game_fab";
  static const gameDurationDisplay = "game_duration_display";
  static const invitationIconButton = "invitation_icon_button";
  static const maxNumberPlayers = "max_number_players";
  static const gameIdManagement = "game_ids_management";
  static const profileIconManagement = "game_icon_management";
  static const platform = "platform";
  static const postTitle = "post_title";
  static const postContent = "post_content";
}