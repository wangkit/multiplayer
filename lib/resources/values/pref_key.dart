class PrefKey {
  static String authPwKey = "passwordKey";
  static String id = "idKey";
  static String username = "usernameKey";
  static String uniqueId = "uniqueIdKey";
  static String email = "emailKey";
  static String hasCalledFCMRegistration = "hasCalledFCMRegistrationKey";
  static String addGameToCalendar = "addGameToCalendar";
  static String isVideoStartMuted = "isVideoStartMuted";
}