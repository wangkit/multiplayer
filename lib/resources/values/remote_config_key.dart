class RemoteConfigKey {
  static const minimumVersion = "minimum_version";
  static const enforceMinimumVersion = "enforce_minimum_version";
  static const gamingPlatforms = "gaming_platforms";
  static const pcPlatforms = "pc_platforms";
  static const mobilePlatforms = "mobile_platforms";
  static const xboxPlatforms = "xbox_platforms";
  static const psPlatforms = "ps_platforms";
  static const nintendoPlatforms = "nintendo_platforms";
  static const socialSubtitle = "social_meta_tag_subtitle";
  static const socialTitle = "social_meta_tag_title";
  static const socialImage = "social_meta_tag_image";
  static const psnAddFriendLink = "psn_add_friend_link";
  static const integrationGif = "integration_gif";
}