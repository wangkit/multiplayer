class Analysis {
  static const quitGame = "quit_game";
  static const joinGame = "join_game";
  static const signUp = "sign_up";
  static const signIn = "login";
  static const conflictedQuit = "conflicted_quit";
  static const signInFail = "login_fail";
  static const signOut = "sign_out";
  static const signUpFacebook = "sign_up_facebook";
  static const signInFacebook = "sign_in_facebook";
  static const signInApple = "sign_in_apple";
  static const signUpApple = "sign_up_apple";
  static const report = "report";
  static const updateCover = "update_cover";
  static const updateGameId = "update_";
  static const revertLikes = "revert_likes";
  static const signInGoogle = "sign_in_google";
  static const signUpGoogle = "sign_up_google";
  static const openAppByNotification = "open_app_by_notification";
  static const block = "block";
  static const createGame = "create_game";
  static const createPost = "create_post";
  static const createReply = "create_reply";
  static const likePost = "like_post";
  static const dislikePost = "dislike_post";
  static const shareThroughInvite = "share_through_invite";
}
