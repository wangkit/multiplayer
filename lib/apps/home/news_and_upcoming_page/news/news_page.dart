import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storyteller/configs/app_config.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/bubble/news_bubble.dart';

class NewsPage extends StatefulWidget {

  NewsPage({
    Key? key,
  }) : super(key: key);

  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> with AutomaticKeepAliveClientMixin<NewsPage> {

  late Future<QuerySnapshot> _getNews;
  late DocumentSnapshot? _lastDocument;
  late RefreshController _refreshController;
  late Query _query;
  late ScrollController _scrollController;
  List<News> _newsList = [];
  List<String> _newsName = [];
  
  @override
  void initState() {
    // SchedulerBinding.instance!.addPostFrameCallback((Duration duration) {
    //   FeatureDiscovery.discoverFeatures(
    //     getRoute.getContext(),
    //     const <String>{
    //       DiscoveryValue.addGameFab,
    //       DiscoveryValue.invitationIconButton,
    //     },
    //   );
    // });
    _refreshController = RefreshController();
    _scrollController = ScrollController(keepScrollOffset: true);
    _query = firestore
        .collection(FirestoreCollections.news)
        .orderBy(NewsCollection.createdAt, descending: true)
        .limit(20);
    _getNews = _query.get();
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return FutureBuilder<QuerySnapshot>(
      future: _getNews,
      builder: (context, snapshot) {

        if (!snapshot.hasData || snapshot.data!.docs.length == 0 || snapshot.connectionState != ConnectionState.done) {
          return CommonWidgets.loading();
        }

        void _handleNews(List? news) {

          if (news == null || news.length == 0) {
            return;
          }

          for (QueryDocumentSnapshot _news in news) {
            News _thisNews = News.fromMapToNews(_news.data() as Map);

            if (!_newsName.contains(_thisNews.name)) {
              _newsList.add(_thisNews);
              _newsName.add(_thisNews.name);
              _lastDocument = _news;
            }
          }
        }

        _handleNews(snapshot.data!.docs);

        return SmartRefresher(
          controller: _refreshController,
          enablePullUp: true,
          enablePullDown: false,
          scrollController: _scrollController,
          onLoading: () async {
            if (_lastDocument != null) {
              QuerySnapshot _result = await _query.startAfterDocument(_lastDocument!)
                  .get();
              _handleNews(_result.docs);
              _refreshController.loadComplete();
              if (_result.size > 0) {
                refreshThisPage();
              }
            }
          },
          child: ListView.builder(
            itemCount: _newsList.length,
            controller: _scrollController,
            itemBuilder: (ctx, index) {
              return NewsBubble(
                news: _newsList[index],
              );
            },
          ),
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
