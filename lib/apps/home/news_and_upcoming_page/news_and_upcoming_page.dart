import 'package:animate_do/animate_do.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:soy_common/pages/soy_about_page.dart';
import 'package:storyteller/apps/add_game/platform_page.dart';
import 'package:storyteller/apps/home/game/home_game_page.dart';
import 'package:storyteller/apps/home/news_and_upcoming_page/news/news_page.dart';
import 'package:storyteller/apps/home/news_and_upcoming_page/upcoming/upcoming_page.dart';
import 'package:storyteller/apps/home/profile/profile_page.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/values/discovery_value.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_routing_bottom_sheet.dart';
import 'package:storyteller/widgets/animations/faded_indexed_stack.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/button/bottom_sheet_button.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/count_down/count_down.dart';
import 'package:storyteller/widgets/feature_discovery/custom_feature_overlay.dart';
import 'package:storyteller/widgets/bubble/game_bubble.dart';
import 'package:storyteller/widgets/image/network_thumbnail.dart';
import 'package:http/http.dart' as http;
import 'package:storyteller/widgets/search/search_game.dart';

class NewsAndUpcomingPage extends StatefulWidget {

  final TabController tabController;

  NewsAndUpcomingPage({
    Key? key,
    required this.tabController,
  }) : super(key: key);

  @override
  _NewsAndUpcomingPageState createState() => _NewsAndUpcomingPageState();
}

class _NewsAndUpcomingPageState extends State<NewsAndUpcomingPage> with AutomaticKeepAliveClientMixin<NewsAndUpcomingPage>, TickerProviderStateMixin {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      body: TabBarView(
        controller: widget.tabController,
        children: [
          UpcomingPage(),
          NewsPage(),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
