import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storyteller/configs/app_config.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/bubble/news_bubble.dart';
import 'package:storyteller/widgets/bubble/upcoming_bubble.dart';

class UpcomingPage extends StatefulWidget {

  UpcomingPage({
    Key? key,
  }) : super(key: key);

  @override
  _UpcomingPageState createState() => _UpcomingPageState();
}

class _UpcomingPageState extends State<UpcomingPage> with AutomaticKeepAliveClientMixin<UpcomingPage> {

  late Future<QuerySnapshot> _getGames;
  late DocumentSnapshot? _lastDocument;
  late RefreshController _refreshController;
  late Query _query;
  late ScrollController _scrollController;
  List<UpcomingGame> _upcomingList = [];
  List<String> _upcomingName = [];

  @override
  void initState() {
    // SchedulerBinding.instance!.addPostFrameCallback((Duration duration) {
    //   FeatureDiscovery.discoverFeatures(
    //     getRoute.getContext(),
    //     const <String>{
    //       DiscoveryValue.addGameFab,
    //       DiscoveryValue.invitationIconButton,
    //     },
    //   );
    // });
    _refreshController = RefreshController();
    _scrollController = ScrollController(keepScrollOffset: true);
    _query = firestore
        .collection(FirestoreCollections.popularGames)
        .where(PopularCollection.releaseDate, isGreaterThanOrEqualTo: DateTime.now())
        .orderBy(PopularCollection.releaseDate)
        .limit(20);
    _getGames = _query.get();
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return FutureBuilder<QuerySnapshot>(
      future: _getGames,
      builder: (context, snapshot) {

        if (!snapshot.hasData || snapshot.data!.docs.length == 0 || snapshot.connectionState != ConnectionState.done) {
          return CommonWidgets.loading();
        }

        void _handleUpcoming(List? news) {

          if (news == null || news.length == 0) {
            return;
          }

          for (QueryDocumentSnapshot _news in news) {
            UpcomingGame _thisUpcoming = UpcomingGame.fromMapToUpcomings(_news.data() as Map);

            if (!_upcomingName.contains(_thisUpcoming.gameName)) {
              _upcomingList.add(_thisUpcoming);
              _upcomingName.add(_thisUpcoming.gameName);
              _lastDocument = _news;
            }
          }
        }

        _handleUpcoming(snapshot.data!.docs);

        return SmartRefresher(
          controller: _refreshController,
          scrollController: _scrollController,
          reverse: false,
          enablePullUp: true,
          enablePullDown: false,
          onLoading: () async {
            if (_lastDocument != null) {
              QuerySnapshot _result = await _query
                  .startAfterDocument(_lastDocument!)
                  .get();
              _handleUpcoming(_result.docs);
              _refreshController.loadComplete();
              if (_result.size > 0) {
                refreshThisPage();
              }
            }
          },
          child: ListView.builder(
            itemCount: _upcomingList.length,
            padding: EdgeInsets.only(top: 4),
            controller: _scrollController,
            itemBuilder: (ctx, index) {
              return UpcomingBubble(
                upcoming: _upcomingList[index],
              );
            },
          ),
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
