import 'package:animate_do/animate_do.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:soy_common/pages/soy_about_page.dart';
import 'package:storyteller/apps/add_game/platform_page.dart';
import 'package:storyteller/apps/home/forum/forum_page.dart';
import 'package:storyteller/apps/home/game/home_game_page.dart';
import 'package:storyteller/apps/home/news_and_upcoming_page/news/news_page.dart';
import 'package:storyteller/apps/home/news_and_upcoming_page/news_and_upcoming_page.dart';
import 'package:storyteller/apps/home/news_and_upcoming_page/upcoming/upcoming_page.dart';
import 'package:storyteller/apps/home/profile/profile_page.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/values/discovery_value.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_routing_bottom_sheet.dart';
import 'package:storyteller/widgets/animations/faded_indexed_stack.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/button/bottom_sheet_button.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/image/network_thumbnail.dart';
import 'package:http/http.dart' as http;
import 'package:storyteller/widgets/search/search_game.dart';

class MainPage extends StatefulWidget {

  bool showSpotlight;

  MainPage({
    Key? key,
    this.showSpotlight = true,
  }) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with AutomaticKeepAliveClientMixin<MainPage>, TickerProviderStateMixin {

  late ValueNotifier<bool> isDialOpen;
  late AnimationController _animationController;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  late int _currentIndex;
  int _bottomBarItemCount = 5;
  final _homeIndex = 0;
  final _forumIndex = 1;
  final _addIndex = 2;
  final _newsIndex = 3;
  final _profileIndex = 4;
  final double _bottomBarIcon = 29;
  late TabController _tabController;

  @override
  void initState() {
    _currentIndex = 0;
    _tabController = TabController(
      length: 2,
      initialIndex: 0,
      vsync: this,
    );
    _animationController = AnimationController(vsync: this, duration: kThemeAnimationDuration);
    isDialOpen = ValueNotifier(false);
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      if (utils.hasLoggedIn()) {
        /// Register firebase messaging
        NotificationSettings settings = await firebaseMessaging!.requestPermission(
          alert: true,
          announcement: false,
          badge: true,
          carPlay: false,
          criticalAlert: false,
          provisional: false,
          sound: true,
        );
      }
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    isDialOpen.dispose();
    _tabController.dispose();
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget _profileIcon() {
    return NetworkThumbnail(
      imageUrl: utils.hasLoggedIn() ? qp.photoUrl! : verticalGreyPlaceholder,
      hero: qp.id!,
      text: qp.username!,
      width: _bottomBarIcon,
      height: _bottomBarIcon,
      disableOnTap: true,
    );
  }

  List<Widget> _actions() {
    return [
      _currentIndex == _homeIndex ? IconButton(
        onPressed: () async {
          await showSearch(
            context: context,
            delegate: SearchGame(contextPage: context),
          );
        },
        icon: Icon(
          Icons.search,
        ),
      ) : CommonWidgets.emptyBox(),
      IconButton(
        onPressed: () async {
          await utils.shareShortUrl("https://multiplayer.com/share");
        },
        icon: Icon(
          MdiIcons.accountPlusOutline,
        ),
      ),
      IconButton(
        onPressed: () {
          getRoute.navigateTo(SoyAboutPage(
            mainColor: mainColor,
            shareMsg: "I have been using $myAppName! Let's play together!",
            title: TextData.about,
            titleBarColor: Colors.white70,
            backgroundColor: appBgColor,
            smallTextStyle: TextStyle(
              fontSize: 12,
            ),
            appName: myAppName,
            version: kIsWeb ? "" : versionName! + "(" + versionCode.toString() + ")",

            /// Default elevation is 4
            elevation: Theme.of(context).appBarTheme.elevation ?? 4,
          ));
        },
        icon: Icon(
          Icons.info_outline,
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return WillPopScope(
      onWillPop: () async {
        if (isDialOpen.value) {
          isDialOpen.value = false;
        }
        return isDialOpen.value;
      },
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: littleGrey,
        bottomNavigationBar: SizedBox(
          height: 48,
          child: BottomNavigationBar(
            showSelectedLabels: false,
            showUnselectedLabels: false,
            type: BottomNavigationBarType.fixed,
            currentIndex: _currentIndex,
            iconSize: _bottomBarIcon,
            onTap: (index) {
              if (index == _addIndex) {
                if (qp.id!.isEmpty) {
                  getSignUpRoutingSheet();
                } else {
                  getRoute.navigateTo(PlatformPage());
                }
              } else if (index != _currentIndex) {
                _currentIndex = index;
                refreshThisPage();
                if (utils.hasLoggedIn() && _currentIndex == _profileIndex) {
                  SchedulerBinding.instance!.addPostFrameCallback((Duration duration) {
                    FeatureDiscovery.discoverFeatures(
                      context,
                      const <String>{
                        DiscoveryValue.profileIconManagement,
                        DiscoveryValue.gameIdManagement,
                      },
                    );
                  });
                }
              }
            },
            selectedItemColor: mainColor,
            unselectedItemColor: mainColor,
            selectedFontSize: 0.0,
            items: List.generate(_bottomBarItemCount, (index) {
             return BottomNavigationBarItem(
               label: index == _homeIndex ? TextData.home :
               index == _addIndex ? TextData.add :
               index == _newsIndex ? TextData.news :
               index == _forumIndex ? TextData.upcomingGames :
               TextData.profile,
               icon: Swing(
                 child: index == _profileIndex ? _profileIcon() : Icon(
                   _currentIndex == index ?
                   index == _addIndex ? Icons.add_box_outlined :
                   index == _homeIndex ? MdiIcons.home :
                   index == _newsIndex ? MdiIcons.gamepadCircle
                       : MdiIcons.post :
                   index == _homeIndex ? MdiIcons.homeOutline :
                   index == _addIndex ? Icons.add_box_outlined :
                   index == _newsIndex ? MdiIcons.gamepadCircleOutline
                       : MdiIcons.postOutline,
                   color: _currentIndex == index ? appIconColor : null,
                 ),
               ),
             );
            })
          ),
        ),
        appBar: CustomAppBar(
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          actions: _actions(),
          bottom: _currentIndex == _newsIndex ? TabBar(
            controller: _tabController,
            indicatorColor: appIconColor,
            tabs: [
              Tab(
                icon: Icon(
                    MdiIcons.gamepadCircleOutline
                ),
              ),
              Tab(
                icon: Icon(
                    MdiIcons.newspaperVariantOutline
                ),
              ),
            ],
          ) : null,
          title: Row(
            children: [
              /// No need profile icon at this place
              // SizedBox(
              //   width: qp.photoUrl != null && qp.photoUrl!.isNotEmpty && firebaseUser != null ? 15 : 0,
              // ),
              // qp.photoUrl != null && qp.photoUrl!.isNotEmpty && firebaseUser != null ? OpenContainerWrapper(
              //     closedWidget: _profileIcon(),
              //     closedElevation: 0,
              //     openedWidget: ProfilePage(userId: qp.id!,),
              // ) : Container(),
              SizedBox(
                width: 20,
              ),
              Text(
                _currentIndex == _homeIndex ? myAppName : _currentIndex == _newsIndex ? TextData.information : _currentIndex == _forumIndex ? TextData.feed : TextData.profile,
                style: Theme.of(context).textTheme.headline6,
              ),
            ],
          ),
        ),
        body: IndexedStack(
          index: _currentIndex,
          children: [
            HomeGamePage(),
            ForumPage(),
            Container(),
            NewsAndUpcomingPage(tabController: _tabController),
            ProfilePage(
              userId: qp.id!,
              showAppBar: false,
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
