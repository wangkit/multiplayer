import 'package:animate_do/animate_do.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storyteller/configs/app_config.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/enum/game_page_type.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/discovery_value.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/dialog/binary_dialog.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/count_down/count_down.dart';
import 'package:storyteller/widgets/bubble/game_bubble.dart';

class HomeGamePage extends StatefulWidget {

  HomeGamePage({
    Key? key,
  }) : super(key: key);

  @override
  _HomeGamePageState createState() => _HomeGamePageState();
}

class _HomeGamePageState extends State<HomeGamePage> with AutomaticKeepAliveClientMixin<HomeGamePage> {

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  late RefreshController _refreshController;
  DocumentSnapshot? _lastDocument;
  late Query _originalQuery;
  late Query _activeQuery;
  late Stream<QuerySnapshot> _gameStream;
  late Future<QuerySnapshot> _currentGame;
  late ScrollController _scrollController;
  List<GameBubble> _gameBubbles = [];
  Map<String, int> _gameMaps = {};
  late List<String> _filterTheme;
  late String _filterPlatform;

  @override
  void dispose() {
    _scrollController.dispose();
    _refreshController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _filterTheme = [];
    _filterPlatform = "";
    _currentGame = firestore.collection(FirestoreCollections.games)
        .where(GameCollection.endTime, isGreaterThanOrEqualTo: DateTime.now())
        .where(GameCollection.playerIds, arrayContains: qp.id!)
        .get();
    _originalQuery = firestore
        .collection(FirestoreCollections.games)
        .where(GameCollection.startTime, isGreaterThan: DateTime.now().toUtc())
        .orderBy(GameCollection.startTime, descending: false)
        .limit(GameCollection.limit);
    _activeQuery = _originalQuery;
    _gameStream = _activeQuery.snapshots();
    _scrollController = ScrollController(keepScrollOffset: true);
    _refreshController = RefreshController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      backgroundColor: transparent,
      floatingActionButton: _filterPlatform.isNotEmpty || _filterTheme.isNotEmpty ? FloatingActionButton(
        onPressed: () {
          _gameBubbles.clear();
          _gameMaps.clear();
          _filterPlatform = "";
          _filterTheme.clear();
          _activeQuery = _originalQuery;
          _gameStream = _activeQuery.snapshots();
          refreshThisPage();
        },
        backgroundColor: appIconColor,
        child: Icon(
          Icons.clear_outlined,
          color: CustomWhite,
        ),
      ) : null,
      body: SafeArea(
        child: StreamBuilder<QuerySnapshot>(
          stream: _gameStream,
          builder: (context, snapshot) {

            void _onThemeTagClick(String _theme) {
              if (!_filterTheme.contains(_theme)) {
                _gameBubbles.clear();
                _gameMaps.clear();
                _filterTheme.add(_theme);
                _activeQuery = _originalQuery.where(GameCollection.gameTheme, arrayContainsAny: _filterTheme);
                _gameStream = _activeQuery.snapshots();
                refreshThisPage();
              }
            }

            void _onPlatformTagClick(String _platformProduct) {
              if (_platformProduct != _filterPlatform) {
                _gameBubbles.clear();
                _gameMaps.clear();
                _filterPlatform = _platformProduct;
                _activeQuery = _activeQuery.where(GameCollection.platformProduct, isEqualTo: _filterPlatform);
                _gameStream = _activeQuery.snapshots();
                refreshThisPage();
              }
            }

            void _handleGames(List? games) {

              if (games == null || games.length == 0) {
                return;
              }

              for (QueryDocumentSnapshot game in games) {

                Game _game = Game.fromMapToGame(game.data() as Map);

                GameBubble _bubble = GameBubble(
                  game: _game,
                  onThemeTagClick: _onThemeTagClick,
                  onPlatformTagClick: _onPlatformTagClick,
                );

                if (!_gameMaps.containsKey(_game.gameId)) {
                  _gameBubbles.add(_bubble);
                  _gameMaps[_game.gameId] = _gameBubbles.length - 1;
                  _lastDocument = game;
                } else {
                  _gameBubbles[_gameMaps[_game.gameId]!] = _bubble;
                }
              }
            }

            if (snapshot.connectionState != ConnectionState.active) {
              return CommonWidgets.loading();
            }

            if (!snapshot.hasData || snapshot.data!.size == 0) {
              return CommonWidgets.emptyMessage(TextData.noGames);
            }

            _handleGames(snapshot.data!.docs);

            return SmartRefresher(
              controller: _refreshController,
              reverse: false,
              enablePullUp: true,
              enablePullDown: false,
              scrollController: _scrollController,
              onLoading: () {
                if (_lastDocument != null) {
                  _originalQuery
                      .startAfterDocument(_lastDocument!)
                      .snapshots().listen((QuerySnapshot _loadSnapshot) {
                    _handleGames(_loadSnapshot.docs);
                    _refreshController.loadComplete();
                    if (_loadSnapshot.size > 0) {
                      refreshThisPage();
                    }
                  });
                }
              },
              child: ListView.builder(
                controller: _scrollController,
                itemCount: _gameBubbles.length == 0 ? 1 : _gameBubbles.length,
                itemBuilder: (ctx, index) {
                  return Column(
                    children: [
                      index > 0 ? CommonWidgets.emptyBox() : StatefulBuilder(
                        builder: (ctx, customState) {
                          return FutureBuilder(
                            future: _currentGame,
                            builder: (ctx, AsyncSnapshot<QuerySnapshot> snapshot) {
                              if (snapshot.hasData) {
                                List<QueryDocumentSnapshot> _joinedGames = snapshot.data!.docs;
                                if (_joinedGames.length > 0) {
                                  Game _joinedGame = Game.fromMapToGame(_joinedGames[0].data() as Map);
                                  if (_joinedGame.startTime.isBefore(DateTime.now())) {
                                    return Material(
                                      elevation: 6,
                                      child: Container(
                                        width: deviceWidth,
                                        height: 50,
                                        color: unfocusedColor,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Text(
                                              "Time for ${_joinedGame.gameName}...",
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                            CountDown(
                                              endTime: _joinedGame.endTime.millisecondsSinceEpoch,
                                              endText: "Ended",
                                              headText: "",
                                              onEnd: () {},
                                              showHeadText: false,
                                              countDownStyle: Theme.of(context).textTheme.subtitle1,
                                            ),
                                            IconButton(
                                              onPressed: () {
                                                getBinaryDialog("Quit Game", "Are you sure you want to quit?", () {
                                                  getRoute.pop();
                                                  apiManager.quitGame(_joinedGame.gameId);
                                                  utils.deleteEvents(_joinedGame);
                                                  customState(() {});
                                                });
                                              },
                                              icon: Icon(
                                                Icons.delete_outline,
                                                color: mainColor,
                                                size: 20,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  }
                                }
                              }
                              return Container();
                            },
                          );
                        },
                      ),
                      (_filterTheme.isNotEmpty || _filterPlatform.isNotEmpty) && index == 0 ? Padding(
                        padding: EdgeInsets.symmetric(vertical: 8),
                        child: Wrap(
                          alignment: WrapAlignment.start,
                          runAlignment: WrapAlignment.start,
                          children: List.generate(_filterTheme.length + (_filterPlatform.isNotEmpty ? 1 : 0), (int index) {

                            late String _title;

                            if (index >= _filterTheme.length) {
                              _title = _filterPlatform;
                            } else {
                              _title = _filterTheme[index];
                            }

                            return CommonWidgets.tag(
                              _title,
                              icon: Icon(
                                MdiIcons.deleteOutline,
                                size: 16,
                              ),
                              onTap: () {
                                if (_filterTheme.contains(_title)) {
                                  _filterTheme.remove(_title);
                                } else if (_filterPlatform == _title) {
                                  _filterPlatform = "";
                                }
                                _gameBubbles.clear();
                                _gameMaps.clear();
                                if (_filterTheme.isEmpty && _filterPlatform.isEmpty) {
                                  _activeQuery = _originalQuery;
                                  _gameStream = _activeQuery.snapshots();
                                } else if (_filterPlatform.isEmpty && _filterTheme.isNotEmpty) {
                                  _activeQuery = _originalQuery.where(GameCollection.gameTheme, arrayContainsAny: _filterTheme);
                                  _gameStream = _activeQuery.snapshots();
                                } else if (_filterPlatform.isNotEmpty && _filterTheme.isEmpty) {
                                  _activeQuery = _originalQuery.where(GameCollection.platformProduct, isEqualTo: _filterPlatform);
                                  _gameStream = _activeQuery.snapshots();
                                } else {
                                  _activeQuery = _originalQuery.where(GameCollection.gameTheme, arrayContainsAny: _filterTheme);
                                  _activeQuery = _activeQuery.where(GameCollection.platformProduct, isEqualTo: _filterPlatform);
                                  _gameStream = _activeQuery.snapshots();
                                }
                                refreshThisPage();
                              },
                              color: unfocusedColor,
                            );
                          }),
                        ),
                      ) : CommonWidgets.emptyBox(),
                      _gameBubbles[index],
                    ],
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
