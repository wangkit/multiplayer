import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storyteller/apps/posts/add_post_page.dart';
import 'package:storyteller/configs/app_config.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_routing_bottom_sheet.dart';
import 'package:storyteller/widgets/bubble/post_bubble.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/bubble/news_bubble.dart';
import 'package:storyteller/widgets/bubble/upcoming_bubble.dart';

class ForumPage extends StatefulWidget {

  ForumPage({
    Key? key,
  }) : super(key: key);

  @override
  _ForumPageState createState() => _ForumPageState();
}

class _ForumPageState extends State<ForumPage> with AutomaticKeepAliveClientMixin<ForumPage> {

  late Future<QuerySnapshot> _getGames;
  late DocumentSnapshot? _lastDocument;
  late RefreshController _refreshController;
  late Query _query;
  late ScrollController _scrollController;
  List<Post> _postList = [];
  List<String> _postIds = [];

  @override
  void initState() {
    _refreshController = RefreshController();
    _scrollController = ScrollController(keepScrollOffset: true);
    _query = firestore
        .collection(FirestoreCollections.posts)
        .orderBy(PostCollection.createdAt, descending: true)
        .limit(50);
    _getGames = _query.get();
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (utils.hasLoggedIn()) {
            getRoute.navigateTo(AddPostPage());
          } else {
            getSignUpRoutingSheet();
          }
        },
        child: Icon(
          Icons.post_add_outlined,
          color: CustomWhite,
        ),
      ),
      body: FutureBuilder<QuerySnapshot>(
        future: _getGames,
        builder: (context, snapshot) {

          if (!snapshot.hasData || snapshot.data!.docs.length == 0 || snapshot.connectionState != ConnectionState.done) {
            if (snapshot.hasData && snapshot.data!.docs.length == 0) {
              return CommonWidgets.emptyMessage(TextData.nothingToShow);
            }
            return CommonWidgets.loading();
          }

          void _handleUpcoming(List? posts) {

            if (posts == null || posts.length == 0) {
              return;
            }

            for (QueryDocumentSnapshot _post in posts) {
              Post _thisPost = Post.fromMapToPost(_post.data() as Map);

              if (!_postIds.contains(_thisPost.postId)) {
                _postList.add(_thisPost);
                _postIds.add(_thisPost.postId);
                _lastDocument = _post;
              }
            }
          }

          _handleUpcoming(snapshot.data!.docs);

          return SmartRefresher(
            controller: _refreshController,
            scrollController: _scrollController,
            reverse: false,
            enablePullUp: true,
            enablePullDown: true,
            onRefresh: () async {
              QuerySnapshot _result = await _query.get();
              _postList.clear();
              _postIds.clear();
              _handleUpcoming(_result.docs);
              _refreshController.refreshCompleted();
              refreshThisPage();
            },
            onLoading: () async {
              if (_lastDocument != null) {
                QuerySnapshot _result = await _query
                    .startAfterDocument(_lastDocument!)
                    .get();
                _handleUpcoming(_result.docs);
                _refreshController.loadComplete();
                if (_result.size > 0) {
                  refreshThisPage();
                }
              }
            },
            child: ListView.separated(
              separatorBuilder: (ctx, index) {
                return CommonWidgets.divider();
              },
              itemCount: _postList.length + 2,
              controller: _scrollController,
              itemBuilder: (ctx, index) {

                if (index == 0 || index == _postList.length + 1) {
                  return CommonWidgets.divider();
                }

                return PostBubble(
                  post: _postList[index - 1],
                );
              },
            ),
          );
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
