import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get_connect/http/src/response/response.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storyteller/apps/add_game/add_game_page.dart';
import 'package:storyteller/apps/games/games_page.dart';
import 'package:storyteller/apps/home/main/main_page.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/enum/game_page_type.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/discovery_value.dart';
import 'package:storyteller/resources/values/pref_key.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_routing_bottom_sheet.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_with_email.dart';
import 'package:storyteller/utils/dialog/binary_dialog.dart';
import 'package:storyteller/utils/dialog/block_dialog.dart';
import 'package:storyteller/utils/dialog/game_id_dialog.dart';
import 'package:storyteller/utils/dialog/report_dialog.dart';
import 'package:storyteller/utils/steam/steam_utils.dart';
import 'package:storyteller/widgets/animations/faded_indexed_stack.dart';
import 'package:storyteller/widgets/animations/open_container_wrapper.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/button/bottom_sheet_button.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/count_down/count_down.dart';
import 'package:storyteller/widgets/feature_discovery/custom_feature_overlay.dart';
import 'package:storyteller/widgets/bubble/game_bubble.dart';
import 'package:storyteller/widgets/image/file_thumbnail.dart';
import 'package:storyteller/widgets/image/network_thumbnail.dart';
import 'package:http/http.dart' as http;
import 'package:storyteller/widgets/search/search_game.dart';
import 'package:storyteller/widgets/webview/custom_web_view.dart';
import 'package:validators2/validators.dart';

class ProfilePage extends StatefulWidget {

  final String userId;
  final bool showAppBar;

  ProfilePage({
    Key? key,
    required this.userId,
    this.showAppBar = true,
  }) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> with AutomaticKeepAliveClientMixin<ProfilePage> {

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  late bool isMyProfile;
  File? _image;
  late Future<QuerySnapshot> _getProfile;

  @override
  void initState() {
    isMyProfile = widget.userId == qp.id!;
    _getProfile = firestore
        .collection(FirestoreCollections.users)
        .where(UserCollection.id, isEqualTo: widget.userId)
        .limit(1)
        .get();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return !utils.hasLoggedIn() ? Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Sign in now to view your profile",
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          ElevatedButton(
            onPressed: () {
              getSignUpRoutingSheet();
            },
            style: ElevatedButton.styleFrom(
              primary: unfocusedColor,
            ),
            child: Text(
              TextData.signIn,
            ),
          ),
        ],
      ),
    ) : FutureBuilder<QuerySnapshot>(
      future: _getProfile,
      builder: (context, snapshot) {

        if ((!snapshot.hasData || snapshot.data!.docs.length == 0) && !isMyProfile) {
          /// We need this widget to be a scaffold in order to avoid weird black flash
          return Scaffold(
            appBar: CustomAppBar(),
            body: Center(
              child: CommonWidgets.loading(),
            ),
          );
        }

        UserProfile _thisUser = isMyProfile ? qp : UserProfile.fromMapToProfile(snapshot.data!.docs.first.data() as Map);

        return Scaffold(
          floatingActionButton: _image != null ? FloatingActionButton(
            onPressed: () async {
              utils.loadToast(msg: TextData.uploading);
              await apiManager.updateProfilePicture(_image!, qp.id!);
              _image = null;
              refreshThisPage();
              getRoute.pop();
            },
            child: Icon(
              Icons.cloud_upload_outlined,
              color: Colors.white,
            ),
          ) : null,
          appBar: !widget.showAppBar ? null : isMyProfile ? CustomAppBar() : CustomAppBar(
            actions: [
              IconButton(
                onPressed: () {
                  getGenericBottomSheet([
                    BottomSheetButton(
                      onTap: () {
                        getReportDialog(_thisUser);
                      },
                      iconData: Icons.report_outlined,
                      label: TextData.report,
                    ),
                    BottomSheetButton(
                      onTap: () {
                        getBlockDialog(_thisUser);
                      },
                      iconData: Icons.block_outlined,
                      label: TextData.block,
                    ),
                  ]);
                },
                icon: Icon(
                  Icons.more_vert,
                ),
              ),
            ],
            title: Text(
              _thisUser.username!,
            ),
          ),
          body: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 30,
                  ),
                  CustomFeatureOverlay(
                    id: DiscoveryValue.profileIconManagement,
                    title: TextData.changeProfileIcon,
                    description: TextData.changeProfileIconDescription,
                    backgroundColor: sharp_pink,
                    child: Center(
                      child: GestureDetector(
                        onTap: () async {
                          if (isMyProfile) {
                            final pickedFile = await imagePicker.getImage(source: ImageSource.gallery);
                            if (pickedFile != null) {
                              _image = File(pickedFile.path);
                              refreshThisPage();
                            }
                          }
                        },
                        child: _image != null ? CustomFileImage(
                          file: _image!,
                          width: 200,
                          height: 200,
                        ) : NetworkThumbnail(
                          /// Remember to only show hero to top left if this is my profile
                          /// value: TextData.myProfile
                          /// Maybe open a new class to store hero key value is better and clearer
                          /// Do not show hero if this is not my profile
                          hero: widget.showAppBar ? widget.userId : UniqueKey().toString(),
                          imageUrl: _thisUser.photoUrl!,
                          disableOnTap: isMyProfile,
                          text: _thisUser.username!,
                          width: 200,
                          height: 200,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    "Since ${timeUtils.getParsedYearMonthDay(_thisUser.createdAt!)}",
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CustomFeatureOverlay(
                    id: DiscoveryValue.gameIdManagement,
                    title: TextData.gameIdDiscovery,
                    description: TextData.gameIdDescription,
                    backgroundColor: ruby,
                    child: Container(
                      height: 150,
                      child: GridView.count(
                        crossAxisCount: 2,
                        primary: false,
                        physics: NeverScrollableScrollPhysics(),
                        childAspectRatio: 3 / 1,
                        children: List.generate(4, (index) {

                          String _id = [_thisUser.psn!, _thisUser.xboxGamerTag!, _thisUser.nintendoFriendCode!, _thisUser.steamFriendCode!][index];

                          void refreshWithNewId({bool needWaitBinding = true}) {

                            void _setId() {
                              _id = [qp.psn!, qp.xboxGamerTag!, qp.nintendoFriendCode!, qp.steamFriendCode!][index];
                              refreshThisPage();
                            }

                            if (needWaitBinding) {
                              WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
                                _setId();
                              });
                            } else {
                              _setId();
                            }
                          }

                          return Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(commonRadius),
                            ),
                            color: [playStation, xboxColor, switchColor, steamColor][index],
                            child: InkWell(
                              onLongPress: () {
                                utils.copyToClipboard(_id);
                                utils.toast(TextData.copied);
                              },
                              onTap: !isMyProfile ? () {
                                /// Not myself, go add friend
                                if (_id.isNotEmpty) {
                                  utils.addFriendIntegration([ConfigCollection.playstation, ConfigCollection.xbox, ConfigCollection.nintendo, ConfigCollection.steam][index], _id);
                                }
                              } : () async {
                                /// Is myself, ask update
                                if (index == 2) {
                                  /// Nintendo
                                  await getUpdateGameIdDialog(
                                    [UserCollection.psn, UserCollection.xboxGamerTag, UserCollection.nintendoFriendCode, UserCollection.steamFriendCode][index],
                                  );
                                } else {
                                  switch ([ConfigCollection.playstation, ConfigCollection.xbox, ConfigCollection.nintendo, ConfigCollection.steam][index]) {
                                    case ConfigCollection.nintendo:
                                      utils.toast(TextData.noNintendoSupport, isWarning: true);
                                      break;
                                    case ConfigCollection.playstation:
                                    /// No spaces for psn name
                                    /// Do not use in-app webview for playstation page since this page does not work on in-app webview
                                      if (Platform.isIOS) {
                                        getBinaryDialog(
                                          TextData.safari,
                                          TextData.safariBug,
                                              () {
                                            getRoute.pop();
                                            utils.launchURL(Uri.parse(psnAddFriendBaseLink).toString());
                                          },
                                          positiveText: TextData.go,
                                        );
                                      } else {
                                        /// Android webview can work
                                        if (qp.psn!.isEmpty) {
                                          getRoute.navigateTo(CustomWebView(
                                            link: psnLoginLink,
                                            targetPlatform: ConfigCollection.playstation,
                                            onDismiss: refreshWithNewId,
                                          ),);
                                        } else {
                                          getRoute.navigateTo(CustomWebView(
                                            link: psnAddFriendBaseLink + qp.psn!,
                                          ),);
                                        }
                                      }
                                      break;
                                    case ConfigCollection.xbox:
                                      if (qp.xboxGamerTag!.isEmpty) {
                                        /// If empty friend code, go to login
                                        getRoute.navigateTo(CustomWebView(
                                          link: Uri.parse(xboxAddFriendBaseLink).toString(),
                                          targetPlatform: ConfigCollection.xbox,
                                          onDismiss: refreshWithNewId,
                                        ));
                                      } else {
                                        /// If not empty friend code, go to profile page
                                        getRoute.navigateTo(CustomWebView(
                                          link: Uri.parse(xboxAddFriendBaseLink + qp.xboxGamerTag!).toString(),
                                        ));
                                      }
                                      break;
                                    case ConfigCollection.steam:
                                      if (qp.steamFriendCode!.isEmpty) {
                                        /// If empty friend code, go to login
                                        getRoute.navigateTo(CustomWebView(
                                          link: Uri.parse(steamLoginLink).toString(),
                                          targetPlatform: ConfigCollection.steam,
                                          onDismiss: refreshWithNewId,
                                        ));
                                      } else {
                                        /// If not empty friend code, go to profile page
                                        getRoute.navigateTo(CustomWebView(
                                          link: Uri.parse(steamProfileLink + SteamUtils.from32To64(qp.steamFriendCode!)).toString(),
                                        ));
                                      }
                                      break;
                                  }
                                }

                                /// Set the id value to new latest value after change / or not change
                                refreshWithNewId(
                                  needWaitBinding: false
                                );
                              },
                              borderRadius: BorderRadius.circular(commonRadius),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Icon(
                                    [MdiIcons.sonyPlaystation, MdiIcons.microsoftXbox, MdiIcons.nintendoSwitch, MdiIcons.steam][index],
                                    key: Key(_id + "icon"),
                                    color: _id.isEmpty ? unfocusedColor : Colors.white,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: Text(
                                      _id.isEmpty ? "Empty" : _id,
                                      overflow: TextOverflow.ellipsis,
                                      key: Key(_id + "text"),
                                      style: TextStyle(
                                        color: _id.isEmpty ? unfocusedColor : Colors.white,
                                      ),
                                    ),
                                  ),
                                  IconButton(
                                    onPressed: () async {
                                      if (isMyProfile) {

                                        String _name = ["PSN", "Xbox Gamertag", "Nintendo Friend Code", "Steam Friend Code"][index];

                                        getGenericBottomSheet([
                                          BottomSheetButton(
                                            onTap: () async {
                                              await getUpdateGameIdDialog(
                                                [UserCollection.psn, UserCollection.xboxGamerTag, UserCollection.nintendoFriendCode, UserCollection.steamFriendCode][index],
                                              );
                                              refreshWithNewId(
                                                  needWaitBinding: false
                                              );
                                            },
                                            label: "Edit $_name",
                                            iconData: Icons.edit_outlined,
                                          ),
                                          BottomSheetButton(
                                            onTap: () async {
                                              late String _targetKey;
                                              switch ([ConfigCollection.playstation, ConfigCollection.xbox, ConfigCollection.nintendo, ConfigCollection.steam][index]) {
                                                case ConfigCollection.nintendo:
                                                  _targetKey = UserCollection.nintendoFriendCode;
                                                  qp.nintendoFriendCode = "";
                                                  break;
                                                case ConfigCollection.playstation:
                                                  _targetKey = UserCollection.psn;
                                                  qp.psn = "";
                                                  break;
                                                case ConfigCollection.xbox:
                                                  _targetKey = UserCollection.xboxGamerTag;
                                                  qp.xboxGamerTag = "";
                                                  break;
                                                case ConfigCollection.steam:
                                                  _targetKey = UserCollection.steamFriendCode;
                                                  qp.steamFriendCode = "";
                                                  break;
                                              }
                                              if (_targetKey != null && _targetKey.isNotEmpty) {
                                                /// Clear value
                                                await firestore.collection(FirestoreCollections.users).doc(qp.id!).update({
                                                  _targetKey: "",
                                                });
                                                refreshThisPage();
                                                utils.toast(TextData.removed);
                                              }
                                            },
                                            label: "Remove $_name",
                                            iconData: Icons.delete_outline,
                                          ),
                                        ]);
                                      } else {
                                        utils.copyToClipboard(_id);
                                        utils.toast(TextData.copied);
                                      }
                                    },
                                    icon: Icon(
                                      isMyProfile ? Icons.more_vert_outlined : Icons.copy,
                                      color: unfocusedColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        })
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CommonWidgets.divider(),
                  ListTile(
                    onTap: () {
                      getRoute.navigateTo(GamesPage(
                        type: GamePageType.joined,
                        targetId: _thisUser.id!,
                        title: TextData.joinedGames,
                      ),);
                    },
                    leading: Icon(
                      Icons.videogame_asset_outlined,
                    ),
                    title: Text(
                      TextData.joinedGames,
                    ),
                  ),
                  CommonWidgets.divider(),
                  ListTile(
                    onTap: () {
                      getRoute.navigateTo(isMyProfile ? GamesPage(
                        type: GamePageType.created,
                        targetId: _thisUser.id!,
                        title: TextData.createdGames,
                      ) : GamesPage(
                        type: GamePageType.mutual,
                        targetId: _thisUser.id!,
                        title: "You and ${_thisUser.username}",
                      ),);
                    },
                    leading: Icon(
                      isMyProfile ? Icons.create : Icons.history_outlined,
                    ),
                    title: Text(
                      isMyProfile ? TextData.createdGames : "You and ${_thisUser.username}",
                    ),
                  ),
                  CommonWidgets.divider(),
                  isMyProfile ? Flexible(
                    child: ListTile(
                      onTap: () {
                        getBinaryDialog("Sign Out", "Are you sure you want to sign out?", () {
                          utils.logoutClearData();
                          getRoute.navigateToAndPopAll(MainPage());
                        }, positiveText: TextData.yes, negativeText: TextData.no, positiveColor: errorColor);
                      },
                      leading: Icon(
                        Icons.logout,
                      ),
                      title: Text(
                        "Sign Out",
                      ),
                    ),
                  ) : Container(),
                  isMyProfile ? CommonWidgets.divider() : Container(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
