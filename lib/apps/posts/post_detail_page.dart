import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storyteller/apps/posts/add_post_page.dart';
import 'package:storyteller/apps/posts/add_reply_page.dart';
import 'package:storyteller/configs/app_config.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_routing_bottom_sheet.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/bubble/post_bubble.dart';
import 'package:storyteller/widgets/bubble/reply_bubble.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/bubble/news_bubble.dart';
import 'package:storyteller/widgets/bubble/upcoming_bubble.dart';
import 'package:storyteller/widgets/image/network_thumbnail.dart';

class PostDetailPage extends StatefulWidget {

  Post post;
  Function? refreshParent;

  PostDetailPage({
    Key? key,
    required this.post,
    required this.refreshParent,
  }) : super(key: key);

  @override
  _PostDetailPageState createState() => _PostDetailPageState();
}

class _PostDetailPageState extends State<PostDetailPage> with AutomaticKeepAliveClientMixin<PostDetailPage> {

  late Future<QuerySnapshot> _getReplies;
  late DocumentSnapshot? _lastDocument;
  late RefreshController _refreshController;
  late Query _query;
  late ScrollController _scrollController;
  List<Reply> _replyList = [];
  List<String> _replyIds = [];

  @override
  void initState() {
    _lastDocument = null;
    _refreshController = RefreshController();
    _scrollController = ScrollController(keepScrollOffset: true);
    _query = firestore
        .collection(FirestoreCollections.posts)
        .doc(widget.post.postId)
        .collection(FirestoreCollections.replies)
        .orderBy(PostCollection.createdAt, descending: true)
        .limit(50);
    _getReplies = _query.get();
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      appBar: CustomAppBar(
        title: Text(
          TextData.post,
        ),
        actions: [
          IconButton(
            onPressed: () {
              utils.postLongPress(widget.post);
            },
            icon: Icon(
              Icons.more_vert_outlined,
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              width: 10,
            ),
            NetworkThumbnail(
              imageUrl: qp.photoUrl!,
              text: '',
              width: 40,
              height: 40,
            ),
            SizedBox(
              width: 10,
              height: 60,
            ),
            Expanded(
              child: Material(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(commonRadius),
                child: InkWell(
                  splashColor: utils.convertStrToColor(qp.id!),
                  borderRadius: BorderRadius.circular(commonRadius),
                  onTap: () {
                    if (utils.hasLoggedIn()) {
                      getRoute.navigateTo(AddReplyPage(post: widget.post));
                    } else {
                      getSignUpRoutingSheet();
                    }
                  },
                  child: Padding(
                    padding: EdgeInsets.only(left: 12.0),
                    child: TextField(
                      readOnly: true,
                      enabled: false,
                      decoration: InputDecoration(
                        fillColor: Colors.grey[200],
                        enabledBorder: InputBorder.none,
                        hintText: "Add a reply",
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 20,
            ),
          ],
        ),
      ),
      body: FutureBuilder<QuerySnapshot>(
        future: _getReplies,
        builder: (context, snapshot) {

          if (snapshot.connectionState != ConnectionState.done) {
            return CommonWidgets.loading();
          }

          void _handleUpcoming(List? posts) {

            if (posts == null || posts.length == 0) {
              return;
            }

            for (QueryDocumentSnapshot _reply in posts) {
              Reply _thisReply = Reply.fromMapToReply(_reply.data() as Map);

              if (!_replyIds.contains(_thisReply.replyId)) {
                _replyList.add(_thisReply);
                _replyIds.add(_thisReply.replyId);
                _lastDocument = _reply;
              }
            }
          }

          _handleUpcoming(snapshot.data!.docs);

          return SmartRefresher(
            controller: _refreshController,
            scrollController: _scrollController,
            reverse: false,
            enablePullUp: true,
            enablePullDown: true,
            onRefresh: () async {
              QuerySnapshot _result = await _query.get();
              _replyList.clear();
              _replyIds.clear();
              _handleUpcoming(_result.docs);
              _refreshController.refreshCompleted();
              refreshThisPage();
            },
            onLoading: () async {
              if (_lastDocument != null) {
                QuerySnapshot _result = await _query
                    .startAfterDocument(_lastDocument!)
                    .get();
                _handleUpcoming(_result.docs);
                _refreshController.loadComplete();
                if (_result.size > 0) {
                  refreshThisPage();
                }
              }
            },
            child: ListView.separated(
              separatorBuilder: (ctx, index) {
                return CommonWidgets.divider();
              },
              itemCount: _replyList.length == 0 ? 1 : _replyList.length,
              controller: _scrollController,
              itemBuilder: (ctx, index) {

                if (index == 0) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      PostBubble(
                        post: widget.post,
                        refreshParent: widget.refreshParent,
                        isClickable: false,
                        isFeed: false,
                      ),
                      CommonWidgets.divider(),
                      snapshot.data!.docs.length == 0 ? SizedBox(
                        height: 70,
                      ) : CommonWidgets.emptyBox(),
                      snapshot.data!.docs.length == 0 ? CommonWidgets.emptyMessageWithIcon(TextData.noReplies) :
                          ReplyBubble(
                        reply: _replyList[index],
                        post: widget.post,
                      ),
                    ],
                  );
                }

                return ReplyBubble(
                  reply: _replyList[index],
                  post: widget.post,
                );
              },
            ),
          );
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
