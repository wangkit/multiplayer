import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/discovery_value.dart';
import 'package:storyteller/resources/values/pref_key.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_routing_bottom_sheet.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/custom_date_picker/custom_date_picker.dart';
import 'package:storyteller/widgets/custom_datetime_picker_formfield/custom_datetime_picker_formfield.dart';
import 'package:storyteller/widgets/custom_time_picker/custom_time_picker.dart';
import 'package:storyteller/widgets/feature_discovery/custom_feature_overlay.dart';
import 'package:storyteller/widgets/tag/tag.dart';
import 'package:storyteller/widgets/button/long_elevated_button.dart';
import 'package:storyteller/widgets/textfield/email_textfield.dart';
import 'package:storyteller/widgets/textfield/password_textfield.dart';
import 'package:uuid/uuid.dart';
import 'package:validators2/validators.dart';

class AddPostPage extends StatefulWidget {

  AddPostPage({
    Key? key,
  }) : super(key: key);

  @override
  _AddPostPageState createState() => _AddPostPageState();
}

class _AddPostPageState extends State<AddPostPage> {

  late TextEditingController postTitleController;
  late TextEditingController postContentController;
  late FocusNode postTitleNode;
  late FocusNode postContentNode;
  GlobalKey<FormState> formKey = GlobalKey();
  late Post _newPost;

  @override
  void initState() {
    postTitleController = TextEditingController();
    postContentController = TextEditingController();
    postTitleNode = FocusNode();
    postContentNode = FocusNode();
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((Duration duration) {
      FeatureDiscovery.discoverFeatures(
        context,
        const <String>{ // Feature ids for every feature that you want to showcase in order.
          DiscoveryValue.postTitle,
          DiscoveryValue.postContent,
        },
      );
    });
  }

  @override
  void dispose() {
    postTitleController.dispose();
    postContentController.dispose();
    postTitleNode.dispose();
    postContentNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        centerTitle: false,
        leading: IconButton(
          onPressed: () => getRoute.pop(),
          icon: Icon(
            Icons.clear,
            color: mainColor,
          ),
        ),
        title: Text(
          "Add Post",
        ),
        actions: [
          TextButton(
            onPressed: () async {
              if (formKey.currentState!.validate()) {
                if (!utils.hasLoggedIn()) {
                  getSignUpRoutingSheet();
                }
                utils.loadToast();
                _newPost = Post(
                  creatorUsername: qp.username!,
                  creatorId: qp.id!,
                  createdAt: DateTime.now(),
                  likeIds: [],
                  dislikeIds: [],
                  postId: Uuid().v4(),
                  replyCount: 0,
                  title: postTitleController.text.trim(),
                  content: postContentController.text.trim(),
                );
                await apiManager.createPost(_newPost);
                postTitleController.clear();
                postContentController.clear();
                getRoute.pop();
                getRoute.pop();
              }
            },
            child: Text(
              TextData.post,
              style: TextStyle(
                color: appIconColor,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 0.0),
                child: TextFormField(
                  controller: postTitleController,
                  focusNode: postTitleNode,
                  maxLines: null,
                  textCapitalization: TextCapitalization.sentences,
                  keyboardType: TextInputType.text,
                  maxLength: 120,
                  validator: (String? current) {
                    if (current == null || current.trim().isEmpty) {
                      return "Title cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: TextData.postTitle,
                  ),
                ),
              ),
              CommonWidgets.divider(),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 0.0),
                child: TextFormField(
                  controller: postContentController,
                  focusNode: postContentNode,
                  textInputAction: TextInputAction.newline,
                  maxLines: 15,
                  textCapitalization: TextCapitalization.sentences,
                  keyboardType: TextInputType.multiline,
                  maxLength: 1000,
                  validator: (String? current) {
                    if (current == null || current.trim().isEmpty) {
                      return "Content cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: TextData.postContent,
                  ),
                ),
              ),
              CommonWidgets.divider(),
            ],
          ),
        ),
      ),
    );
  }
}
