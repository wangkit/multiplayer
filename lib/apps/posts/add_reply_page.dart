import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/discovery_value.dart';
import 'package:storyteller/resources/values/pref_key.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/custom_date_picker/custom_date_picker.dart';
import 'package:storyteller/widgets/custom_datetime_picker_formfield/custom_datetime_picker_formfield.dart';
import 'package:storyteller/widgets/custom_time_picker/custom_time_picker.dart';
import 'package:storyteller/widgets/feature_discovery/custom_feature_overlay.dart';
import 'package:storyteller/widgets/tag/tag.dart';
import 'package:storyteller/widgets/button/long_elevated_button.dart';
import 'package:storyteller/widgets/textfield/email_textfield.dart';
import 'package:storyteller/widgets/textfield/password_textfield.dart';
import 'package:uuid/uuid.dart';
import 'package:validators2/validators.dart';

class AddReplyPage extends StatefulWidget {
  
  final Post post;

  AddReplyPage({
    Key? key,
    required this.post,
  }) : super(key: key);

  @override
  _AddReplyPageState createState() => _AddReplyPageState();
}

class _AddReplyPageState extends State<AddReplyPage> {

  late TextEditingController replyContentController;
  late FocusNode replyContentNode;
  GlobalKey<FormState> formKey = GlobalKey();
  late Reply _newReply;

  @override
  void initState() {
    replyContentController = TextEditingController();
    replyContentNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    replyContentController.dispose();
    replyContentNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        centerTitle: false,
        leading: IconButton(
          onPressed: () => getRoute.pop(),
          icon: Icon(
            Icons.clear,
            color: mainColor,
          ),
        ),
        title: Text(
          "Add Reply",
        ),
        actions: [
          TextButton(
            onPressed: () async {
              if (formKey.currentState!.validate()) {
                utils.loadToast();
                _newReply = Reply(
                  creatorUsername: qp.username!,
                  creatorId: qp.id!,
                  createdAt: DateTime.now(),
                  postId: widget.post.postId,
                  replyId: Uuid().v4(),
                  content: replyContentController.text.trim(),
                );
                await apiManager.createReply(_newReply);
                replyContentController.clear();
                getRoute.pop();
                getRoute.pop();
              }
            },
            child: Text(
              TextData.reply,
              style: TextStyle(
                color: appIconColor,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 24.0),
                child: Text(
                  widget.post.title,
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
              CommonWidgets.divider(),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 0.0),
                child: TextFormField(
                  controller: replyContentController,
                  focusNode: replyContentNode,
                  textInputAction: TextInputAction.newline,
                  maxLines: 15,
                  textCapitalization: TextCapitalization.sentences,
                  keyboardType: TextInputType.multiline,
                  maxLength: 1000,
                  validator: (String? current) {
                    if (current == null || current.trim().isEmpty) {
                      return "Content cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: TextData.replyContent,
                  ),
                ),
              ),
              CommonWidgets.divider(),
            ],
          ),
        ),
      ),
    );
  }
}
