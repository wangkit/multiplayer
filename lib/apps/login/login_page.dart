import 'package:flutter/material.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/button/long_elevated_button.dart';
import 'package:storyteller/widgets/textfield/email_textfield.dart';
import 'package:storyteller/widgets/textfield/password_textfield.dart';


class LoginPage extends StatefulWidget {

  LoginPage({
    Key? key,
  }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  late TextEditingController emailController;
  late TextEditingController passwordController;
  late FocusNode emailNode;
  late FocusNode passwordNode;
  GlobalKey<FormState> formKey = GlobalKey();

  @override
  void initState() {
    emailController = TextEditingController();
    passwordController = TextEditingController();
    emailNode = FocusNode();
    passwordNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    emailNode.dispose();
    passwordNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: Text(
          myAppName,
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 30,),
                ClipRRect(
                  borderRadius: BorderRadius.circular(200),
                  child: Image.asset(
                    "assets/placeholder.png",
                    width: 200,
                    height: 200,
                  ),
                ),
                SizedBox(height: 30,),
                EmailTextField(
                  controller: emailController,
                  focusNode: emailNode,
                  nextFocusNode: passwordNode,
                  borderRadius: commonRadius,
                ),
                SizedBox(height: 5,),
                PasswordTextField(
                  controller: passwordController,
                  focusNode: passwordNode,
                  nextFocusNode: emailNode,
                  borderRadius: commonRadius,
                ),
                SizedBox(height: 30,),
                LongElevatedButton(
                  onPressed: () async {
                    if (formKey.currentState!.validate()) {
                      await apiManager.signIn(emailController.text, passwordController.text);
                    }
                  },
                  title: TextData.signIn,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
