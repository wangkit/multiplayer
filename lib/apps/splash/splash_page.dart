import 'dart:async';
import 'dart:convert';
import 'package:animate_do/animate_do.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:storyteller/apps/chat/chat_page.dart';
import 'package:storyteller/apps/games/game_details_page.dart';
import 'package:storyteller/apps/home/main/main_page.dart';
import 'package:storyteller/apps/login/login_page.dart';
import 'package:storyteller/apps/posts/post_detail_page.dart';
import 'package:storyteller/apps/splash/soy_splash_page.dart';
import 'package:storyteller/network/api_services.dart';
import 'package:storyteller/page_route/global_route.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/analysis.dart';
import 'package:storyteller/resources/values/discovery_value.dart';
import 'package:storyteller/resources/values/pref_key.dart';
import 'package:storyteller/resources/values/remote_config_key.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/extensions/string_extensions.dart';
import 'dart:io';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/models/env.dart';

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  /// This is basically useless now since we have getInitialMessage at our disposal

  print("Handling a background message: ${message.data}");
}

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  Future<void> initSharedPreference() async {
    await utils.loadPref();
    await commonUtils.init();
    if (qp.id!.isNotEmpty) {
      DocumentSnapshot _doc = await firestore.collection(FirestoreCollections.users).doc(qp.id!).get();
      if (_doc.exists) {
        qp = UserProfile.fromMapToProfile(_doc.data() as Map);
      } else {
        utils.logoutClearData();
      }
    }
  }

  Future<bool> initDynamicLink() async {

    Future<bool> _handleLink(Uri dl) async {
      if (dl.queryParameters.containsKey(PostCollection.postId)) {
        String _postId = dl.queryParameters[PostCollection.postId]!;
        DocumentSnapshot _result = await firestore.collection(FirestoreCollections.posts).doc(_postId).get();
        if (_result.exists) {
          Post _thisPost = Post.fromMapToPost(_result.data() as Map);
          getRoute.navigateToAndPopAll(MainPage());
          getRoute.navigateTo(
            PostDetailPage(
              post: _thisPost,
              refreshParent: null,
            ),
          );
          return false;
        }
      }
      if (dl.queryParameters.containsKey(GameCollection.gameId)) {
        String _thisGameId = dl.queryParameters[GameCollection.gameId]!;
        DocumentSnapshot _result = await firestore.collection(FirestoreCollections.games).doc(_thisGameId).get();
        if (_result.exists) {
          Game _thisGame = Game.fromMapToGame(_result.data() as Map);
          getRoute.navigateToAndPopAll(MainPage());
          getRoute.navigateTo(
            GameDetailsPage(game: _thisGame),
          );
          return false;
        }
      }
      return true;
    }

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData? dynamicLink) async {
          if (dynamicLink != null) {
            return await _handleLink(dynamicLink.link);
          }
          return true;
        },
        onError: (OnLinkErrorException e) async {
          debugPrint('onLinkError');
          debugPrint(e.message);
        }
    );

    final PendingDynamicLinkData? data = await FirebaseDynamicLinks.instance.getInitialLink();
    if (data != null) {
      return await _handleLink(data.link);
    }
    return true;
  }

  void fetchRemoteListValues() {
    if (!kIsWeb) {
      try {
        gamingPlatforms = json.decode(remoteConfig!.getValue(RemoteConfigKey.gamingPlatforms).asString())[RemoteConfigKey.gamingPlatforms].cast<String>();
        pcPlatforms = json.decode(remoteConfig!.getValue(RemoteConfigKey.pcPlatforms).asString())[RemoteConfigKey.pcPlatforms].cast<String>();
        xboxPlatforms = json.decode(remoteConfig!.getValue(RemoteConfigKey.xboxPlatforms).asString())[RemoteConfigKey.xboxPlatforms].cast<String>();
        mobilePlatforms = json.decode(remoteConfig!.getValue(RemoteConfigKey.mobilePlatforms).asString())[RemoteConfigKey.mobilePlatforms].cast<String>();
        nintendoPlatforms = json.decode(remoteConfig!.getValue(RemoteConfigKey.nintendoPlatforms).asString())[RemoteConfigKey.nintendoPlatforms].cast<String>();
        psPlatforms = json.decode(remoteConfig!.getValue(RemoteConfigKey.psPlatforms).asString())[RemoteConfigKey.psPlatforms].cast<String>();
        integrationGif = remoteConfig!.getString(RemoteConfigKey.integrationGif);
        gamingPlatforms.forEach((String platformName) {
          switch (platformName) {
            case "PC":
              gamingMaps[platformName] = pcPlatforms;
              break;
            case "Mobile":
              gamingMaps[platformName] = mobilePlatforms;
              break;
            case "Playstation":
              gamingMaps[platformName] = psPlatforms;
              break;
            case "Xbox":
              gamingMaps[platformName] = xboxPlatforms;
              break;
            default:
              gamingMaps[platformName] = nintendoPlatforms;
          }
        });
      } catch (e) {
        debugPrint("Failed to fetch: $e");
      }
    }
  }

  void _initCalls() {
    /// Step one: Init instances and fetch remote
    apiBaseUrl = env!.baseUrl;
    fetchRemoteListValues();
    bool needForceUpdate = false;
    WidgetsFlutterBinding.ensureInitialized();
    /// Step three: Init shared preference
    initSharedPreference().then((_) {
      Future.wait([
        PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
          /// Using packageInfo.appName here will get a null value for iOS.
          /// Since we will not change the app name in the future, simply hardcoding the app name here solves the problem.
          appName = myAppName;
          versionCode = int.parse(packageInfo.buildNumber);
          versionName = packageInfo.version;
          int installedVersionNumber = int.parse(versionName.toString().trim().replaceAll(".", ""));
          int requiredVersionNumber = int.parse(remoteConfig!.getString(RemoteConfigKey.minimumVersion).toString().trim().replaceAll(".", ""));
          needForceUpdate = installedVersionNumber < requiredVersionNumber && remoteConfig!.getBool(RemoteConfigKey.enforceMinimumVersion);
        }),
      ]).then((_) {
        if (!needForceUpdate) {
          /// Step nine: Navigate to forum app.main page if myProfile is got and this app does not need to update
          SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) async {
            /// Init firebase messaging instance
            firebaseMessaging = FirebaseMessaging.instance;
            /// Register firebase permission moved to main page after logged in
            FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
            FirebaseMessaging.onMessage.listen((RemoteMessage message) {
              print('Got a message whilst in the foreground!');
              print('Message data: ${message.data}');
              if (message.notification != null) {
                print('Message also contained a notification: ${message.notification}');
              }
            });
            /// Go to dynamic link
            if (!qp.email!.isNullOrEmpty && !qp.authPw!.isNullOrEmpty && !qp.id!.isNullOrEmpty) {
              await apiManager.signInToFirebase();
              /// Check if this is opened from a notification
              RemoteMessage? initialMessage = await FirebaseMessaging.instance.getInitialMessage();
              if (initialMessage != null && initialMessage.data.containsKey("screen")) {
                String _targetScreen = initialMessage.data["screen"];
                String? _gameId;
                switch (_targetScreen) {
                  case "gameDetails":
                    getRoute.navigateToAndPopAll(MainPage(
                      showSpotlight: false,
                    ));
                    apiManager.logAnalysis(Analysis.openAppByNotification);
                    _gameId = initialMessage.data[GameCollection.gameId];
                    var _thisGame = await firestore.collection(FirestoreCollections.games).doc(_gameId).get();
                    Game _joinedGame = Game.fromMapToGame(_thisGame.data()!);
                    getRoute.navigateTo(GameDetailsPage(game: _joinedGame));
                    getRoute.navigateTo(ChatPage(game: _joinedGame));
                    return;
                }
              }
            }
            bool _needContinueToMainPage = await initDynamicLink();
            if (_needContinueToMainPage) {
              /// If normal open, go to main page
              getRoute.navigateToAndPopAll(MainPage());
            }
          });
        } else if (needForceUpdate) {
//          showForceUpdateDialog();
        } else {
          utils.toast(TextData.connectionFailed, isWarning: true);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    return SoySplashPage(
      buildFunctions: () {
        deviceWidth = MediaQuery.of(context).size.width;
        deviceHeight = MediaQuery.of(context).size.height;
      },
      backgroundColorCode: utils.getHexCodeByColor(littleGrey),
      initFunctions: () => _initCalls(),
      assetPath: "assets/lottie/controller.json",
    );
  }
}