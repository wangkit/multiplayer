import 'package:animate_do/animate_do.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/enum/game_page_type.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/chat/chat_input_bar.dart';
import 'package:storyteller/widgets/chat/message_bubble.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';

class ChatPage extends StatefulWidget {

  final Game game;

  ChatPage({
    Key? key,
    required this.game,
  }) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> with AutomaticKeepAliveClientMixin<ChatPage> {

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  late Query query;
  late DocumentSnapshot _lastDocument;
  List<String> _selectedMessageId = [];
  late RefreshController _refreshController;
  late ScrollController _scrollController;

  @override
  void dispose() {
    _refreshController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _refreshController = RefreshController();
    _scrollController = ScrollController();
    query = firestore
        .collection(FirestoreCollections.messages)
        .doc(widget.game.gameId)
        .collection(FirestoreCollections.chats)
        .orderBy(ChatCollection.createdAt, descending: true)
        .limit(100);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      appBar: CustomAppBar(
        title: Text(
          widget.game.gameName + " - ${widget.game.platformProduct}",
        ),
        actions: [
          _selectedMessageId.length > 0 ? IconButton(
            onPressed: () {
              _selectedMessageId.forEach((messageId) {
                apiManager.deleteOneMessage(messageId, widget.game.gameId);
              });
              _selectedMessageId.clear();
              refreshThisPage();
            },
            icon: Icon(
                Icons.delete,
                color: mainColor
            ),
          ) : Container(),
        ],
      ),
      body: SafeArea(
        child: Column(
          children: [
            StreamBuilder<QuerySnapshot>(
              stream: query.snapshots(),
              builder: (context, snapshot) {

                List<MessageBubble> _messageBubbles = [];

                void _handleMessages(List? messages) {

                  if (messages == null || messages.length == 0) {
                    return;
                  }

                  if (messages.isNotEmpty) {

                    _lastDocument = messages.last;

                    for (QueryDocumentSnapshot message in messages) {

                      Map? _map = message.data() as Map;

                      final messageBubble = MessageBubble(
                        messageId: _map[ChatCollection.messageId],
                        message: _map[ChatCollection.message],
                        senderId: _map[ChatCollection.senderId],
                        senderName: _map[ChatCollection.senderName],
                        createdAt: timeUtils.getStringFromTimestamp(_map[ChatCollection.createdAt]),
                        isSelected: _selectedMessageId.contains(_map[ChatCollection.messageId]),
                        selectedMessages: _selectedMessageId,
                        notifyParent: refreshThisPage,
                        game: widget.game,
                      );

                      if (!_messageBubbles.contains(messageBubble)) {
                        _messageBubbles.add(messageBubble);
                      }
                    }
                  }
                }

                if (!snapshot.hasData || snapshot.data!.size == 0) {
                  return Flexible(
                    child: Center(
                      child: Text(
                        TextData.noChats,
                      ),
                    ),
                  );
                }

                _handleMessages(snapshot.data!.docs);

                return Expanded(
                  child: SmartRefresher(
                    controller: _refreshController,
                    reverse: true,
                    enablePullUp: true,
                    enablePullDown: false,
                    onLoading: () {
                      query.startAfterDocument(_lastDocument).get().then((result) {
                        final messages = result.docs;
                        _handleMessages(messages);
                        _refreshController.loadComplete();
                      });
                    },
                    child: ListView(
                      reverse: true,
                      controller: _scrollController,
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                      children: _messageBubbles,
                    ),
                  ),
                );
              },
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: SafeArea(
                child: ChatInputBar(game: widget.game,),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
