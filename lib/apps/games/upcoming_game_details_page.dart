import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable/expandable.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:soy_common/widgets/tag/tag.dart';
import 'package:storyteller/apps/add_game/add_game_page.dart';
import 'package:storyteller/apps/chat/chat_page.dart';
import 'package:storyteller/apps/home/main/main_page.dart';
import 'package:storyteller/apps/home/profile/profile_page.dart';
import 'package:storyteller/utils/extensions/string_extensions.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/enum/game_page_type.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/analysis.dart';
import 'package:storyteller/resources/values/pref_key.dart';
import 'package:storyteller/resources/values/remote_config_key.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_with_email.dart';
import 'package:storyteller/utils/dialog/binary_dialog.dart';
import 'package:storyteller/utils/dialog/update_cover_dialog.dart';
import 'package:storyteller/widgets/animations/faded_indexed_stack.dart';
import 'package:storyteller/widgets/animations/open_container_wrapper.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/button/bottom_sheet_button.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/count_down/count_down.dart';
import 'package:storyteller/widgets/bubble/game_bubble.dart';
import 'package:storyteller/widgets/image/file_thumbnail.dart';
import 'package:storyteller/widgets/image/network_thumbnail.dart';
import 'package:http/http.dart' as http;
import 'package:storyteller/widgets/button/long_elevated_button.dart';
import 'package:storyteller/widgets/search/search_game.dart';
import 'package:storyteller/widgets/webview/custom_web_view.dart';
import 'package:storyteller/widgets/youtube/custom_youtube_player.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class UpcomingGameDetailsPage extends StatefulWidget {

  final UpcomingGame upcoming;
  final double height;

  UpcomingGameDetailsPage({
    Key? key,
    required this.upcoming,
    this.height = 130,
  }) : super(key: key);

  @override
  _UpcomingGameDetailsPageState createState() => _UpcomingGameDetailsPageState();
}

class _UpcomingGameDetailsPageState extends State<UpcomingGameDetailsPage> with AutomaticKeepAliveClientMixin<UpcomingGameDetailsPage> {

  GlobalKey<CustomYoutubePlayerState> _playerKey = GlobalKey();
  late Color _thisColor;

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    _thisColor = utils.convertStrToColor(widget.upcoming.gameName);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: widget.upcoming.gameVideo.isEmpty ? CustomAppBar() : CustomAppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () => getRoute.truePop(),
          icon: Icon(
              Icons.arrow_back,
              color: Colors.white
          ),
        ),
        actions: [
          kIsWeb ? Container() : StatefulBuilder(
            builder: (ctx, customState) {
              return IconButton(
                icon: Icon(
                  isVideoStartMuted ? Icons.volume_off : Icons.volume_up,
                  color: Colors.white,
                ),
                onPressed: _playerKey.currentState != null ? _playerKey.currentState!.getIsPlayerReady() ? () {
                  isVideoStartMuted
                      ? _playerKey.currentState!.unMute()
                      : _playerKey.currentState!.mute();
                  customState(() {
                    isVideoStartMuted = !isVideoStartMuted;
                  });
                  utils.saveBoo(PrefKey.isVideoStartMuted, isVideoStartMuted);
                } : null : null,
              );
            },
          ),
        ],
      ),
      body: SafeArea(
        top: false,
        child: Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              getRoute.navigateTo(CustomWebView(
                link: widget.upcoming.gameLink,
              ));
            },
            child: Icon(
              Icons.open_in_browser_outlined,
              color: Colors.white,
            ),
          ),
          body: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                widget.upcoming.gameVideo.isEmpty ? SafeArea(
                  child: SizedBox(),
                ) : CustomYoutubePlayer(
                  key: _playerKey,
                  videoId: widget.upcoming.gameVideo,
                ),
                Container(
                  width: deviceWidth,
                  child: Row(
                    mainAxisAlignment: kIsWeb ? MainAxisAlignment.center : MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 8, bottom: 8, left: 10),
                        child: Container(
                          height: widget.height + 30,
                          child: AspectRatio(
                            aspectRatio: 10.4 / 16,
                            child: NetworkThumbnail(
                              imageUrl: widget.upcoming.gameCover,
                              radius: 6,
                              text: '',
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        child: Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Flexible(
                                child: Text(
                                  widget.upcoming.gameName,
                                  textAlign: TextAlign.center,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: Theme.of(context).textTheme.subtitle1!.merge(
                                    TextStyle(
                                      fontWeight: FontWeight.bold,
                                    )
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Flexible(
                                child: Wrap(
                                  clipBehavior: Clip.antiAlias,
                                  alignment: WrapAlignment.start,
                                  runAlignment: WrapAlignment.start,
                                  children: List.generate(widget.upcoming.gameTheme.length > 4 ? 4 : widget.upcoming.gameTheme.length, (int index) {

                                    String _thisTheme = widget.upcoming.gameTheme[index];

                                    return CommonWidgets.tag(
                                      _thisTheme,
                                      color: _thisColor,
                                    );
                                  }),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                CommonWidgets.divider(),
                CommonWidgets.gameDetailsTitle(
                  TextData.summary,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                  child: ExpandableNotifier(
                    child: ScrollOnExpand(
                      child: Expandable(
                        collapsed: ExpandableButton(
                          child: Text(
                            widget.upcoming.gameSummary,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 3,
                          ),
                        ),
                        expanded: ExpandableButton(
                          child: Text(
                            widget.upcoming.gameSummary,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 10,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                CommonWidgets.divider(),
                CommonWidgets.gameDetailsTitle(
                  TextData.approximateReleaseDate,
                ),
                Flexible(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                    child: Text(
                      timeUtils.getParsedYearMonthDay(widget.upcoming.releaseDate),
                    ),
                  ),
                ),
                CommonWidgets.divider(),
                CommonWidgets.gameDetailsTitle(
                  TextData.platform,
                ),
                Flexible(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Wrap(
                        alignment: WrapAlignment.start,
                        runAlignment: WrapAlignment.start,
                        children: List.generate(widget.upcoming.gamePlatforms.length, (int index) {

                          return CommonWidgets.tag(
                            widget.upcoming.gamePlatforms[index],
                            color: _thisColor,
                          );
                        }),
                      ),
                    ),
                  ),
                ),
                CommonWidgets.divider(),
                Container(
                  height: 150,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
