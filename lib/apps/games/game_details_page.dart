import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable/expandable.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:soy_common/widgets/tag/tag.dart';
import 'package:storyteller/apps/add_game/add_game_page.dart';
import 'package:storyteller/apps/chat/chat_page.dart';
import 'package:storyteller/apps/home/main/main_page.dart';
import 'package:storyteller/apps/home/profile/profile_page.dart';
import 'package:storyteller/utils/extensions/string_extensions.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/enum/game_page_type.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/analysis.dart';
import 'package:storyteller/resources/values/pref_key.dart';
import 'package:storyteller/resources/values/remote_config_key.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_with_email.dart';
import 'package:storyteller/utils/dialog/binary_dialog.dart';
import 'package:storyteller/utils/dialog/update_cover_dialog.dart';
import 'package:storyteller/widgets/animations/faded_indexed_stack.dart';
import 'package:storyteller/widgets/animations/open_container_wrapper.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/button/bottom_sheet_button.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/count_down/count_down.dart';
import 'package:storyteller/widgets/bubble/game_bubble.dart';
import 'package:storyteller/widgets/image/file_thumbnail.dart';
import 'package:storyteller/widgets/image/network_thumbnail.dart';
import 'package:http/http.dart' as http;
import 'package:storyteller/widgets/button/long_elevated_button.dart';
import 'package:storyteller/widgets/search/search_game.dart';
import 'package:storyteller/widgets/youtube/custom_youtube_player.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class GameDetailsPage extends StatefulWidget {

  final Game game;
  final double height;

  GameDetailsPage({
    Key? key,
    required this.game,
    this.height = 130,
  }) : super(key: key);

  @override
  _GameDetailsPageState createState() => _GameDetailsPageState();
}

class _GameDetailsPageState extends State<GameDetailsPage> with AutomaticKeepAliveClientMixin<GameDetailsPage> {

  GlobalKey<CustomYoutubePlayerState> _playerKey = GlobalKey();

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  bool _hasStarted() {
    return widget.game.startTime.isBefore(DateTime.now());
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: widget.game.gameVideo == null || widget.game.gameVideo!.isEmpty ? CustomAppBar() : CustomAppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () => getRoute.truePop(),
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white
          ),
        ),
        actions: [
          kIsWeb ? Container() : StatefulBuilder(
            builder: (ctx, customState) {
              return IconButton(
                icon: Icon(
                  isVideoStartMuted ? Icons.volume_off : Icons.volume_up,
                  color: Colors.white,
                ),
                onPressed: _playerKey.currentState != null ? _playerKey.currentState!.getIsPlayerReady() ? () {
                  isVideoStartMuted
                      ? _playerKey.currentState!.unMute()
                      : _playerKey.currentState!.mute();
                  customState(() {
                    isVideoStartMuted = !isVideoStartMuted;
                  });
                  utils.saveBoo(PrefKey.isVideoStartMuted, isVideoStartMuted);
                } : null : null,
              );
            },
          ),
        ],
      ),
      body: SafeArea(
        top: false,
        child: FadeIn(
          child: StreamBuilder<DocumentSnapshot>(
            stream: firestore
                .collection(FirestoreCollections.games)
                .doc(widget.game.gameId)
                .snapshots(),
            builder: (context, snapshot) {

              if (!snapshot.hasData || !snapshot.data!.exists)  {
                return Center(
                  child: Text(
                    TextData.noGames,
                  ),
                );
              }

              Game _thisGame = Game.fromMapToGame(snapshot.data!.data() as Map);

              return Scaffold(
                floatingActionButton: _thisGame.playerIds.contains(qp.id!) ? Padding(
                  padding: EdgeInsets.only(bottom: 60.0),
                  child: FloatingActionButton(
                    onPressed: () {
                      getRoute.navigateTo(ChatPage(
                        game: _thisGame,
                      ));
                    },
                    child: Icon(
                      MdiIcons.chatOutline,
                      color: Colors.white,
                    ),
                  ),
                ) : null,
                body: Stack(
                  children: [
                    Positioned(
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            widget.game.gameVideo == null || widget.game.gameVideo!.isEmpty ? SafeArea(
                              child: SizedBox(),
                            ) : CustomYoutubePlayer(
                              key: _playerKey,
                              videoId: widget.game.gameVideo!,
                            ),
                            Container(
                              width: deviceWidth,
                              height: widget.height + 30,
                              child: Row(
                                mainAxisAlignment: kIsWeb ? MainAxisAlignment.center : MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(top: 8, bottom: 8, left: 10),
                                    child: AspectRatio(
                                      aspectRatio: 10.4 / 16,
                                      child: GestureDetector(
                                        onTap: widget.game.creatorId == qp.id! ? () {
                                          getUpdateCoverDialog(widget.game);
                                        } : null,
                                        child: NetworkThumbnail(
                                          imageUrl: widget.game.gameCover == null || widget.game.gameCover!.isEmpty ? gameCoverLink : widget.game.gameCover!.last,
                                          radius: 6,
                                          text: '',
                                          onError: (StackTrace stacktrace) {
                                            if (stacktrace.toString().contains("MultiImageStreamCompleter.<anonymous closure>")) {
                                              apiManager.removeCover(widget.game.gameCover!.last, widget.game.gameId);
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    child: Center(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Flexible(
                                            child: Text(
                                              _thisGame.gameName,
                                              textAlign: TextAlign.center,
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 2,
                                              style: Theme.of(context).textTheme.subtitle1!.merge(
                                                TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                )
                                              ),
                                            ),
                                          ),
                                          Flexible(
                                            child: Text(
                                              timeUtils.getParsedDateTimeLongFormatWithHourMin(widget.game.startTime) + (widget.game.startTime.isBefore(DateTime.now()) ? " (Started)" : ""),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          Text(
                                            utils.determineNeedS(_thisGame.endTime.difference(_thisGame.startTime).inMinutes, TextData.minute),
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            CommonWidgets.divider(),
                            CommonWidgets.gameDetailsTitle(
                              TextData.summary,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                              child: ExpandableNotifier(
                                child: ScrollOnExpand(
                                  child: Expandable(
                                    collapsed: ExpandableButton(
                                      child: Text(
                                        _thisGame.gameSummary == null ? TextData.noGameSummary : _thisGame.gameSummary!,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 3,
                                      ),
                                    ),
                                    expanded: ExpandableButton(
                                      child: Text(
                                        _thisGame.gameSummary == null ? TextData.noGameSummary : _thisGame.gameSummary!,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 10,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            CommonWidgets.divider(),
                            CommonWidgets.gameDetailsTitle(
                              TextData.platform,
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: EdgeInsets.only(left: 12.0, top: 12.0, bottom: 12.0),
                                child: Text(
                                  widget.game.gamePlatform + " - " + widget.game.platformProduct,
                                ),
                              ),
                            ),
                            CommonWidgets.divider(),
                            Column(
                              children: List.generate(_thisGame.playerIds.length == 0 ? 1 : _thisGame.playerIds.length, (index) {

                                Widget _profileWidget(UserProfile _thisProfile) {

                                  String _id = "";
                                  String _platformAction = widget.game.gamePlatform;
                                  switch (widget.game.gamePlatform) {
                                    case ConfigCollection.nintendo:
                                      _id = _thisProfile.nintendoFriendCode!;
                                      break;
                                    case ConfigCollection.playstation:
                                      _id = _thisProfile.psn!;
                                      break;
                                    case ConfigCollection.xbox:
                                      _id = _thisProfile.xboxGamerTag!;
                                      break;
                                    case ConfigCollection.pc:
                                      if (widget.game.platformProduct == ConfigCollection.steam) {
                                        _id = _thisProfile.steamFriendCode!;
                                        _platformAction = ConfigCollection.steam;
                                      }
                                      break;
                                  }

                                  return InkWell(
                                    splashColor: utils.convertStrToColor(_thisProfile.id!),
                                    onTap: () {
                                      /// If is myself, then navigate to profile directly
                                      if (_thisProfile.id == qp.id!) {
                                        getRoute.navigateTo(ProfilePage(
                                          userId: qp.id!,
                                        ));
                                        return;
                                      }
                                      /// Else show bottom sheet
                                      getGenericBottomSheet([
                                        BottomSheetButton(
                                          onTap: () {
                                            getRoute.navigateTo(ProfilePage(
                                              userId: _thisProfile.id!,
                                            ));
                                          },
                                          iconData: Icons.person_outlined,
                                          label: TextData.profile,
                                        ),
                                        BottomSheetButton(
                                          onTap: () {
                                            utils.addFriendIntegration(_platformAction, _id);
                                          },
                                          iconData: utils.getGamePlatformIcon(_platformAction),
                                          label: TextData.sendFriendRequest,
                                        ),
                                      ]);
                                    },
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 24),
                                          child: NetworkThumbnail(
                                            imageUrl: _thisProfile.photoUrl!,
                                            hero: _thisProfile.id,
                                            text: "",
                                          ),
                                        ),
                                        Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Text(
                                              userTag + _thisProfile.username!,
                                            ),
                                            _id.isEmpty ? CommonWidgets.emptyBox() : Text(
                                              _id,
                                            ),
                                          ],
                                        ),
                                        [qp.id!, _thisGame.creatorId].contains(_thisProfile.id) ? CommonWidgets.tag(
                                          _thisProfile.id == qp.id! ? TextData.myself : TextData.gameCreator,
                                          outsidePadding: EdgeInsets.only(left: 24.0),
                                          color: unfocusedColor,
                                        ) : Container(),
                                      ],
                                    ),
                                  );
                                }

                                return Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    index == 0 ? CommonWidgets.gameDetailsTitle(
                                      "${_thisGame.playerIds.length} / ${_thisGame.maxPlayers} ${utils.determineNeedS(_thisGame.playerIds.length, TextData.player, showNumber: false)}",
                                    ) : SizedBox(),
                                    _thisGame.playerIds.length > 0 ? _thisGame.playerIds[index] == qp.id! ? _profileWidget(qp) : FutureBuilder(
                                      future: firestore.collection(FirestoreCollections.users).doc(_thisGame.playerIds[index]).get(),
                                      builder: (ctx, AsyncSnapshot<DocumentSnapshot> snapshot) {

                                        if (!snapshot.hasData) {
                                          return CommonWidgets.loading();
                                        }

                                        return _profileWidget(UserProfile.fromMapToProfile(snapshot.data!.data() as Map));
                                      },
                                    ) : SizedBox(),
                                    _thisGame.playerIds.length > 0 ? CommonWidgets.divider() : SizedBox(),
                                  ],
                                );
                              }),
                            ),
                            Container(
                              height: 150,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          width: deviceWidth,
                          color: littleGrey,
                          child: Padding(
                            padding: EdgeInsets.all(8.0),
                            child: LongElevatedButton(
                              isEnabled: !_hasStarted(),
                              borderRadius: commonRadius / 2,
                              isReverseColor: _hasStarted() || _thisGame.playerIds.contains(qp.id!),
                              onPressed: () async {
                                await utils.joinQuitGame(_thisGame, refreshThisPage);
                              },
                              title: _hasStarted() ? TextData.gameStarted : _thisGame.playerIds.contains(qp.id!) ? TextData.quit : TextData.join,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
