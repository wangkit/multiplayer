import 'package:animate_do/animate_do.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storyteller/configs/app_config.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/enum/game_page_type.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/bubble/game_bubble.dart';

class GamesPage extends StatefulWidget {

  final GamePageType type;
  final String targetId;
  final String title;

  GamesPage({
    Key? key,
    required this.type,
    required this.targetId,
    required this.title,
  }) : super(key: key);

  @override
  _GamesPageState createState() => _GamesPageState();
}

class _GamesPageState extends State<GamesPage> with AutomaticKeepAliveClientMixin<GamesPage> {

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  late RefreshController _refreshController;
  late Stream<QuerySnapshot> _stream;
  late Query _query;
  DocumentSnapshot? _lastDocument;
  late ScrollController _scrollController;
  List<GameBubble> _gameBubbles = [];
  Map<String, int> _gameMaps = {};
  late String _emptyMessage;

  /// This is specifically created to handle how to hide quitted game in Joined Game Page
  List<String> _quittedGameInJoinedGames = [];

  @override
  void dispose() {
    _refreshController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _refreshController = RefreshController();
    _scrollController = ScrollController(keepScrollOffset: true);
    switch (widget.type) {
      case GamePageType.created:
        _query = firestore
            .collection(FirestoreCollections.games)
            .where(GameCollection.creatorId, isEqualTo: widget.targetId)
            .orderBy(GameCollection.createdAt)
            .limit(GameCollection.limit);
        _emptyMessage = "Create a new game to start seeing things here";
        break;
      case GamePageType.joined:
        _query = firestore
            .collection(FirestoreCollections.games)
            .where(GameCollection.playerIds, arrayContains: widget.targetId)
            .orderBy(GameCollection.startTime)
            .limit(GameCollection.limit);
        _emptyMessage = "Join a game to start seeing things here";
        break;
      case GamePageType.mutual:
        _query = firestore
            .collection(FirestoreCollections.games)
            .where(GameCollection.playerIds, arrayContains: widget.targetId)
            .orderBy(GameCollection.startTime)
            .limit(GameCollection.limit);
        _emptyMessage = "No games in common";
        break;
    }
    _stream = _query.snapshots();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      backgroundColor: littleGrey,
      appBar: CustomAppBar(
        title: Text(
          widget.title,
        ),
      ),
      body: SafeArea(
        child: StreamBuilder<QuerySnapshot>(
          stream: _stream,
          builder: (context, snapshot) {

            /// This function is to handle onQuit at one single place
            GameBubble _buildGameBubble(Game _gameData) {
              return GameBubble(
                game: _gameData,
                onQuit: widget.type == GamePageType.joined ? () {
                  _quittedGameInJoinedGames.add(_gameData.gameId);
                  refreshThisPage();
                } : null,
              );
            }

            void _handleGames(List? games) {

              if (games == null || games.length == 0) {
                return;
              }

              for (QueryDocumentSnapshot game in games) {

                Game _game = Game.fromMapToGame(game.data() as Map);

                void _updateAndAddGame() {
                  if (!_gameMaps.containsKey(_game.gameId)) {
                    _gameBubbles.add(_buildGameBubble(_game));
                    _gameMaps[_game.gameId] = _gameBubbles.length - 1;
                    _lastDocument = game;
                  } else {
                    try {
                      _gameBubbles[_gameMaps[_game.gameId]!] = _buildGameBubble(_game);
                    } on RangeError catch (e) {
                      debugPrint("Quitting game in joined game page");
                    }
                  }
                }

                if (widget.type == GamePageType.mutual) {
                  if (_game.playerIds.contains(qp.id)) {
                    _updateAndAddGame();
                  }
                } else {
                  _updateAndAddGame();
                }
              }
            }

            if (!snapshot.hasData || snapshot.data!.docs.length == 0) {
              return CommonWidgets.emptyMessage(_emptyMessage);
            }

            _handleGames(snapshot.data!.docs);

            return SmartRefresher(
              controller: _refreshController,
              scrollController: _scrollController,
              reverse: false,
              enablePullUp: true,
              enablePullDown: false,
              onLoading: () {
                if (_lastDocument != null) {
                  _query
                      .startAfterDocument(_lastDocument!)
                      .snapshots().listen((QuerySnapshot _loadSnapshot) {
                    _handleGames(_loadSnapshot.docs);
                    _refreshController.loadComplete();
                    if (_loadSnapshot.size > 0) {
                      refreshThisPage();
                    }
                  });
                }
              },
              child: ListView.builder(
                controller: _scrollController,
                itemCount: _gameBubbles.length,
                itemBuilder: (ctx, index) {
                  return _quittedGameInJoinedGames.contains(_gameBubbles[index].game.gameId) ? CommonWidgets.emptyBox() : _gameBubbles[index];
                },
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
