import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/discovery_value.dart';
import 'package:storyteller/resources/values/pref_key.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/custom_date_picker/custom_date_picker.dart';
import 'package:storyteller/widgets/custom_datetime_picker_formfield/custom_datetime_picker_formfield.dart';
import 'package:storyteller/widgets/custom_time_picker/custom_time_picker.dart';
import 'package:storyteller/widgets/feature_discovery/custom_feature_overlay.dart';
import 'package:storyteller/widgets/tag/tag.dart';
import 'package:storyteller/widgets/button/long_elevated_button.dart';
import 'package:storyteller/widgets/textfield/email_textfield.dart';
import 'package:storyteller/widgets/textfield/password_textfield.dart';
import 'package:uuid/uuid.dart';
import 'package:validators2/validators.dart';

class AddGamePage extends StatefulWidget {

  final String targetPlatform;
  final bool newUserPage;

  AddGamePage({
    Key? key,
    this.newUserPage = false,
    required this.targetPlatform,
  }) : super(key: key);

  @override
  _AddGamePageState createState() => _AddGamePageState();
}

class _AddGamePageState extends State<AddGamePage> {

  late TextEditingController gameNameController;
  late TextEditingController startDateController;
  late TextEditingController startTimeController;
  late FocusNode gameNameNode;
  late FocusNode startDateNode;
  late FocusNode startTimeNode;
  late int gameDuration;
  late int maxPlayers;
  GlobalKey<FormState> formKey = GlobalKey();
  late Game newGame;
  final dateFormat = DateFormat("EEE, d MMM yyyy");
  final timeFormat = DateFormat("HH:mm");
  Widget d = const Divider(
    thickness: 2,
  );

  @override
  void initState() {
    gameNameController = TextEditingController();
    startDateController = TextEditingController(text: timeUtils.getParsedDateTimeLongFormat(DateTime.now().add(Duration(hours: 1))));
    startTimeController = TextEditingController(text: timeUtils.getParsedDateTimeHourMin(DateTime.now().add(Duration(hours: 1))));
    gameNameNode = FocusNode();
    startDateNode = FocusNode();
    startTimeNode = FocusNode();
    gameDuration = 1;
    newGame = Game(
      creatorUsername: qp.username!,
      creatorId: qp.id!,
      gameName: '',
      maxPlayers: 2,
      startTime: DateTime.now(),
      createdAt: DateTime.now(),
      platformProduct: '',
      gamePlatform: widget.targetPlatform,
      playerIds: [qp.id!],
      endTime:  DateTime.now(),
      players: [Game.fromProfileToMap(qp)],
      gameId: Uuid().v4(),
      gameCover: [],
      gameGenre: [],
      gameTheme: []
    );
    maxPlayers = newGame.maxPlayers;
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((Duration duration) {
      FeatureDiscovery.discoverFeatures(
        context,
        const <String>{ // Feature ids for every feature that you want to showcase in order.
          DiscoveryValue.gameDurationDisplay,
          DiscoveryValue.maxNumberPlayers,
        },
      );
    });
  }

  @override
  void dispose() {
    gameNameController.dispose();
    startDateController.dispose();
    startTimeController.dispose();
    gameNameNode.dispose();
    startDateNode.dispose();
    startTimeNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.newUserPage ? null : CustomAppBar(
        centerTitle: false,
        leading: IconButton(
          onPressed: () => getRoute.pop(),
          icon: Icon(
            Icons.clear,
            color: mainColor,
          ),
        ),
        title: Text(
          "New ${widget.targetPlatform} Game",
        ),
        actions: [
          TextButton(
            onPressed: () async {
              if (newGame.platformProduct.isEmpty) {
                utils.toast(TextData.productEmpty, isWarning: true);
              } else if (formKey.currentState!.validate() && newGame.platformProduct.isNotEmpty) {
                utils.loadToast();
                utils.closeKeyboard();
                utils.loadPref();
                DateTime _gameStartDate = timeUtils.getParsedTimeLongFormat(startDateController.text);
                DateTime _gameStartTime = timeUtils.getParsedTimeHourMin(startTimeController.text);
                newGame.gameName = gameNameController.text.trim();
                newGame.startTime = DateTime(_gameStartDate.year, _gameStartDate.month, _gameStartDate.day, _gameStartTime.hour, _gameStartTime.minute);
                if (newGame.startTime.isBefore(DateTime.now())) {
                  getRoute.pop();
                  utils.toast(TextData.createStartBeforeNow, isWarning: true);
                  return;
                }
                newGame.endTime = newGame.startTime.add(Duration(hours: gameDuration));
                newGame.createdAt = DateTime.now().toUtc();
                Game? _hasNoGame = await apiManager.hasNoGameInThisPeriodOrIsBlocked(newGame);
                if (_hasNoGame == null) {
                  String _igdbName = await apiManager.checkGameName(newGame.gameName);
                  if (_igdbName.isEmpty) {
                    getRoute.pop();
                    utils.toast(TextData.noSuchGame, isWarning: true);
                    return;
                  }
                  await apiManager.createGame(newGame);
                  getRoute.pop();
                  getRoute.pop();
                  getRoute.pop();
                  if (addGameToCalendar) {
                    utils.createEvents(newGame);
                  }
                  utils.toast(TextData.createdSuccessfully);
                } else {
                  getRoute.pop();
                  utils.toast(TextData.alreadyHaveGame + ": ${utils.determineNeedS(_hasNoGame.endTime.difference(DateTime.now()).inMinutes, "minute",)}", isWarning: true);
                }
              }
            },
            child: Text(
              TextData.done,
              style: TextStyle(
                color: appIconColor,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
      body: StatefulBuilder(
        builder: (ctx, customState) {

          return SingleChildScrollView(
            child: Form(
              key: formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: widget.newUserPage ? MainAxisAlignment.center : MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 0.0),
                    child: TextFormField(
                      controller: gameNameController,
                      focusNode: gameNameNode,
                      maxLines: 1,
                      textCapitalization: TextCapitalization.words,
                      keyboardType: TextInputType.visiblePassword,
                      maxLength: 100,
                      validator: (String? current) {
                        if (current == null || current.trim().isEmpty) {
                          return "${TextData.gameName} cannot be empty";
                        } else if (!RegExp(r"^[a-zA-Z0-9 ]*$").hasMatch(current)) {
                          return "${TextData.gameName} must only contain digits or letters or spaces";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        counterText: "",
                        icon: Icon(
                          MdiIcons.disc,
                          color: unfocusedColor,
                          size: 36,
                        ),
                        labelText: TextData.gameName,
                        labelStyle: TextStyle(
                          color: Colors.black,
                        )
                      ),
                    ),
                  ),
                  CommonWidgets.divider(),
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 0.0),
                        child: Icon(
                          MdiIcons.gamepad,
                          color: unfocusedColor,
                          size: 30,
                        ),
                      ),
                      Flexible(
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 0.0),
                          child: Wrap(
                            runSpacing: 8,
                            children: gamingMaps[widget.targetPlatform]!.map((String product) {
                              return Padding(
                                padding: EdgeInsets.all(4.0),
                                child: Tag(
                                  key: Key(newGame.platformProduct),
                                  radius: commonRadius,
                                  horizontalPadding: 12,
                                  verticalPadding: 6,
                                  color: newGame.platformProduct == product ? appIconColor : unfocusedColor,
                                  text: product,
                                  onPressed: () {
                                    customState(() {
                                      newGame.platformProduct = product;
                                    });
                                  },
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                    ],
                  ),
                  CommonWidgets.divider(),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 3,
                        child: CustomDateTimeField(
                          format: dateFormat,
                          style: TextStyle(
                              color: mainColor
                          ),
                          resetIcon: null,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              MdiIcons.clockStart,
                              color: unfocusedColor,
                              size: 36,
                            ),
                          ),
                          validator: (DateTime? current) {
                            if (startDateController.text.isEmpty) {
                              return "Start date cannot be empty";
                            } else {
                              return null;
                            }
                          },
                          label: TextData.from,
                          controller: startDateController,
                          focusNode: startDateNode,
                          onShowPicker: (context, currentValue) async {
                            final date = await showCustomDatePicker(
                              context: context,
                              firstDate: DateTime.now(),
                              initialDate: currentValue ?? DateTime.now(),
                              lastDate: DateTime(DateTime.now().year + 100),
                            );
                            if (date != null) {
                              return date;
                            } else {
                              return currentValue;
                            }
                          },
                        ),
                      ),
                      Flexible(
                        child: CustomDateTimeField(
                          format: timeFormat,
                          style: TextStyle(
                              color: mainColor
                          ),
                          resetIcon: null,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                          ),
                          validator: (DateTime? current) {
                            if (startDateController.text.isEmpty) {
                              return "Start time cannot be empty";
                            } else {
                              return null;
                            }
                          },
                          label: "",
                          controller: startTimeController,
                          focusNode: startTimeNode,
                          onShowPicker: (context, currentValue) async {
                            final time = await showCustomTimePicker(
                              context: context,
                              initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now().add(Duration(hours: 1))),
                            );
                            if (time != null) {
                              return DateTimeField.convert(time);
                            } else {
                              return currentValue;
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 3,
                        child: Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Icon(
                                MdiIcons.clockEnd,
                                color: unfocusedColor,
                                size: 36,
                              ),
                            ),
                            Expanded(
                              child: Slider.adaptive(
                                value: gameDuration.toDouble(),
                                min: 1,
                                max: 24,
                                divisions: 24,
                                activeColor: appIconColor,
                                inactiveColor: unfocusedColor,
                                onChanged: (double value) {
                                  customState(() {
                                    gameDuration = value.toInt();
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      Flexible(
                        child: CustomFeatureOverlay(
                          id: DiscoveryValue.gameDurationDisplay,
                          backgroundColor: colorAccent,
                          title: TextData.gameDurationDiscovery,
                          description: TextData.gameDurationDescription,
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 0.0),
                            child: TextField(
                              enableInteractiveSelection: false,
                              enableSuggestions: false,
                              controller: TextEditingController(text: utils.determineNeedS(gameDuration, "hour"),),
                              readOnly: true,
                              onTap: () {
                                if (gameDuration < 24) {
                                  gameDuration++;
                                } else {
                                  gameDuration = 1;
                                }
                                customState(() {});
                              },
                              decoration: InputDecoration(
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  CommonWidgets.divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Padding(
                        padding: EdgeInsets.all(12.0),
                        child: Icon(
                          MdiIcons.account,
                          color: unfocusedColor,
                          size: 36,
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: NumberPicker(
                          value: maxPlayers,
                          axis: Axis.horizontal,
                          minValue: 2,
                          maxValue: 50,
                          selectedTextStyle: Theme.of(context).textTheme.headline6!.merge(
                              TextStyle(
                                color: appIconColor,
                                fontSize: 24,
                              )
                          ),
                          onChanged: (int value) {
                            customState(() {
                              maxPlayers = value;
                              newGame.maxPlayers = maxPlayers;
                            });
                          },
                        ),
                      ),
                      Flexible(
                        child: CustomFeatureOverlay(
                          id: DiscoveryValue.maxNumberPlayers,
                          backgroundColor: colorPrimaryDark,
                          title: TextData.maxPlayerDiscovery,
                          description: TextData.maxPlayerDescription,
                          child: GestureDetector(
                            onTap: () {
                              customState(() {
                                maxPlayers++;
                                newGame.maxPlayers = maxPlayers;
                              });
                            },
                            child: Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Text(
                                utils.determineNeedS(maxPlayers, "player", showNumber: false),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  CommonWidgets.divider(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
