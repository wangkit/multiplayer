import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:storyteller/apps/add_game/add_game_page.dart';
import 'package:storyteller/apps/home/profile/profile_page.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/values/discovery_value.dart';
import 'package:storyteller/resources/values/pref_key.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/feature_discovery/custom_feature_overlay.dart';
import 'package:storyteller/utils/extensions/string_extensions.dart';

class PlatformPage extends StatefulWidget {

  final bool isNewUser;

  PlatformPage({
    Key? key,
    this.isNewUser = false,
  }) : super(key: key);

  @override
  _PlatformPageState createState() => _PlatformPageState();
}

class _PlatformPageState extends State<PlatformPage> with AutomaticKeepAliveClientMixin<PlatformPage> {

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((Duration duration) {
      FeatureDiscovery.discoverFeatures(
        context,
        const <String>{
          DiscoveryValue.platform,
        },
      );
    });
    if (widget.isNewUser) {
      WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
        showModal(
          context: context,
          builder: (_) {
            return NetworkGiffyDialog(
              image: CachedNetworkImage(
                imageUrl: integrationGif,
              ),
              title: Text(
                TextData.integrationTitle,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
              description:Text(
                TextData.integrationEasy,
                textAlign: TextAlign.center,
              ),
              entryAnimation: EntryAnimation.BOTTOM,
              onOkButtonPressed: () {
                getRoute.pop();
                getRoute.navigateToAndReplace(ProfilePage(userId: qp.id!));
              },
            );
          },
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      appBar: CustomAppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        leading: widget.isNewUser ? null : IconButton(
          onPressed: () {
            getRoute.pop();
          },
          icon: Icon(
            Icons.clear,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: deviceHeight * 0.05,
                ),
                Text(
                  widget.isNewUser ? "Let's create your first game now" : "Select a platform",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline6,
                ),
                SizedBox(
                  height: 30,
                ),
                CustomFeatureOverlay(
                  id: DiscoveryValue.platform,
                  backgroundColor: appIconBlue,
                  title: TextData.platform.titleCase,
                  description: TextData.platformDiscover,
                  child: Center(
                    child: Wrap(
                      runSpacing: 30,
                      spacing: 20,
                      children: List.generate(gamingPlatforms.length, (index) {

                        String platform = gamingPlatforms[index];

                        return Container(
                          height: 150,
                          width: 150,
                          child: FittedBox(
                            child: Column(
                              children: [
                                FloatingActionButton(
                                  key: UniqueKey(),
                                  heroTag: UniqueKey(),
                                  elevation: 0,
                                  onPressed: () {
                                    getRoute.navigateToAndReplace(AddGamePage(targetPlatform: platform));
                                  },
                                  child: Icon(
                                    utils.getGamePlatformIcon(platform),
                                    color: CustomWhite,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  platform,
                                ),
                              ],
                            ),
                          ),
                        );
                      })
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
