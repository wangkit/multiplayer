import 'package:animate_do/animate_do.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:storyteller/apps/chat/chat_page.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/enum/game_page_type.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';

class MessagePage extends StatefulWidget {

  MessagePage({
    Key? key,
  }) : super(key: key);

  @override
  _MessagePageState createState() => _MessagePageState();
}

class _MessagePageState extends State<MessagePage> with AutomaticKeepAliveClientMixin<MessagePage> {

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  late Query query;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    query = firestore
        .collection(FirestoreCollections.messages)
        .where(MessagesCollection.users, arrayContains: qp.id!)
        .where(MessagesCollection.endTime, isGreaterThanOrEqualTo: DateTime.now())
        .orderBy(MessagesCollection.endTime, descending: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return SafeArea(
      child: StreamBuilder<QuerySnapshot>(
        stream: query.snapshots(),
        builder: (context, snapshot) {

          List<Widget> _rooms = [];

          void _handleMessages(List? rooms) {

            if (rooms == null || rooms.length == 0) {
              return;
            }
            for (QueryDocumentSnapshot room in rooms) {

              String gameId = (room.data() as Map)[MessagesCollection.gameId];

              Widget _thisWidget = StreamBuilder(
                stream: firestore.collection(FirestoreCollections.games).doc(gameId).snapshots(),
                builder: (ctx, AsyncSnapshot<DocumentSnapshot> snapshot) {

                  if (!snapshot.hasData || !snapshot.data!.exists) {
                    return Container();
                  }

                  Game _thisGame = Game.fromMapToGame(snapshot.data!.data() as Map);

                  return ListTile(
                    onTap: () {
                      getRoute.navigateTo(ChatPage(
                        game: _thisGame,
                      ));
                    },
                    title: Row(
                      children: [
                        Text(
                          _thisGame.gameName,
                        ),
                        CommonWidgets.separationDot(),
                        Flexible(
                          flex: 2,
                          child: Text(
                            _thisGame.platformProduct,
                          ),
                        ),
                        CommonWidgets.separationDot(),
                        Flexible(
                          flex: 2,
                          child: Text(
                            timeUtils.getParsedDateTimeHourMin(_thisGame.startTime),
                          ),
                        ),
                      ],
                    ),
                    subtitle: Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: StreamBuilder(
                        stream: firestore.collection(FirestoreCollections.messages)
                            .doc(_thisGame.gameId)
                            .collection(FirestoreCollections.chats)
                            .orderBy(ChatCollection.createdAt, descending: true)
                            .limit(1)
                            .snapshots(),
                        builder: (ctx, AsyncSnapshot<QuerySnapshot> snapshot) {

                          if (!snapshot.hasData || snapshot.data!.size == 0) {
                            return Text(
                              TextData.noMessageYet,
                            );
                          }

                          String _lastMessage = (snapshot.data!.docs.first.data() as Map)[ChatCollection.message];
                          DateTime _lastMessageCreatedAt = timeUtils.getDateTimeFromTimeStamp((snapshot.data!.docs.first.data() as Map)[ChatCollection.createdAt]).toLocal();

                          return Row(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                _lastMessage,
                              ),
                              Flexible(
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    timeUtils.getParsedDateTimeHourMin(_lastMessageCreatedAt),
                                  ),
                                ),
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                  );
                },
              );
              if (!_rooms.contains(_thisWidget)) {
                _rooms.add(_thisWidget);
              }
            }
          }

          if (!snapshot.hasData || snapshot.data!.docs.length == 0) {
            return Center(
              child: Text(
                TextData.noMessages,
              ),
            );
          }

          _handleMessages(snapshot.data!.docs);

          return FadeIn(
            child: ListView.separated(
              itemCount: _rooms.length,
              separatorBuilder: (ctx, index) {
                return CommonWidgets.divider();
              },
              itemBuilder: (ctx, index) {
                return _rooms[index];
              },
            ),
          );
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
