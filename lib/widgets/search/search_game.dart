import 'package:animate_do/animate_do.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/widgets/bubble/game_bubble.dart';

class SearchGame extends SearchDelegate<String> {
  SearchGame({required this.contextPage});

  BuildContext contextPage;

  @override
  String get searchFieldLabel => "Enter a game name";

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(
          Icons.clear,
        ),
        onPressed: () {
          query = "";
          showSuggestions(context);
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, "");
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: firestore
          .collection(FirestoreCollections.games)
          .where(GameCollection.gameName, isEqualTo: query)
          .limit(30)
          .orderBy(GameCollection.startTime, descending: true)
          .snapshots(),
      builder: (context, snapshot) {

        if (!snapshot.hasData || snapshot.data!.docs.length == 0) {
          return Align(
            alignment: Alignment.topCenter,
            child: ListTile(
              leading: Icon(
                Icons.videogame_asset,
              ),
              title: Text(
                "No matches",
              ),
            ),
          );
        }

        return FadeIn(
            child: ListView.builder(
              itemCount: snapshot.data!.docs.length,
              itemBuilder: (ctx, index) {
                return GameBubble(game: Game.fromMapToGame(snapshot.data!.docs[index].data() as Map));
              },
            )
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return FutureBuilder<QuerySnapshot>(
      future: firestore
          .collection(FirestoreCollections.games)
          .limit(3)
          .get(),
      builder: (context, snapshot) {

        if (!snapshot.hasData) {
          return Container();
        }

        return FadeIn(
          child: ListView.builder(
            itemCount: snapshot.data!.docs.length,
            itemBuilder: (ctx, index) {

              String _gameName = (snapshot.data!.docs[index].data() as Map)[GameCollection.gameName];

              return ListTile(
                onTap: () {
                  query = _gameName;
                  showResults(context);
                },
                trailing: Icon(
                  Icons.search,
                  color: unfocusedColor,
                ),
                title: Text(
                    _gameName,
                ),
              );
            },
          )
        );
      },
    );
  }
}