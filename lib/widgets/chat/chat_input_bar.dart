import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:uuid/uuid.dart';

class ChatInputBar extends StatefulWidget {

  final Game game;

  ChatInputBar({
    Key? key,
    required this.game,
  }) : super(key: key);

  @override
  _ChatInputBarState createState() => _ChatInputBarState();
}

class _ChatInputBarState extends State<ChatInputBar> {

  late TextEditingController _inputController;
  late FocusNode _inputFocusNode;

  @override
  void initState() {
    _inputController = TextEditingController();
    _inputFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _inputController.dispose();
    _inputFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Flexible(
                child: Material(
                  borderRadius: BorderRadius.circular(20.0),
                  color: unfocusedColor,
                  child: Row(
                    children: <Widget>[
                      Container(width: 8.0),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.all(4.0),
                          child: TextField(
                            focusNode: _inputFocusNode,
                            maxLines: null,
                            style: TextStyle(
                              color: Colors.white,
                            ),
                            keyboardType: TextInputType.multiline,
                            textInputAction: TextInputAction.newline,
                            controller: _inputController,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              counterText: null,
                              hintText: TextData.message,
                              hintStyle: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 8.0),
                child: FloatingActionButton(
                  key: UniqueKey(),
                  heroTag: UniqueKey(),
                  elevation: 0,
                  backgroundColor: appIconColor,
                  onPressed: () {
                    if (_inputController.text.isNotEmpty) {
                      DateTime _now = DateTime.now();
                      String _messageText = _inputController.text;
                      String _newMessageId = Uuid().v4();
                      _inputController.clear();
                      firestore.collection(FirestoreCollections.messages).doc(widget.game.gameId)
                        .collection(FirestoreCollections.chats)
                        .doc(_newMessageId)
                        .set({
                        ChatCollection.messageId: _newMessageId,
                        ChatCollection.message: _messageText,
                        ChatCollection.senderName: qp.username!,
                        ChatCollection.senderId: qp.id!,
                        ChatCollection.createdAt: _now,
                        ChatCollection.gameId: widget.game.gameId,
                      });
                    }
                  },
                  child: Icon(
                    Icons.send,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}