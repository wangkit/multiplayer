import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';

class CountDown extends StatefulWidget {

  final int endTime;
  final Function onEnd;
  final String headText;
  final bool showHeadText;
  final TextStyle? countDownStyle;
  final String endText;

  CountDown({
    Key? key,
    required this.endTime,
    required this.onEnd,
    required this.headText,
    required this.endText,
    this.showHeadText = true,
    this.countDownStyle,
  });

  @override
  _CountDownState createState() => _CountDownState();
}

class _CountDownState extends State<CountDown> {

  @override
  Widget build(BuildContext context) {
    return CountdownTimer(
      endTime: widget.endTime,
      onEnd: () => widget.onEnd(),
      widgetBuilder: (context, CurrentRemainingTime? time) {

        if (time == null) {
          return Text(
            widget.endText,
          );
        }

        int days = time.days != null ? time.days! : 0;
        int hours = time.hours != null ? time.hours! : 0;
        int minutes = time.min != null ? time.min! : 0;
        int seconds = time.sec != null ? time.sec! : 0;

        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            widget.showHeadText ? Text(
              widget.headText,
              style: Theme.of(context).textTheme.subtitle1!,
            ) : SizedBox(),
            Text(
              "${hours + days * 24}h ${minutes}m ${seconds}s",
              style: widget.countDownStyle == null ? Theme.of(context).textTheme.subtitle1!.merge(
                TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ) : widget.countDownStyle,
            ),
          ],
        );
      },
      endWidget: Text(
        widget.endText,
      ),
    );
  }
}