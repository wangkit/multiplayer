import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:storyteller/constants/const.dart';

class OpenContainerWrapper extends StatelessWidget {
  const OpenContainerWrapper({
    this.tappable = true,
    required this.closedWidget,
    required this.openedWidget,
    this.closedElevation = 6,
    this.closedColor,
    this.closedShape = const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(26),
      ),
    ),
  });

  final bool tappable;
  final Widget closedWidget;
  final Color? closedColor;
  final Widget openedWidget;
  final ShapeBorder closedShape;
  final double closedElevation;

  @override
  Widget build(BuildContext context) {
    return OpenContainer<bool>(
      transitionType: ContainerTransitionType.fade,
      openBuilder: (BuildContext context, VoidCallback _) {
        return openedWidget;
      },
      closedColor: closedColor ?? appBgColor,
      closedElevation: closedElevation,
      closedShape: closedShape,
      onClosed: null,
      tappable: tappable,
      closedBuilder: (BuildContext _, VoidCallback openContainer) {
        return closedWidget;
      },
    );
  }
}