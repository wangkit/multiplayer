import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';

class LongElevatedButton extends StatefulWidget {
  final bool isReverseColor;
  final Function onPressed;
  final String title;
  final double? height;
  final double? elevation;
  final double? width;
  final Color? color;
  final double borderRadius;
  final Color? splashColor;
  final Widget? child;
  final TextStyle? textStyle;
  final EdgeInsets? padding;
  final bool isEnabled;
  final Widget? icon;

  LongElevatedButton({
    Key? key,
    this.padding,
    this.textStyle,
    this.icon,
    this.child,
    this.isEnabled = true,
    this.splashColor,
    this.borderRadius = 36,
    this.height,
    this.color,
    this.width,
    this.elevation,
    this.isReverseColor = false,
    required this.onPressed,
    required this.title,
  }) : super(key: key);

  @override
  LongElevatedButtonState createState() => LongElevatedButtonState();
}

class LongElevatedButtonState extends State<LongElevatedButton> {


  late bool disabled;
  late Color bgColor;

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void disableButton() {
    disabled = true;
    refreshThisPage();
  }

  void enableButton() {
    disabled = false;
    refreshThisPage();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    disabled = !widget.isEnabled;
    bgColor = (widget.isReverseColor ? Colors.grey : widget.color != null ? widget.color : appIconColor)!;

    return AbsorbPointer(
      absorbing: disabled,
      child: Padding(
        padding: EdgeInsets.all(0.0),
        child: Container(
          width: widget.width != null ? widget.width : deviceWidth * 0.9,
          height: widget.height != null ? widget.height : 50,
          decoration: BoxDecoration(
            color: bgColor,
            borderRadius: BorderRadius.circular(widget.borderRadius),
          ),
          child: Material(
            elevation: widget.elevation ?? 0.0,
            color: Colors.transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(widget.borderRadius),
              onTap: () => widget.onPressed(),
              splashColor: bgColor.withOpacity(0.5),
              child: widget.child != null ? widget.child : Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    widget.icon ?? Container(),
                    Text(
                      widget.title,
                      style: widget.textStyle != null ? widget.textStyle : TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 18.0,
                        color: mainColor,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}