import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:storyteller/configs/dialog_configs.dart';

class DialogNegativeButton extends StatefulWidget {
  final Function? onCancelTap;
  final String? buttonCancelText;
  final Color? cancelColor;

  DialogNegativeButton({
    Key? key,
    this.onCancelTap,
    this.buttonCancelText,
    this.cancelColor,
  }) : super(key: key);

  @override
  DialogNegativeButtonState createState() => DialogNegativeButtonState();
}

class DialogNegativeButtonState extends State<DialogNegativeButton> {


  @override
  Widget build(BuildContext context) {

    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: widget.cancelColor ?? Colors.grey,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(DialogConfig.dialogCornerRadius),
        ),
      ),
      onPressed: widget.onCancelTap != null ? () => widget.onCancelTap!() : () => Navigator.of(context).pop(),
      child: Text(
        widget.buttonCancelText ?? 'Cancel',
        style: TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }
}