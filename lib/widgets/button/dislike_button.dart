import 'package:animate_do/animate_do.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:soy_common/color/color.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_routing_bottom_sheet.dart';

class DislikeButton extends StatefulWidget {
  final Function onPressed;
  final bool isDisliked;
  final bool isAnimation;

  DislikeButton({
    Key? key,
    required this.onPressed,
    required this.isDisliked,
    this.isAnimation = true,
  }) : super(key: key);

  @override
  DislikeButtonState createState() => DislikeButtonState();
}

class DislikeButtonState extends State<DislikeButton> {

  @override
  void initState() {
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget _mainWidget() {
    return Tooltip(
      message: TextData.dislike,
      child: InkWell(
        customBorder: CircleBorder(),
        splashColor: dislikeIconSplash,
        highlightColor: dislikeIconSplash,
        child: Icon(
          widget.isDisliked ? MdiIcons.arrowDownBold : MdiIcons.arrowDownBoldOutline,
          color: widget.isDisliked ? dislikeIconGlow : unfocusedColor,
        ),
        onTap: () {
          if (utils.hasLoggedIn()) {
            widget.onPressed();
          } else {
            getSignUpRoutingSheet();
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.isAnimation ? ElasticInDown(
      child: _mainWidget(),
    ) : _mainWidget();
  }
}