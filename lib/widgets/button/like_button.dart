import 'package:animate_do/animate_do.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_routing_bottom_sheet.dart';

class LikeButton extends StatefulWidget {
  final Function onPressed;
  final bool isLiked;
  final bool isAnimation;

  LikeButton({
    Key? key,
    required this.onPressed,
    required this.isLiked,
    this.isAnimation = true,
  }) : super(key: key);

  @override
  LikeButtonState createState() => LikeButtonState();
}

class LikeButtonState extends State<LikeButton> {

  @override
  void initState() {
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget _mainWidget() {
    return Tooltip(
      message: TextData.like,
      child: InkWell(
        customBorder: CircleBorder(),
        splashColor: likeIconSplash,
        highlightColor: likeIconSplash,
        child: Icon(
          widget.isLiked ? MdiIcons.arrowUpBold : MdiIcons.arrowUpBoldOutline,
          color: widget.isLiked ? likeIconGlow : unfocusedColor,
        ),
        onTap: () {
          if (utils.hasLoggedIn()) {
            widget.onPressed();
          } else {
            getSignUpRoutingSheet();
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.isAnimation ? ElasticInUp(
      child: _mainWidget(),
    ) : _mainWidget();
  }
}