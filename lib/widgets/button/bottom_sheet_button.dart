import 'package:animate_do/animate_do.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:storyteller/constants/const.dart';

class BottomSheetButton extends StatefulWidget {
  final Function onTap;
  final String label;
  final IconData? iconData;
  final Widget? leading;
  final TextStyle? labelStyle;

  BottomSheetButton({
    Key? key,
    required this.onTap,
    required this.label,
    this.labelStyle,
    this.iconData,
    this.leading,
  }) : assert(iconData == null ? leading != null : leading == null), super(key: key);

  @override
  BottomSheetButtonState createState() => BottomSheetButtonState();
}

class BottomSheetButtonState extends State<BottomSheetButton> {


  @override
  Widget build(BuildContext context) {

    return Flexible(
      child: ListTile(
        onTap: () {
          getRoute.pop();
          widget.onTap();
        },
        leading: widget.iconData != null ? IconButton(
          tooltip: widget.label,
          iconSize: 20.0,
          icon: Icon(widget.iconData, color: mainColor,),
          onPressed: null,
        ) : widget.leading,
        title: Text(
          widget.label,
          overflow: TextOverflow.ellipsis,
          style: widget.labelStyle ?? TextStyle(color: mainColor),),
      ),
    );
  }
}