import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/enum/game_page_type.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart' as yiframe;

class CustomYoutubePlayer extends StatefulWidget {

  final String videoId;

  CustomYoutubePlayer({
    Key? key,
    this.videoId = "",
  }) : super(key: key);

  @override
  CustomYoutubePlayerState createState() => CustomYoutubePlayerState();
}

class CustomYoutubePlayerState extends State<CustomYoutubePlayer> with AutomaticKeepAliveClientMixin<CustomYoutubePlayer> {

  late YoutubePlayerController _youtubePlayerController;
  late yiframe.YoutubePlayerController _iframeYouTubeController;
  late PlayerState _playerState;
  late YoutubeMetaData _videoMetaData;
  bool _isPlayerReady = false;

  bool getIsPlayerReady() {
    return _isPlayerReady;
  }

  void mute() {
    _youtubePlayerController.mute();
  }

  void unMute() {
    _youtubePlayerController.unMute();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    _youtubePlayerController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _iframeYouTubeController = yiframe.YoutubePlayerController(
        initialVideoId: widget.videoId,
        params: yiframe.YoutubePlayerParams(
          showControls: true,
          showFullscreenButton: true,
        ),
    );
    if (widget.videoId.isNotEmpty) {
      _youtubePlayerController = YoutubePlayerController(
        initialVideoId: widget.videoId,
        flags: YoutubePlayerFlags(
          autoPlay: false,
          mute: isVideoStartMuted,
        ),
      )..addListener(listener);
    }
    _videoMetaData = const YoutubeMetaData();
    _playerState = PlayerState.unknown;
    super.initState();
  }

  void listener() {
    if (_isPlayerReady && mounted && !_youtubePlayerController.value.isFullScreen) {
      setState(() {
        _playerState = _youtubePlayerController.value.playerState;
        _videoMetaData = _youtubePlayerController.metadata;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return kIsWeb ? yiframe.YoutubePlayerIFrame(
      controller: _iframeYouTubeController,
      aspectRatio: 18 / 9,
    ) :  YoutubePlayer(
      aspectRatio: 18 / 9,
      controller: _youtubePlayerController,
      showVideoProgressIndicator: true,
      progressIndicatorColor: appIconColor,
      bottomActions: [
        const SizedBox(width: 14.0),
        CurrentPosition(),
        const SizedBox(width: 8.0),
        ProgressBar(
          isExpanded: true,
          colors: ProgressBarColors(
            playedColor: appIconColor,
            handleColor: Colors.amberAccent,
          ),
        ),
        RemainingDuration(),
        const PlaybackSpeedButton(),
        IconButton(
          icon: Icon(
            Icons.open_in_browser,
            color: Colors.white,
          ),
          onPressed: () {
            utils.launchURL(youtubeUrl + widget.videoId);
          },
        ),
      ],
      onReady: () {
        _isPlayerReady = true;
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
