import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/steam/steam_utils.dart';
import 'package:storyteller/widgets/appbar/custom_appbar.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';


class CustomWebView extends StatefulWidget {
  final String link;
  final String targetPlatform;
  final Function? onDismiss;

  CustomWebView({
    Key? key,
    required this.link,
    this.targetPlatform = "",
    this.onDismiss,
  }) : super(key: key);

  @override
  _CustomWebViewState createState() => _CustomWebViewState();
}

class _CustomWebViewState extends State<CustomWebView> {

  late String link;
  late PullToRefreshController pullToRefreshController;
  final GlobalKey webViewKey = GlobalKey();
  double _psnLoadedTimes = 0;
  final double _stopSign = 1000;
  double progress = 0;

  InAppWebViewController? webViewController;
  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
    crossPlatform: InAppWebViewOptions(
      useShouldOverrideUrlLoading: false,
      mediaPlaybackRequiresUserGesture: false,
    ),
    android: AndroidInAppWebViewOptions(
      useHybridComposition: true,
    ),
    ios: IOSInAppWebViewOptions(
      allowsInlineMediaPlayback: true,
    ),
  );

  @override
  void dispose() {
    super.dispose();
    if (widget.onDismiss != null) {
      widget.onDismiss!();
    }
  }

  @override
  void initState() {
    link = widget.link;
    if (link.startsWith("www.")) link = "https://" + link;
    super.initState();
    pullToRefreshController = PullToRefreshController(
      options: PullToRefreshOptions(
        color: appIconColor,
      ),
      onRefresh: () async {
        if (Platform.isAndroid) {
          webViewController?.reload();
        } else if (Platform.isIOS) {
          webViewController?.loadUrl(
            urlRequest: URLRequest(url: await webViewController?.getUrl()),
          );
        }
      },
    );
  }

  void _handleGetSteam(Uri? _uri) {
    /// HANDLING NEW STEAM ID
    /// Widget.targetPlatform is used to determine what platform we are working on
    /// and if any changes needed to be made because if this is browsing
    /// other player's profile, then we would not want to update the id
    /// of current user by the id of that other user
    if (widget.targetPlatform == ConfigCollection.steam && _uri != null && RegExp(r"https://steamcommunity.com/profiles/765\d+").hasMatch(_uri.toString()) && qp.steamFriendCode!.isEmpty) {
      if (int.tryParse(_uri.pathSegments.last) != null) {
        qp.steamFriendCode = SteamUtils.from64To32(_uri.pathSegments.last);
        firestore.collection(FirestoreCollections.users).doc(qp.id!).update({
          UserCollection.steamFriendCode: qp.steamFriendCode,
        });
        getRoute.pop();
        utils.toast(TextData.setSteam,);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        accentColor: appTheme,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: appBgColor,
        ),
        child: WillPopScope(
          onWillPop: () async {
            return true;
          },
          child: Scaffold(
            appBar: CustomAppBar(
              actions: [
                Padding(
                  padding: EdgeInsets.all(4.0),
                  child: IconButton(
                    tooltip: TextData.copyLink,
                    icon: Icon(Icons.content_copy, color: mainColor,),
                    onPressed: () {
                      Clipboard.setData(ClipboardData(text: widget.link));
                      utils.toast(TextData.copiedSuccessfully);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(4.0),
                  child: IconButton(
                    tooltip: TextData.openInBrowser,
                    icon: Icon(Icons.open_in_browser, color: mainColor,),
                    onPressed: () async {
                      await utils.launchURL(link);
                    },
                  ),
                ),
              ],
            ),
            backgroundColor: transparent,
            body: Stack(
              children: [
                InAppWebView(
                  key: webViewKey,
                  initialUrlRequest: URLRequest(url: Uri.parse(link)),
                  initialOptions: options,
                  pullToRefreshController: pullToRefreshController,
                  onWebViewCreated: (controller) async {
                    webViewController = controller;
                  },
                  androidOnPermissionRequest: (controller, origin, resources) async {
                    return PermissionRequestResponse(
                        resources: resources,
                        action: PermissionRequestResponseAction.GRANT,
                    );
                  },
                  onLoadStop: (InAppWebViewController _controller, Uri? _uri) async {

                    _handleGetSteam(_uri);

                    /// HANDLING NEW XBOX GAMERTAG
                    String? _title = await _controller.getTitle();
                    if (widget.targetPlatform == ConfigCollection.xbox && _title != null && qp.xboxGamerTag!.isEmpty && RegExp(r"^[a-zA-Z0-9 ]{1,15} - Profile$").hasMatch(_title)) {
                      qp.xboxGamerTag = _title.split(" - Profile").first;
                      firestore.collection(FirestoreCollections.users).doc(qp.id!).update({
                        UserCollection.xboxGamerTag: qp.xboxGamerTag,
                      });
                      getRoute.pop();
                      utils.toast(TextData.setXbox,);
                    }

                    if (widget.targetPlatform == ConfigCollection.playstation && _psnLoadedTimes > 1 && _psnLoadedTimes != _stopSign) {
                      /// Login successfully, now reload the page to make it navigate to profile/{psn}/friends page
                      Future.delayed(Duration(seconds: 2,), () async {
                        await webViewController!.reload();
                      });
                      /// Prevent forever refreshing
                      _psnLoadedTimes = _stopSign;
                    }

                    debugPrint("load end...${_uri.toString()}...");
                    if (widget.targetPlatform == ConfigCollection.playstation && qp.psn!.isEmpty && _uri != null && RegExp(r"^https://my.playstation.com/profile/[a-zA-Z0-9-_]{3,16}/friends$").hasMatch(_uri.toString())) {
                      qp.psn = _uri.toString().split("/friends").first.split("https://my.playstation.com/profile/").last;
                      firestore.collection(FirestoreCollections.users).doc(qp.id!).update({
                        UserCollection.psn: qp.psn,
                      });
                      getRoute.pop();
                      utils.toast(TextData.setPs,);
                    }
                  },
                  onLoadStart: (InAppWebViewController _controller, Uri? _uri) async {

                    _handleGetSteam(_uri);

                    /// Making sure psn website will redirect to profile page that has psn
                    if (widget.targetPlatform == ConfigCollection.playstation && _uri != null && _uri.toString().endsWith("https://my.playstation.com/") && _psnLoadedTimes != _stopSign) {
                      _psnLoadedTimes++;
                    }
                    debugPrint("load starting...${_uri.toString()}...");
                  },
                  onProgressChanged: (controller, currentProgress) {
                    if (currentProgress == 100) {
                      pullToRefreshController.endRefreshing();
                    }
                    setState(() {
                      progress = currentProgress / 100;
                    });
                  },
                  shouldOverrideUrlLoading: (controller, navigationAction) async {
                    var uri = navigationAction.request.url!;

                    if (![ "http", "https", "file", "chrome", "data", "javascript", "about"].contains(uri.scheme)) {
                      if (await canLaunch(link)) {
                        // Launch the App
                        await launch(
                          link,
                        );
                        // and cancel the request
                        return NavigationActionPolicy.CANCEL;
                      }
                    }
                    return NavigationActionPolicy.ALLOW;
                  },
                  onLoadError: (controller, url, code, message) {
                    pullToRefreshController.endRefreshing();
                  },
                  onConsoleMessage: (controller, consoleMessage) {
                    debugPrint(consoleMessage.message);
                  },
                ),
                progress < 1.0
                    ? LinearProgressIndicator(
                    value: progress,
                  valueColor: AlwaysStoppedAnimation(appIconColor),
                ) : CommonWidgets.emptyBox(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}