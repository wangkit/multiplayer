import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/utils/extensions/string_extensions.dart';
import 'package:validators2/validators.dart';

class CustomFileImage extends StatefulWidget {
  final File file;
  final double width;
  final double height;
  final double radius;
  final bool showBorder;
  final bool disableOnTap;

  CustomFileImage({
    Key? key,
    required this.file,
    this.width = 70.0,
    this.height = 70.0,
    this.radius = 200.0,
    this.showBorder = false,
    this.disableOnTap = false,
  }) : super(key: key);

  @override
  CustomFileImageState createState() => CustomFileImageState();
}

class CustomFileImageState extends State<CustomFileImage> {

  late String _hero;

  @override
  void initState() {
    _hero = UniqueKey().toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
//      onTap: widget.disableOnTap ? null : widget.onPressed != null ? widget.onPressed!() : () {
//        getRoute.navigateTo(ImageViewer(
//          text: widget.text,
//          imageUrls: widget.imageUrls != null ? widget.imageUrls : [widget.imageUrl],
//          color: utils.getHexCodeByColor(widget.color),
//          heroKey: _hero,
//        ));
//      },
      child: Hero(
        tag: _hero,
        child: Container(
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(widget.radius),
            image: DecorationImage(
              fit: BoxFit.cover,
              image: FileImage(
                widget.file,
              ),
            ),
          ),
        ),
      ),
    );
  }
}