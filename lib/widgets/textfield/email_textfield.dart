import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/utils/extensions/string_extensions.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:validators2/validators.dart';

class EmailTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode? focusNode;
  final FocusNode? nextFocusNode;
  final String? label;
  final String? helper;
  final bool canEmpty;
  final EdgeInsets? edgeInsets;
  final bool autoFocus;
  final double borderRadius;
  final bool showLabel;
  final bool showHint;
  final TextStyle? labelHintStyle;

  EmailTextField({
    Key? key,
    required this.controller,
    this.focusNode,
    this.borderRadius = 0.0,
    this.nextFocusNode,
    this.label = "Email",
    this.helper,
    this.labelHintStyle,
    this.showHint = true,
    this.showLabel = false,
    this.canEmpty = false,
    this.edgeInsets,
    this.autoFocus = false,
  }) : super(key: key);

  @override
  _EmailTextFieldState createState() => _EmailTextFieldState();
}

class _EmailTextFieldState extends State<EmailTextField> {

  late bool isVisible;

  @override
  void initState() {
    isVisible = false;
    if (widget.focusNode != null) {
      widget.focusNode!.addListener(() {
        if (this.mounted) {
          setState(() {
            isVisible = !isVisible;
          });
        }
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.edgeInsets ?? EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
      child: TextFormField(
        autofocus: widget.autoFocus,
        focusNode: widget.focusNode,
        style: TextStyle(
          color: mainColor,
        ),
        onFieldSubmitted: (term) {
          if (widget.focusNode != null && widget.nextFocusNode != null) {
            utils.fieldFocusChange(widget.focusNode!, widget.nextFocusNode!);
          }
        },
        autofillHints: [AutofillHints.email],
        controller: widget.controller,
        validator: !widget.canEmpty ? (String? value) {
          if (value!.isNullOrEmpty) {
            return "Email must not be empty";
          }
          if (value.contains(" ")) {
            return "Email must not contain white space";
          }
          if (!isEmail(value)) {
            return "Invalid email format";
          }
          return null;
        } : null,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          helperText: widget.helper,
          counterText: "",
          helperMaxLines: 3,
          hintMaxLines: 3,
          errorMaxLines: 2,
          helperStyle: TextStyle(
            color: mainColor,
          ),
          labelText: widget.showLabel ? widget.label : null,
          labelStyle: widget.labelHintStyle ?? TextStyle(
            color: unfocusedColor,
          ),
          hintText: widget.showHint ? "Email" : null,
          hintStyle: widget.labelHintStyle ?? TextStyle(
            color: unfocusedColor,
          ),
          suffixIcon: AnimatedOpacity(
            opacity: isVisible ? 1.0 : 0.0,
            duration: kThemeAnimationDuration,
            child: CommonWidgets.clearTextFieldButton(widget.controller),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: mainColor,
            ),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(widget.borderRadius),
            borderSide: BorderSide(
              color: mainColor,
            ),
          ),
        ),
      ),
    );
  }
}