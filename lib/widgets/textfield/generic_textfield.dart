import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:validators2/validators.dart';
import 'package:storyteller/utils/extensions/string_extensions.dart';

class GenericTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode? focusNode;
  final FocusNode? nextFocusNode;
  final String? hint;
  final String? helper;
  final bool canEmpty;
  final bool canSpace;
  final TextInputType? textInputType;
  final List<String>? autofillHints;
  final int? maxLines;
  final int? maxLength;
  final Function? onSubmitted;
  final Function? validation;
  final List<TextInputFormatter>? inputFormatter;
  final TextCapitalization textCapitalization;
  final bool showLabel;
  final String label;
  final double borderRadius;
  final bool showHint;

  GenericTextField({
    Key? key,
    required this.controller,
    this.focusNode,
    this.validation,
    this.autofillHints,
    this.textCapitalization = TextCapitalization.sentences,
    this.maxLines,
    this.maxLength,
    this.canSpace = true,
    this.inputFormatter,
    this.textInputType,
    this.nextFocusNode, 
    this.hint = "",
    this.onSubmitted,
    this.borderRadius = 0,
    this.helper, 
    this.canEmpty = false,
    this.showLabel = false,
    this.showHint = true,
    this.label = "",
  }) : super(key: key);

  @override
  _GenericTextFieldState createState() => _GenericTextFieldState();
}

class _GenericTextFieldState extends State<GenericTextField> {

  late bool isVisible;

  @override
  void initState() {
    isVisible = false;
    if (widget.focusNode != null) {
      widget.focusNode!.addListener(() {
        if (this.mounted) {
          setState(() {
            isVisible = !isVisible;
          });
        }
      });
    } else {
      isVisible = true;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
      child: TextFormField(
        focusNode: widget.focusNode,
        inputFormatters: widget.inputFormatter,
        onFieldSubmitted: (_) {
          if (widget.focusNode != null && widget.nextFocusNode != null) {
            utils.fieldFocusChange(widget.focusNode!, widget.nextFocusNode!);
          }
        },
        autofillHints: widget.autofillHints,
        controller: widget.controller,
        validator: widget.validation != null ? (String? _val) => widget.validation!(_val) : (String? value) {
          if (value!.isNullOrEmpty && !widget.canEmpty) {
            return "${widget.hint!.capitalize} must not be empty";
          }
          return null;
        },
        textCapitalization: widget.textCapitalization,
        keyboardType: widget.textInputType,
        maxLines: widget.maxLines,
        maxLength: widget.maxLength ?? 150,
        decoration: InputDecoration(
          helperText: widget.helper,
          counterText: widget.maxLength != null ? null : "",
          helperMaxLines: 3,
          hintMaxLines: 3,
          errorMaxLines: 3,
          labelText: widget.showLabel ? widget.label.length == 0 ? widget.hint : widget.label : null,
          labelStyle: TextStyle(
            color: unfocusedColor,
          ),
          helperStyle: TextStyle(
            color: mainColor,
          ),
          hintText: widget.showHint ? widget.hint : null,
          hintStyle: TextStyle(
            color: unfocusedColor,
          ),
          suffixIcon: AnimatedOpacity(
            opacity: isVisible ? 1.0 : 0.0,
            duration: kThemeAnimationDuration,
            child: CommonWidgets.clearTextFieldButton(widget.controller),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: mainColor,
            ),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(widget.borderRadius),
            borderSide: BorderSide(
              color: mainColor,
            ),
          ),
        ),
      ),
    );
  }
}