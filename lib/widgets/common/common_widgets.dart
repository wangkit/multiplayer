import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:soy_common/widgets/tag/tag.dart';
import 'package:soy_common/widgets/textfield/generic_textfield.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/widgets/image/network_thumbnail.dart';

class CommonWidgets {

  static Widget loading({double size = 50}) {
    return SpinKitFadingCircle(
      color: unfocusedColor,
      size: size,
    );
  }

  static gameDetailsTitle(String title) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.only(left: 12.0, top: 12.0),
        child: Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  static forumProfilePicture(bool _isMyself, String _userId,) {

    Widget _profilePicture(String photo) {
      return NetworkThumbnail(
        imageUrl: photo,
        padding: EdgeInsets.only(right: 8),
        width: 20,
        height: 20,
        text: "",
      );
    }

    return _isMyself ? _profilePicture(qp.photoUrl!) : FutureBuilder(
      future: apiManager.getUserProfile(
          _userId,
      ),
      initialData: UserProfile(
        photoUrl: defaultProfilePictureLink,
      ),
      builder: (ctx, AsyncSnapshot<UserProfile?> snapshot) {

        UserProfile _profile = snapshot.hasData ? snapshot.data! : UserProfile(
          photoUrl: defaultProfilePictureLink,
        );

        return _profile.photoUrl != null ? _profilePicture(_profile.photoUrl!) : CommonWidgets.emptyBox();
      },
    );
  }

  static emptyMessageWithIcon(String message) {
    return Padding(
      padding: EdgeInsets.all(8),
      child: Center(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              "assets/icon/empty.png",
              width: 40,
              height: 40,
            ),
            SizedBox(
              width: 10,
            ),
            emptyMessage(message),
          ],
        ),
      ),
    );
  }

  static emptyMessage(String message) {
    return Padding(
      padding: EdgeInsets.all(8),
      child: Center(
        child: Text(
          message,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 20,
          ),
        ),
      ),
    );
  }

  static Widget emptyBox() {
    return SizedBox(
      width: 0,
      height: 0,
    );
  }

  static Widget tag(String title, {Color? color, Function? onTap, EdgeInsets outsidePadding = const EdgeInsets.all(3.0), Icon? icon}) {
    return FadeIn(
      child: Padding(
        padding: outsidePadding,
        child: Tag(
          onPressed: () {
            if (onTap != null) {
              onTap();
            }
          },
          icon: icon,
          radius: commonRadius,
          text: title,
          horizontalPadding: 9,
          verticalPadding: 4,
          color: color,
          elevation: 1,
        ),
      ),
    );
  }

  static Widget verticalDivider({double width = 0, double thickness = 0.5, Color? color}) {
    return VerticalDivider(
      thickness: thickness,
      width: width,
      color: color,
    );
  }

  static Widget divider({double height = 0, double thickness = 0.5, Color? color}) {
    return Divider(
      thickness: thickness,
      height: height,
      color: color,
    );
  }

  static Widget separationDot() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 6.0,),
      child: Icon(
        MdiIcons.circle,
        color: Colors.grey,
        size: 6.0,
      ),
    );
  }

  static IconButton clearTextFieldButton(TextEditingController controller, {Function? onPressed}) {
    return IconButton(
      icon: Icon(
        Icons.clear,
        color: unfocusedColor,
        size: 20,
      ),
      onPressed: () {
        if (onPressed != null) {
          onPressed();
        }
        WidgetsBinding.instance!.addPostFrameCallback((_) => controller.clear());
      },
    );
  }

  static Widget bottomSheetPullIndicator() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Divider(color: unfocusedColor, thickness: 4, indent: deviceWidth * 0.43, endIndent: deviceWidth * 0.43,),
    );
  }
}