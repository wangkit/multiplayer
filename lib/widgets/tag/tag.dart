import 'package:bubble/bubble.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';

class Tag extends StatefulWidget {
  final String text;
  final Function? onPressed;
  final Icon? icon;
  final Icon? leadingIcon;
  final double margin;
  late double? horizontalPadding;
  late double? verticalPadding;
  late double? radius;
  final double elevation;
  final Widget? child;
  final TextStyle? style;
  final Color? color;

  Tag({
    Key? key,
    required this.text,
    this.onPressed,
    this.leadingIcon,
    this.radius = 0,
    this.child,
    this.elevation = 1,
    this.style,
    this.margin = 0,
    this.horizontalPadding = 0,
    this.verticalPadding = 0,
    this.icon,
    this.color,
  }) : super(key: key);

  @override
  TagState createState() => TagState();
}

class TagState extends State<Tag> {

  @override
  void initState() {
    if (widget.radius == 0) widget.radius = commonRadius;
    if (widget.horizontalPadding == 0) widget.horizontalPadding = null;
    if (widget.verticalPadding == 0) widget.verticalPadding = null;
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onPressed != null ? () => widget.onPressed!() : null,
      child: Bubble(
        margin: BubbleEdges.all(widget.margin),
        padding: BubbleEdges.symmetric(
          horizontal: widget.horizontalPadding,
          vertical: widget.verticalPadding,
        ),
        radius: Radius.circular(widget.radius!),
        elevation: widget.elevation,
        color: widget.color != null ? widget.color : appIconColor,
        child: widget.child != null ? widget.child : Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            widget.leadingIcon == null ? SizedBox(
              width: 0,
              height: 0,
            ) : Padding(
              padding: EdgeInsets.only(right: 2.0,),
              child: widget.leadingIcon,
            ),
            Flexible(
              child: Text(
                widget.text,
                style: widget.style != null ? widget.style : null,
              ),
            ),
            widget.icon == null ? Container() : Padding(
              padding: EdgeInsets.only(left: 2.0),
              child: widget.icon,
            ),
          ],
        ),
      ),
    );
  }
}