import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:share/share.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/network/fetch_preview.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/analysis.dart';
import 'package:storyteller/resources/values/fetch_link.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:storyteller/widgets/button/bottom_sheet_button.dart';
import 'package:storyteller/widgets/webview/custom_web_view.dart';
import 'package:validators2/validators.dart';

class NewsBubble extends StatefulWidget {

  final News news;

  NewsBubble({
    Key? key,
    required this.news,
  });

  @override
  _NewsBubbleState createState() => _NewsBubbleState();
}

class _NewsBubbleState extends State<NewsBubble> with AutomaticKeepAliveClientMixin<NewsBubble> {

  String _image = "";
  late Color _thisColor;
  final double _widthPadding = 4;
  final double _radius = 12;

  Future<void> _fetchLinkImage(String url) async {
    Map? _map = await FetchPreview().fetch(url);
    if (_map != null) {
      _image = _map[FetchLink.image];
      if (_image.isEmpty) {
        _image = _map[FetchLink.appleIcon];
      }
      if (_image.isEmpty) {
        _image = _map[FetchLink.favIcon];
      }
    }
  }

  @override
  void initState() {
    _thisColor = utils.convertStrToColor(widget.news.name);
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return FutureBuilder(
      future: _fetchLinkImage(widget.news.link),
      builder: (ctx, snapshot) {

        return Container(
          width: deviceWidth,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(_radius),
          ),
          child: Card(
            margin: EdgeInsets.symmetric(vertical: 4, horizontal: _widthPadding),
            child: InkWell(
              splashColor: _thisColor,
              onLongPress: () {
                getGenericBottomSheet([
                  BottomSheetButton(
                    onTap: () {
                      getRoute.navigateTo(CustomWebView(
                        link: widget.news.link,
                      ));
                    },
                    iconData: Icons.open_in_browser_outlined,
                    label: TextData.openInBrowser,
                  ),
                  BottomSheetButton(
                    onTap: () {
                      Share.share(widget.news.link);
                    },
                    iconData: Icons.ios_share,
                    label: TextData.share,
                  ),
                ]);
              },
              onTap: () {
                getRoute.navigateTo(CustomWebView(
                  link: widget.news.link,
                ));
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  widget.news.link.contains("msn.com") || !isURL(widget.news.link) ? CommonWidgets.emptyBox() : ClipRRect(
                    borderRadius: BorderRadius.circular(_radius),
                    child: CachedNetworkImage(
                      imageUrl: _image,
                      placeholder: (ctx, value) {
                        return CachedNetworkImage(
                          imageUrl: horizontalGreyPlaceholder,
                        );
                      },
                      errorWidget: (ctx, value, va) {
                        return CachedNetworkImage(
                          imageUrl: horizontalGreyPlaceholder,
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8,),
                    child: Text(
                      widget.news.name,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 19,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}


