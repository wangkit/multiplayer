import 'package:animate_do/animate_do.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:storyteller/apps/games/upcoming_game_details_page.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/webview/custom_web_view.dart';

class UpcomingBubble extends StatefulWidget {

  final UpcomingGame upcoming;

  UpcomingBubble({
    Key? key,
    required this.upcoming,
  });

  @override
  _UpcomingBubbleState createState() => _UpcomingBubbleState();
}

class _UpcomingBubbleState extends State<UpcomingBubble> {

  late Color _thisColor;
  final double _widthPadding = 4;
  final double _radius = 12;

  @override
  void initState() {
    _thisColor = utils.convertStrToColor(widget.upcoming.gameName);
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      width: deviceWidth,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(_radius),
      ),
      child: Card(
        margin: EdgeInsets.symmetric(vertical: 4, horizontal: _widthPadding),
        child: InkWell(
          splashColor: _thisColor,
          onTap: () {
            getRoute.navigateTo(UpcomingGameDetailsPage(
              upcoming: widget.upcoming,
            ));
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Flexible(
                child: SizedBox(
                  width: 120,
                  height: 180,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(_radius),
                    child: CachedNetworkImage(
                      imageUrl: widget.upcoming.gameCover,
                      placeholder: (ctx, value) {
                        return CachedNetworkImage(imageUrl: verticalGreyPlaceholder);
                      },
                      errorWidget: (ctx, value, va) {
                        return CachedNetworkImage(imageUrl: verticalGreyPlaceholder);
                      },
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text(
                        widget.upcoming.gameName,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Flexible(
                      child: Text(
                        "Approximately ${timeUtils.getParsedYearMonthDay(widget.upcoming.releaseDate)}",
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Flexible(
                      child: Wrap(
                        clipBehavior: Clip.antiAlias,
                        alignment: WrapAlignment.start,
                        runAlignment: WrapAlignment.start,
                        children: List.generate(widget.upcoming.gameTheme.length > 2 ? 2 : widget.upcoming.gameTheme.length, (int index) {

                          return CommonWidgets.tag(
                            widget.upcoming.gameTheme[index],
                            color: _thisColor,
                          );
                        }),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


