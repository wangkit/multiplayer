import 'package:animate_do/animate_do.dart';
import 'package:animations/animations.dart';
import 'package:bubble/bubble.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:device_calendar/device_calendar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:storyteller/apps/chat/chat_page.dart';
import 'package:storyteller/apps/games/game_details_page.dart';
import 'package:storyteller/apps/home/profile/profile_page.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/analysis.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_with_email.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_routing_bottom_sheet.dart';
import 'package:storyteller/utils/dialog/binary_dialog.dart';
import 'package:storyteller/utils/dialog/block_dialog.dart';
import 'package:storyteller/utils/dialog/report_dialog.dart';
import 'package:storyteller/widgets/button/bottom_sheet_button.dart';
import 'package:storyteller/widgets/button/custom_argon_button.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/count_down/count_down.dart';
import 'package:storyteller/widgets/image/network_thumbnail.dart';
import 'package:storyteller/widgets/tag/tag.dart';
import 'package:storyteller/widgets/webview/custom_web_view.dart';

class GameBubble extends StatefulWidget {

  Game game;
  Function? onQuit;
  Function? onThemeTagClick;
  Function? onPlatformTagClick;

  GameBubble({
    Key? key,
    required this.game,
    this.onThemeTagClick,
    this.onPlatformTagClick,
    this.onQuit,
  });

  @override
  _GameBubbleState createState() => _GameBubbleState();
}

class _GameBubbleState extends State<GameBubble> {

  late bool _hasFulled;
  late bool _isMyself;
  late Color _thisColor;
  final _height = 130;
  double _widthPadding = 4;
  final double _borderRadius = 8;
  late double _maxWidth;

  @override
  void initState() {
    _maxWidth = deviceWidth - (_widthPadding * 2) - 1;
    _hasFulled = widget.game.maxPlayers <= widget.game.players.length;
    _isMyself = widget.game.creatorId == qp.id!;
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void showBottomSheet() {
    if (utils.hasLoggedIn()) {
      getGenericBottomSheet([
        BottomSheetButton(
          label: userTag + widget.game.creatorUsername,
          onTap: () {
            getRoute.navigateTo(ProfilePage(
              userId: widget.game.creatorId,
            ));
          },
          iconData: Icons.account_circle_outlined,
        ),
        BottomSheetButton(
          label: TextData.moreAboutThisGame,
          onTap: () {
            getRoute.navigateTo(CustomWebView(
              link: "https://www.google.com/search?q=${widget.game.gameName}",
            ));
          },
          iconData: Icons.more_outlined,
        ),
        _isMyself ? Container() : BottomSheetButton(
          label: TextData.report,
          onTap: () {
            getReportDialog(UserProfile(
                username: widget.game.creatorUsername,
                id: widget.game.creatorId
            ));
          },
          iconData: Icons.report_outlined,
        ),
        _isMyself ? Container() : BottomSheetButton(
          label: TextData.block,
          onTap: () {
            getBlockDialog(UserProfile(
                username: widget.game.creatorUsername,
                id: widget.game.creatorId
            ));
          },
          iconData: Icons.block_outlined,
        ),
      ]);
    } else {
      getSignUpRoutingSheet();
    }
  }

  bool _hasJoined() {
    return widget.game.playerIds.contains(qp.id!);
  }

  @override
  Widget build(BuildContext context) {

    _hasFulled = widget.game.maxPlayers <= widget.game.players.length;
    _thisColor = utils.convertStrToColor(widget.game.creatorId);

    return Card(
      margin: EdgeInsets.symmetric(vertical: 4, horizontal: _widthPadding),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_borderRadius),
      ),
      child: InkWell(
        splashColor: _thisColor,
        onLongPress: () {
          showBottomSheet();
        },
        onTap: () {
          if (utils.hasLoggedIn()) {
//              if (widget.game.gameVideo == null && widget.game.gameCover == null && widget.game.gameCover!.isEmpty || widget.game.gameVideo!.isEmpty) {
//                utils.toast(TextData.fetchingData, isWarning: true);
//                return;
//              }
            getRoute.navigateTo(GameDetailsPage(
              game: widget.game,
              height: _height.toDouble(),
            ));
          } else {
            getSignUpRoutingSheet();
          }
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: _height.toDouble(),
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 8, bottom: 8, left: 10),
                    child: AspectRatio(
                      aspectRatio: 10.4 / 16,
                      child: NetworkThumbnail(
                        imageUrl: widget.game.gameCover != null && widget.game.gameCover!.isNotEmpty ? widget.game.gameCover!.last : gameCoverLink,
                        radius: 6,
                        text: '',
                        onError: (StackTrace stacktrace) {
                          if (stacktrace.toString().contains("MultiImageStreamCompleter.<anonymous closure>")) {
                            apiManager.removeCover(widget.game.gameCover!.last, widget.game.gameId);
                          }
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: 0.0, left: 24, right: 24, top: 5),
                          child: Text(
                            widget.game.gameName,
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.subtitle1!.merge(
                              TextStyle(
                                fontWeight: FontWeight.bold,
                              )
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 0.0, left: 24, right: 24, top: 0),
                          child: Text(
                              timeUtils.isToday(widget.game.startTime) ?
                              "Today ${timeUtils.getParsedDateTimeHourMin(widget.game.startTime)}" : timeUtils.isTomorrow(widget.game.startTime) ?
                              "Tomorrow ${timeUtils.getParsedDateTimeHourMin(widget.game.startTime)}": timeUtils.getParsedDateTimeLongFormatWithHourMin(widget.game.startTime) + (widget.game.startTime.isBefore(DateTime.now()) ?
                              " (Started)" : ""),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 0.0, left: 24.0, right: 24, top: 0),
                          child: Row(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  getRoute.navigateTo(ProfilePage(userId: widget.game.creatorId,));
                                },
                                child: Text(
                                  userTag + widget.game.creatorUsername,
                                ),
                              ),
                              CommonWidgets.separationDot(),
                              Text(
                                timeUtils.getTimeDiffString(timeUtils.getParsedDateTime(widget.game.createdAt.toUtc())),
                              ),
                              CommonWidgets.separationDot(),
                              Text(
                                "${widget.game.players.length} / ${widget.game.maxPlayers}",
                              ),
                              CommonWidgets.separationDot(),
                              IconButton(
                                onPressed: () async {
                                  await utils.shareShortUrl("https://multiplayer.com/invite/?${GameCollection.gameId}=${widget.game.gameId}");
                                },
                                iconSize: 20,
                                padding: EdgeInsets.zero,
                                constraints: BoxConstraints(),
                                icon: Icon(
                                  Icons.ios_share,
                                ),
                              ),
                              Flexible(
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Icon(
                                    utils.getGamePlatformIcon(widget.game.platformProduct),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 8.0, left: 8.0, right: 8, top: 0),
              child: Wrap(
                clipBehavior: Clip.antiAlias,
                alignment: WrapAlignment.start,
                runAlignment: WrapAlignment.start,
                children: List.generate(widget.game.gameTheme!.length + (_hasJoined() ? 1 : 0) + 1, (int index) {

                  late String _thisTheme;

                  if (_hasJoined() && index == widget.game.gameTheme!.length) {
                    _thisTheme = TextData.joined;
                  } else if (index >= widget.game.gameTheme!.length) {
                    _thisTheme = widget.game.platformProduct;
                  } else {
                    _thisTheme = widget.game.gameTheme![index];
                  }

                  return CommonWidgets.tag(
                    _thisTheme,
                    onTap: () {
                      /// Index smaller than the length of the theme list
                      /// makes sure the clicked tag is a theme tag and not platform product or joined
                      if (widget.onThemeTagClick != null && index < widget.game.gameTheme!.length) {
                        widget.onThemeTagClick!(_thisTheme);
                      } else if (widget.onPlatformTagClick != null && index >= widget.game.gameTheme!.length && _thisTheme != TextData.joined) {
                        /// Make sure this is not joined tag and is platform tag
                        widget.onPlatformTagClick!(_thisTheme);
                      }
                    },
                    color: _hasJoined() ? _thisColor : unfocusedColor,
                  );
                }),
              ),
            ),
            CommonWidgets.divider(
              color: unfocusedColor,
            ),
            IntrinsicHeight(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: CustomArgonButton(
                      elevation: 0,
                      width: _hasJoined() ? _maxWidth / 2 : _maxWidth,
                      height: 45,
                      bottomLeftRadius: _borderRadius,
                      bottomRightRadius: _hasJoined() ? 0 : _borderRadius,
                      splashColor: _thisColor,
                      textStyle: TextStyle(
                          fontSize: 16
                      ),
                      onPressed: () async {
                        await utils.joinQuitGame(widget.game, refreshThisPage, showLoading: false, onQuit: widget.onQuit);
                      },
                      loadColor: _thisColor,
                      color: _hasJoined() ? _thisColor : Colors.white,
                      title: _hasJoined() ? TextData.quit : TextData.join,
                    ),
                  ),
                  if (_hasJoined()) CommonWidgets.verticalDivider(
                    width: 1,
                    color: unfocusedColor,
                  ) else CommonWidgets.emptyBox(),
                  if (_hasJoined()) Flexible(
                    child: Container(
                      width: _maxWidth / 2,
                      height: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(_borderRadius)
                        ),
                        color: _hasJoined() ? _thisColor : Colors.white,
                      ),
                      child: InkWell(
                        splashColor: _thisColor,
                        onTap: () {
                          if (_hasJoined() && utils.hasLoggedIn()) {
                            getRoute.navigateTo(ChatPage(
                              game: widget.game,
                            ));
                          } else {
                            getSignUpRoutingSheet();
                          }
                        },
                        child: Center(
                          child: Text(
                            TextData.chat,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16
                            ),
                          ),
                        ),
                      ),
                    ),
                  ) else CommonWidgets.emptyBox(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}


