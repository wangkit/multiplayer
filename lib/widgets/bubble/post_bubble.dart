import 'package:animate_do/animate_do.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:storyteller/apps/home/profile/profile_page.dart';
import 'package:storyteller/apps/posts/post_detail_page.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_routing_bottom_sheet.dart';
import 'package:storyteller/utils/dialog/block_dialog.dart';
import 'package:storyteller/utils/dialog/report_dialog.dart';
import 'package:storyteller/widgets/button/bottom_sheet_button.dart';
import 'package:storyteller/widgets/button/dislike_button.dart';
import 'package:storyteller/widgets/button/like_button.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/image/network_thumbnail.dart';
import 'package:storyteller/widgets/webview/custom_web_view.dart';

class PostBubble extends StatefulWidget {

  Post post;
  Function? refreshParent;
  EdgeInsets padding;
  bool isClickable;
  bool isFeed;

  PostBubble({
    Key? key,
    required this.post,
    this.refreshParent,
    this.padding = const EdgeInsets.only(left: 12, right: 12, top: 10,),
    this.isFeed = true,
    this.isClickable = true,
  });

  @override
  _PostBubbleState createState() => _PostBubbleState();
}

class _PostBubbleState extends State<PostBubble> {

  late Color _thisColor;
  late bool _isMyself;
  late Widget _profilePictureWidget;

  @override
  void initState() {
    _thisColor = utils.convertStrToColor(widget.post.content);
    _isMyself = widget.post.postId == qp.id!;
    _profilePictureWidget = CommonWidgets.forumProfilePicture(_isMyself, widget.post.creatorId);
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {

    return InkWell(
      splashColor: _thisColor,
      onTap: widget.isClickable ? () {
        getRoute.navigateTo(PostDetailPage(
          post: widget.post,
          refreshParent: refreshThisPage,
        ));
      } : null,
      onLongPress: !widget.isClickable ? null : () {
        utils.postLongPress(widget.post);
      },
      child: Padding(
        padding: widget.padding,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () {
                    if (utils.hasLoggedIn()) {
                      getRoute.navigateTo(ProfilePage(
                        userId: widget.post.creatorId,
                      ));
                    } else {
                      getSignUpRoutingSheet();
                    }
                  },
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      _profilePictureWidget,
                      Text(
                        userTag + widget.post.creatorUsername,
                      ),
                    ],
                  ),
                ),
                CommonWidgets.separationDot(),
                Icon(
                  MdiIcons.clockOutline,
                  size: 14,
                ),
                SizedBox(
                  width: 3,
                ),
                Tooltip(
                  message: timeUtils.getLocalDateTimeFromUTC(widget.post.createdAt.toUtc().toString()),
                  child: Text(
                    timeUtils.getTimeDiffString(timeUtils.getParsedDateTime(widget.post.createdAt.toUtc())),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 4,
            ),
            Flexible(
              child: Text(
                widget.post.title,
                style: Theme.of(context).textTheme.headline6,
                maxLines: widget.isFeed ? 3 : null,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            SizedBox(
              height: 7,
            ),
            Flexible(
              child: Text(
                widget.post.content,
                style: Theme.of(context).textTheme.subtitle1,
                maxLines: widget.isFeed ? 3 : null,
              ),
            ),
            StatefulBuilder(
              builder: (ctx, customState) {
                return Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    LikeButton(
                      isLiked: widget.post.likeIds.contains(qp.id!),
                      onPressed: () {
                        if (widget.post.dislikeIds.contains(qp.id!)) {
                          widget.post.dislikeIds.remove(qp.id!);
                        }
                        if (widget.post.likeIds.contains(qp.id!)) {
                          widget.post.likeIds.remove(qp.id!);
                          apiManager.likeDislikePost(widget.post.postId, true, revertAll: true);
                        } else {
                          widget.post.likeIds.add(qp.id!);
                          apiManager.likeDislikePost(widget.post.postId, true);
                        }
                        customState(() {});
                        if (widget.refreshParent != null) {
                          widget.refreshParent!();
                        }
                      },
                      isAnimation: widget.post.likeIds.contains(qp.id!),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      utils.shortStringForLongInt(widget.post.likeIds.length),
                      style: TextStyle(
                        color: mainColor,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    DislikeButton(
                      isDisliked: widget.post.dislikeIds.contains(qp.id!),
                      isAnimation: widget.post.dislikeIds.contains(qp.id!),
                      onPressed: () {
                        if (widget.post.likeIds.contains(qp.id!)) {
                          widget.post.likeIds.remove(qp.id!);
                        }
                        if (widget.post.dislikeIds.contains(qp.id!)) {
                          widget.post.dislikeIds.remove(qp.id!);
                          apiManager.likeDislikePost(widget.post.postId, false, revertAll: true);
                        } else {
                          widget.post.dislikeIds.add(qp.id!);
                          apiManager.likeDislikePost(widget.post.postId, false);
                        }
                        customState(() {});
                        if (widget.refreshParent != null) {
                          widget.refreshParent!();
                        }
                      },
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      utils.shortStringForLongInt(widget.post.dislikeIds.length),
                      style: TextStyle(
                        color: mainColor,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    CommonWidgets.separationDot(),
                    IconButton(
                      tooltip: "Reply Count",
                      icon: Icon(MdiIcons.commentOutline, color: unfocusedColor,),
                      onPressed: null,
                    ),
                    Text(
                      utils.shortStringForLongInt(widget.post.replyCount),
                      style: TextStyle(
                        color: mainColor,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    CommonWidgets.separationDot(),
                    IconButton(
                      tooltip: TextData.share,
                      onPressed: () async {
                        await utils.shareShortUrl("https://multiplayer.com/forum/?${PostCollection.postId}=${widget.post.postId}");
                      },
                      icon: Icon(
                        Icons.ios_share_outlined,
                        color: unfocusedColor,
                      ),
                    ),
                  ],
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}


