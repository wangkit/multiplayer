import 'package:animate_do/animate_do.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:storyteller/apps/home/profile/profile_page.dart';
import 'package:storyteller/apps/posts/post_detail_page.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/color/color.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:storyteller/utils/bottom_sheet/sign_up_routing_bottom_sheet.dart';
import 'package:storyteller/utils/dialog/block_dialog.dart';
import 'package:storyteller/utils/dialog/report_dialog.dart';
import 'package:storyteller/widgets/button/bottom_sheet_button.dart';
import 'package:storyteller/widgets/button/dislike_button.dart';
import 'package:storyteller/widgets/button/like_button.dart';
import 'package:storyteller/widgets/common/common_widgets.dart';
import 'package:storyteller/widgets/tag/tag.dart';
import 'package:storyteller/widgets/webview/custom_web_view.dart';

class ReplyBubble extends StatefulWidget {

  Post post;
  Reply reply;

  ReplyBubble({
    Key? key,
    required this.post,
    required this.reply,
  });

  @override
  _ReplyBubbleState createState() => _ReplyBubbleState();
}

class _ReplyBubbleState extends State<ReplyBubble> {

  late Color _thisColor;
  late bool _isMyself;
  late Widget _profilePictureWidget;

  @override
  void initState() {
    _thisColor = utils.convertStrToColor(widget.reply.content);
    _isMyself = widget.reply.creatorId == qp.id!;
    _profilePictureWidget = CommonWidgets.forumProfilePicture(_isMyself, widget.reply.creatorId);
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      width: deviceWidth,
      child: InkWell(
        splashColor: _thisColor,
        onLongPress: () {
          getGenericBottomSheet([
            BottomSheetButton(
              onTap: () {
                utils.copyToClipboard(widget.post.content);
                utils.toast(TextData.copiedSuccessfully);
              },
              iconData: Icons.copy_outlined,
              label: "Copy Content",
            ),
            _isMyself ? CommonWidgets.emptyBox() : BottomSheetButton(
              onTap: () {
                getReportDialog(UserProfile(
                  id: widget.post.creatorId,
                  username: widget.post.creatorUsername,
                ));
              },
              iconData: Icons.report_outlined,
              label: TextData.report,
            ),
            _isMyself ? CommonWidgets.emptyBox() : BottomSheetButton(
              onTap: () {
                getBlockDialog(UserProfile(
                  id: widget.post.creatorId,
                  username: widget.post.creatorUsername,
                ));
              },
              iconData: Icons.block_outlined,
              label: TextData.block,
            ),
          ]);
        },
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      if (utils.hasLoggedIn()) {
                        getRoute.navigateTo(ProfilePage(
                          userId: widget.reply.creatorId,
                        ));
                      } else {
                        getSignUpRoutingSheet();
                      }
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        _profilePictureWidget,
                        Text(
                          userTag + widget.reply.creatorUsername,
                        ),
                      ],
                    ),
                  ),
                  widget.post.creatorId == widget.reply.creatorId ? CommonWidgets.separationDot() : CommonWidgets.emptyBox(),
                  if (widget.post.creatorId == widget.reply.creatorId) Container(
                    decoration: BoxDecoration(
                      color: appIconColor,
                      borderRadius: BorderRadius.circular(commonRadius),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 6, ),
                      child: Text(
                        TextData.op,
                      ),
                    ),
                  ) else CommonWidgets.emptyBox(),
                  CommonWidgets.separationDot(),
                  Icon(
                    MdiIcons.clockOutline,
                    size: 14,
                  ),
                  SizedBox(
                    width: 3,
                  ),
                  Tooltip(
                    message: timeUtils.getLocalDateTimeFromUTC(widget.reply.createdAt.toUtc().toString()),
                    child: Text(
                      timeUtils.getTimeDiffString(timeUtils.getParsedDateTime(widget.reply.createdAt.toUtc())),
                      style: TextStyle(
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 7,
              ),
              Flexible(
                child: Text(
                  widget.reply.content,
                  style: Theme.of(context).textTheme.subtitle1,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


