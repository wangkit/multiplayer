import 'dart:convert';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:storyteller/apps/add_game/platform_page.dart';
import 'package:storyteller/apps/home/main/main_page.dart';
import 'package:storyteller/constants/const.dart';
import 'package:storyteller/resources/models/dbmodels.dart';
import 'package:storyteller/resources/models/models.dart';
import 'package:storyteller/resources/status/status_code.dart';
import 'package:storyteller/resources/values/analysis.dart';
import 'package:storyteller/resources/values/data_key.dart';
import 'package:storyteller/resources/values/pref_key.dart';
import 'package:storyteller/resources/values/social_key.dart';
import 'package:storyteller/resources/values/text.dart';
import 'package:storyteller/utils/dialog/username_dialog.dart';
import 'package:storyteller/utils/extensions/string_extensions.dart';
import 'package:uuid/uuid.dart';
import 'package:validators2/validators.dart';

class ApiManager {

  Future<void> deleteOneMessage(String messageId, String gameId) async {
    firestore.collection(FirestoreCollections.messages)
        .doc(gameId)
        .collection(FirestoreCollections.chats)
        .where(ChatCollection.messageId, isEqualTo: messageId)
        .get().then((result) {
          if (result.docs.isNotEmpty) {
            result.docs.forEach((messageToBeDeleted) {
              firestore.collection(FirestoreCollections.messages)
                  .doc(gameId)
                  .collection(FirestoreCollections.chats)
                  .doc(messageToBeDeleted.id)
                  .delete();
            });
          }
    });
  }

  Future<Game?> hasNoGameInThisPeriodOrIsBlocked(Game newGame) async {
    /// Firestore doesnt support multiple filters with inequality
    /// Find games that i have joined, which has not ended yet
    /// Check if the newGame.creatorId has blocked us or not
    QuerySnapshot blockedUs = await firestore.collection(FirestoreCollections.block)
        .where(BlockCollection.blockedId, isEqualTo: qp.id!)
        .where(BlockCollection.id, isEqualTo: newGame.creatorId)
        .get();
    if (blockedUs.size > 0) {
      return Game(players: [], gameId: '', platformProduct: '', endTime: DateTime.now(), playerIds: [], createdAt: DateTime.now(), startTime: DateTime.now(), maxPlayers: 0, gameName: '', gamePlatform: '', creatorId: '', creatorUsername: '');
    }
    QuerySnapshot joinedGames = await firestore.collection(FirestoreCollections.games)
        .where(GameCollection.endTime, isGreaterThanOrEqualTo: DateTime.now())
        .where(GameCollection.playerIds, arrayContains: qp.id!)
        .get();
    for (QueryDocumentSnapshot game in joinedGames.docs) {
      Game _joinedGame = Game.fromMapToGame(game.data() as Map);
      if ((newGame.startTime.isAfter(_joinedGame.startTime) && newGame.startTime.isBefore(_joinedGame.endTime)) || (newGame.endTime.isBefore(_joinedGame.endTime) && newGame.endTime.isAfter(_joinedGame.startTime)) || newGame.startTime.isBefore(_joinedGame.startTime) && newGame.endTime.isAfter(_joinedGame.endTime)) {
        return _joinedGame;
      }
    }
    return null;
  }

  Future<void> quitGame(String gameId) async {
    logAnalysis(Analysis.quitGame);
    await firestore.collection(FirestoreCollections.games).doc(gameId).update({
      GameCollection.players: FieldValue.arrayRemove([Game.fromProfileToMap(qp)]),
      GameCollection.playerIds: FieldValue.arrayRemove([qp.id!]),
    });
  }

  Future<void> joinGame(String gameId) async {
    logAnalysis(Analysis.joinGame);
    await firestore.collection(FirestoreCollections.games).doc(gameId).update({
      GameCollection.players: FieldValue.arrayUnion([Game.fromProfileToMap(qp)]),
      GameCollection.playerIds: FieldValue.arrayUnion([qp.id!]),
    });
  }

  Future<void> block(UserProfile profile) async {
    logAnalysis(Analysis.block);
    String _newBlockId = Uuid().v4();
    await firestore.collection(FirestoreCollections.block).doc(_newBlockId).set({
      BlockCollection.id: qp.id!,
      BlockCollection.blockId: _newBlockId,
      BlockCollection.blockedId: profile.id,
      BlockCollection.createdAt: DateTime.now(),
    });
    /// Remove blocked user from all your created game
    firestore.collection(FirestoreCollections.games)
    .where(GameCollection.playerIds, arrayContains: profile.id)
    .where(GameCollection.creatorId, isEqualTo: qp.id!)
    .get().then((QuerySnapshot snapshot) {
      if (snapshot.size > 0) {
        snapshot.docs.forEach((DocumentSnapshot doc) {

          Game _thisGame = Game.fromMapToGame(doc.data() as Map);

          firestore.collection(FirestoreCollections.games).doc(_thisGame.gameId).update({
            GameCollection.players: FieldValue.arrayRemove([Game.fromProfileToMap(profile)]),
            GameCollection.playerIds: FieldValue.arrayRemove([profile.id]),
          });
        });
      }
    });
  }

  Future<void> updateGameId(String target, String newValue) async {
    logAnalysis(Analysis.updateGameId + target);
    await firestore.collection(FirestoreCollections.users).doc(qp.id!).update({
      target: newValue,
    });
  }

  Future<void> removeCover(String url, String gameId) async {
    logAnalysis(Analysis.updateCover);
    await firestore.collection(FirestoreCollections.games).doc(gameId).update({
      GameCollection.gameCover: FieldValue.arrayRemove([url]),
    });
  }

  Future<void> updateCover(String newUrl, String gameId) async {
    logAnalysis(Analysis.updateCover);
    await firestore.collection(FirestoreCollections.games).doc(gameId).update({
      GameCollection.gameCover: FieldValue.arrayUnion([newUrl]),
    });
  }

  Future<void> report(UserProfile profile, String reason) async {
    logAnalysis(Analysis.report);
    String _newReportId = Uuid().v4();
    await firestore.collection(FirestoreCollections.report).doc(_newReportId).set({
      ReportCollection.id: qp.id!,
      ReportCollection.reportId: _newReportId,
      ReportCollection.reportedId: profile.id,
      ReportCollection.reason: reason,
      ReportCollection.createdAt: DateTime.now(),
    });
  }

  Future<String> checkGameName(String gameName) async {
    final url = apiBaseUrl! + "/searchGame?name=$gameName";

    return http.get(Uri.parse(url)).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        if (jsonResponse.length > 0) {
          return jsonResponse[0]["name"] as String;
        }
        return "";
      }
      return "";
    }).catchError((onError) {
      debugPrint("Check game error: $onError");
      return "";
    });
  }

  Future<void> likeDislikePost(String targetPostId, bool isLike, {bool revertAll = false}) async {
    logAnalysis(revertAll ? Analysis.revertLikes : isLike ? Analysis.likePost : Analysis.dislikePost);
    if (revertAll) {
      /// Revert all likes and dislikes of this post
      firestore.collection(FirestoreCollections.posts).doc(targetPostId).update({
        PostCollection.likeIds: FieldValue.arrayRemove([qp.id!]),
        PostCollection.dislikeIds: FieldValue.arrayRemove([qp.id!]),
      });
      await firestore.collection(FirestoreCollections.postLikes).doc(targetPostId + qp.id!).delete();
      return;
    }
    if (isLike) {
      firestore.collection(FirestoreCollections.posts).doc(targetPostId).update({
        PostCollection.likeIds: FieldValue.arrayUnion([qp.id!]),
        PostCollection.dislikeIds: FieldValue.arrayRemove([qp.id!]),
      });
    } else {
      firestore.collection(FirestoreCollections.posts).doc(targetPostId).update({
        PostCollection.likeIds: FieldValue.arrayRemove([qp.id!]),
        PostCollection.dislikeIds: FieldValue.arrayUnion([qp.id!]),
      });
    }
    await firestore.collection(FirestoreCollections.postLikes).doc(targetPostId + qp.id!).set({
      PostLikesCollection.firebaseUserId: firebaseUser!.uid,
      PostLikesCollection.isLiked: isLike,
      PostLikesCollection.createdAt: DateTime.now(),
      PostLikesCollection.postId: targetPostId,
    });
  }

  Future<UserProfile?> getUserProfile(String userId) async {
    DocumentSnapshot? data = await firestore.collection(FirestoreCollections.users)
        .doc(userId).get();
    if (data.exists) {
      return UserProfile.fromMapToProfile(data.data() as Map);
    }
    return null;
  }

  Future<void> createReply(Reply newReply) async {
    logAnalysis(Analysis.createReply);
    firestore.collection(FirestoreCollections.posts)
        .doc(newReply.postId).update({
      PostCollection.replyCount: FieldValue.increment(1),
    });
    await firestore.collection(FirestoreCollections.posts)
        .doc(newReply.postId)
        .collection(FirestoreCollections.replies)
        .doc(newReply.replyId)
        .set({
      ReplyCollection.firebaseUserId: firebaseUser!.uid,
      ReplyCollection.creatorId: newReply.creatorId,
      ReplyCollection.creatorUsername: newReply.creatorUsername,
      ReplyCollection.createdAt: newReply.createdAt,
      ReplyCollection.postId: newReply.postId,
      ReplyCollection.content: newReply.content,
      ReplyCollection.replyId: newReply.replyId,
    });
  }

  Future<void> createPost(Post newPost) async {
    logAnalysis(Analysis.createPost);
    await firestore.collection(FirestoreCollections.posts).doc(newPost.postId).set({
      PostCollection.firebaseUserId: firebaseUser!.uid,
      PostCollection.creatorId: newPost.creatorId,
      PostCollection.creatorUsername: newPost.creatorUsername,
      PostCollection.createdAt: newPost.createdAt,
      PostCollection.postId: newPost.postId,
      PostCollection.title: newPost.title,
      PostCollection.content: newPost.content,
      PostCollection.likeIds: newPost.likeIds,
      PostCollection.dislikeIds: newPost.dislikeIds,
      PostCollection.replyCount: newPost.replyCount
    });
  }

  Future<void> createGame(Game newGame) async {
    logAnalysis(Analysis.createGame);
    await firestore.collection(FirestoreCollections.games).doc(newGame.gameId).set({
      GameCollection.firebaseUserId: firebaseUser!.uid,
      GameCollection.creatorId: newGame.creatorId,
      GameCollection.creatorUsername: newGame.creatorUsername,
      GameCollection.createdAt: newGame.createdAt,
      GameCollection.gameId: newGame.gameId,
      GameCollection.gameName: newGame.gameName,
      GameCollection.gamePlatform: newGame.gamePlatform,
      GameCollection.platformProduct: newGame.platformProduct,
      GameCollection.maxPlayers: newGame.maxPlayers,
      GameCollection.endTime: newGame.endTime,
      GameCollection.startTime: newGame.startTime,
      GameCollection.players: newGame.players,
      GameCollection.playerIds: newGame.playerIds,
      GameCollection.gameCover: newGame.gameCover,
      GameCollection.gameTheme: newGame.gameTheme,
      GameCollection.gameGenre: newGame.gameGenre,
    });
  }

  Future<void> signIn(String email, String password, {String social = ""}) async {
    utils.loadToast(msg: TextData.signingIn);
    await firestore
        .collection(FirestoreCollections.users)
        .where(UserCollection.email, isEqualTo: email)
        .where(UserCollection.password, isEqualTo: commonUtils.encodeString(password))
        .where(UserCollection.social, isEqualTo: social)
        .get()
        .then((QuerySnapshot userResult) async {
          if (userResult.size > 0) {
            userResult.docs.forEach((QueryDocumentSnapshot myProfile) async {
              Map temp = myProfile.data() as Map;
              qp.email = temp[UserCollection.email];
              qp.id = temp[UserCollection.id];
              qp.authPw = temp[UserCollection.authPw];
              qp.username = temp[UserCollection.username];
              qp.photoUrl = temp[UserCollection.photoUrl];
              qp.psn = temp[UserCollection.psn];
              qp.xboxGamerTag = temp[UserCollection.xboxGamerTag];
              qp.nintendoFriendCode = temp[UserCollection.nintendoFriendCode];
              qp.steamFriendCode = temp[UserCollection.steamFriendCode];
              qp.createdAt = timeUtils.getDateTimeFromTimeStamp(temp[UserCollection.createdAt]);
              utils.saveStr(PrefKey.email, qp.email!);
              utils.saveStr(PrefKey.id, qp.id!);
              utils.saveStr(PrefKey.authPwKey, qp.authPw!);
              utils.saveStr(PrefKey.username, qp.username!);
              await signInToFirebase();
              logAnalysis(Analysis.signIn);
              getRoute.pop();
              getRoute.navigateToAndPopAll(MainPage());
              utils.toast(TextData.signInSuccessfully + qp.username!);
            });
          } else {
            logAnalysis(Analysis.signInFail);
            getRoute.pop();
            utils.toast(TextData.credentialFail, isWarning: true);
          }
    });
  }

  Future<void> signInUpWithApple() async {
    try {
      AuthorizationCredentialAppleID credential = await SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
      );
      if (credential.userIdentifier != null && credential.userIdentifier!.isNotEmpty) {

        String _id = credential.userIdentifier!;

        utils.loadToast();

        /// Find this account
        QuerySnapshot _result = await firestore.collection(FirestoreCollections.users)
            .where(UserCollection.social, isEqualTo: SocialKey.apple)
            .where(UserCollection.password, isEqualTo: commonUtils.encodeString(_id))
            .get();

        getRoute.pop();

        if (_result.size > 0) {
          /// Login with apple
          apiManager.logAnalysis(Analysis.signInApple);
          await apiManager.signIn(utils.getAppleEmail(_id), _id, social: SocialKey.apple);
          /// Pop loading
          debugPrint("Sign in with apple");
        } else {
          /// Pop loading
          getRoute.pop();

          var _usernameResult = await getUsernameDialog(credential.givenName != null ? credential.givenName!.replaceAll(" ", "") : "");

          String? _username = _usernameResult[0];
          bool _submitted = _usernameResult[1];

          if (_submitted && _username != null && _username.isNotEmpty) {
            debugPrint("create account in with apple");
            /// Create account with apple
            apiManager.logAnalysis(Analysis.signUpApple);
            /// Do not encode password here, it will be handled underneath
            await apiManager.createAccount(_username, utils.getAppleEmail(_id), _id, social: SocialKey.apple);
          } else {
            utils.toast(TextData.loginSocialFail, isWarning: true);
          }
        }
      } else {
        utils.toast(TextData.loginSocialFail, isWarning: true);
      }
    } on Exception catch (e) {
      utils.toast(TextData.loginSocialFail, isWarning: true);
    }
  }

  Future<dynamic> getFacebookUserDetails(String token) async {
    return await FacebookAuth.instance.getUserData(
      fields: "first_name,email,picture.width(200)",
    );

    /// Outdated
    // final graphResponse = await http.get(Uri.parse('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${token}'));
    // return json.decode(graphResponse.body);
  }

  Future<void> createAccount(String username, String email, String password, {String social = "", String profilePicUrl = defaultProfilePictureLink}) async {
    /// Create firebase auth first
    utils.loadToast(msg: TextData.signingUp);
    String authPw = utils.generatePassword();
    if (isEmail(email)) {
      await firestore
          .collection(FirestoreCollections.users)
          .where(UserCollection.username, isEqualTo: username)
          .get()
          .then((QuerySnapshot usernameResult) async {
        if (usernameResult.size > 0) {
          debugPrint("Already exist username");
          getRoute.pop();
          utils.toast(TextData.duplicateUsername, isWarning: true);
        } else {
          await firestore
              .collection(FirestoreCollections.users)
              .where(UserCollection.email, isEqualTo: email)
              .get()
              .then((QuerySnapshot emailResult) async {
            if (emailResult.size > 0) {
              debugPrint("Already exist email");
              getRoute.pop();
              utils.toast(TextData.duplicateEmail, isWarning: true);
            } else {
              bool success = await createFirebaseAccount(email, authPw);
              if (success) {
                debugPrint('No such user. Creating new...');
                DeviceInfoPlugin deviceInfoPlug = DeviceInfoPlugin();
                String deviceName, deviceVersion, deviceId;
                if (kIsWeb) {
                  deviceName = "Browser";
                  deviceVersion = "";
                  deviceId = "";
                } else {
                  if (Platform.isAndroid) {
                    final androidInfo = await deviceInfoPlug.androidInfo;
                    deviceName = androidInfo.model!;
                    deviceVersion = androidInfo.version.release.toString();
                    deviceId = androidInfo.androidId!;
                  } else {
                    final iosInfo = await deviceInfoPlug.iosInfo;
                    deviceName = iosInfo.name!;
                    deviceVersion = iosInfo.systemVersion!;
                    deviceId = iosInfo.identifierForVendor!;
                  }
                }
                String _newId = Uuid().v4();
                DateTime _now = DateTime.now().toUtc();
                logAnalysis(Analysis.signUp);
                await firestore.collection(FirestoreCollections.users).doc(_newId).set({
                  UserCollection.id: _newId,
                  UserCollection.firebaseUserId: firebaseUser!.uid,
                  UserCollection.authPw: authPw,
                  UserCollection.password: commonUtils.encodeString(password),
                  UserCollection.email: email,
                  UserCollection.createdAt: _now,
                  UserCollection.username: username,
                  UserCollection.deviceId: deviceId,
                  UserCollection.deviceVersion: deviceVersion,
                  UserCollection.deviceModel: deviceName,
                  UserCollection.photoUrl: profilePicUrl,
                  UserCollection.nintendoFriendCode: "",
                  UserCollection.psn: "",
                  UserCollection.steamFriendCode: "",
                  UserCollection.xboxGamerTag: "",
                  UserCollection.social: social,
                });
                qp.id = _newId;
                qp.email = email;
                qp.authPw = authPw;
                qp.username = username;
                qp.photoUrl = profilePicUrl;
                qp.xboxGamerTag = "";
                qp.steamFriendCode = "";
                qp.psn = "";
                qp.nintendoFriendCode = "";
                qp.createdAt = _now;
                utils.saveStr(PrefKey.id, qp.id!);
                utils.saveStr(PrefKey.username, qp.username!);
                utils.saveStr(PrefKey.email, qp.email!);
                utils.saveStr(PrefKey.authPwKey, qp.authPw!);
                await apiManager.signInToFirebase();
                getRoute.pop();
                getRoute.navigateToAndPopAll(MainPage());
                getRoute.navigateTo(PlatformPage(isNewUser: true,));
                utils.toast(TextData.signSuccessfully);
              }
            }
          });
        }
      });
    }
  }

  Future<void> signInUpByGoogle() async {

    GoogleSignIn _googleSignIn = GoogleSignIn(
      scopes: [
        'email',
      ],
    );

    /// Make sure we have signed out every time we sign in / up
    if (await _googleSignIn.isSignedIn()) {
      _googleSignIn.signOut();
    }

    GoogleSignInAccount? _googleAccount = await _googleSignIn.signIn();
    if (_googleAccount != null) {

      utils.loadToast();

      String _id = _googleAccount.id;
      String? _name = _googleAccount.displayName;
      String _email = _googleAccount.email;
      String _photo = defaultProfilePictureLink;

      if (_googleAccount.photoUrl != null && _googleAccount.photoUrl!.isNotEmpty) {
        _photo = _googleAccount.photoUrl!;
      }

      QuerySnapshot _result = await firestore.collection(FirestoreCollections.users)
          .where(UserCollection.social, isEqualTo: SocialKey.google)
          .where(UserCollection.password, isEqualTo: commonUtils.encodeString(_id))
          .get();

      getRoute.pop();
      if (_result.size > 0) {
        /// Account found, login by google
        apiManager.logAnalysis(Analysis.signInGoogle);
        await apiManager.signIn(_email, _id, social: SocialKey.google);
        return;
      } else {

        /// Account not found, sign up by google

        var _usernameResult = await getUsernameDialog(_name == null ? "": _name.replaceAll(" ", ""));

        String? _username = _usernameResult[0];
        bool _submitted = _usernameResult[1];

        if (_submitted && _username != null && _username.isNotEmpty) {
          apiManager.logAnalysis(Analysis.signUpGoogle);
          await apiManager.createAccount(_username, _email, _id, social: SocialKey.google, profilePicUrl: _photo);
          return;
        }
        utils.toast(TextData.loginSocialFail, isWarning: true);
      }
    } else {
      utils.toast(TextData.loginSocialFail, isWarning: true);
    }
  }

  Future<void> signInUpByFacebook() async {
    /// Begin facebook login
    getRoute.pop();

    /// Make sure logged out every time we try to sign in / up
    await FacebookAuth.instance.logOut();

    LoginResult result = await FacebookAuth.instance.login(
      permissions: ['email', 'public_profile'],
    );

    if (result.accessToken != null && result.status == LoginStatus.success) {
      /// Get user details from facebook
      utils.loadToast();
      Map jsonResponse = await apiManager.getFacebookUserDetails(result.accessToken!.token);

      if (jsonResponse.isNotEmpty) {
        try {
          /// Set variables
          String? _firstName;
          String _id = TextData.facebook + jsonResponse["id"];
          String _email = jsonResponse["email"];

          QuerySnapshot _existingUser = await firestore.collection(FirestoreCollections.users)
              .where(UserCollection.email, isEqualTo: _email)
              .where(UserCollection.social, isEqualTo: SocialKey.facebook)
              .get();

          if (_existingUser.size > 0) {
            debugPrint("Found facebook user, sign in");
            /// This facebook user has already registered, bring him in now
            logAnalysis(Analysis.signInFacebook);
            await apiManager.signIn(_email, _email, social: SocialKey.facebook);
            getRoute.pop();
            return;
          }
          getRoute.pop();

          if (jsonResponse["first_name"] != null) {
            _firstName = jsonResponse["first_name"].toString().replaceAll(" ", "");
          }

          /// Ask user what username he wants because can't change afterwards
          var _usernameResults = await getUsernameDialog(_firstName == null ? "" : _firstName);

          String? _username = _usernameResults[0] ?? _id;
          bool _submitted = _usernameResults[1];

          if (_submitted && _username != null && _username.isNotEmpty) {
            logAnalysis(Analysis.signUpFacebook);

            /// Facebook sign in might contain a profile pic for this user already
            String _url = defaultProfilePictureLink;
            try {
              if (jsonResponse.containsKey("picture")) {
                Map _picture = jsonResponse["picture"];
                if (_picture.containsKey("data")) {
                  Map _pictureData = _picture["data"];
                  if (_pictureData.containsKey("url")) {
                    _url = _pictureData["url"];
                  }
                }
              }
            } on Exception catch (e) {
              _url = defaultProfilePictureLink;
            }

            /// Do not use id for password since ios and android facebook have different id
            await apiManager.createAccount(_username, _email, _email, social: SocialKey.facebook, profilePicUrl: _url);
            return;
          }
        } on Exception catch (e) {
          utils.toast(TextData.loginSocialFail, isWarning: true);
        }
      }
    }
    utils.toast(TextData.loginSocialFail, isWarning: true);
  }

  Future<void> updateProfilePicture(File targetImage, String targetId) async {
    try {
      TaskSnapshot _uploadTask = await FirebaseStorage.instance
          .ref('profile/${path.basename(targetImage.path)}')
          .putFile(targetImage);
      String _newPhotoUrl = await _uploadTask.ref.getDownloadURL();
      await firestore.collection(FirestoreCollections.users).doc(targetId).update({
        UserCollection.photoUrl: _newPhotoUrl,
      });
      qp.photoUrl = _newPhotoUrl;
    } on FirebaseException catch (e) {
      debugPrint("Firebase exception while uploading: $e");
    }
  }

  Future<void> signInToFirebase({String? email, String? password}) async {
    try {
      firebaseUser = (await firebaseAuth.signInWithEmailAndPassword(
        email: email != null ? email : qp.email!,
        password: password != null ? password : qp.authPw!,
      )).user;
      if (firebaseUser != null) {
        await setFcmToken();
      }
    } on Exception catch (e) {
      debugPrint("catch: $e");
    }
  }

  Future<void> setFcmToken() async {
    if (!hasCalledFCMRegistration) {
      firebaseMessaging!.getToken().then((String? token) async {
        if (!token!.isNullOrEmpty) {
          DocumentSnapshot existingFcm = await firestore.collection(FirestoreCollections.fcmToken).doc(qp.id!).get();
          if (existingFcm.exists) {
            firestore.collection(FirestoreCollections.fcmToken).doc(qp.id!).update({
              FcmTokenCollection.token: token,
              FcmTokenCollection.updatedAt: DateTime.now().toUtc(),
            });
          } else {
            firestore.collection(FirestoreCollections.fcmToken).doc(qp.id!).set({
              FcmTokenCollection.id: qp.id!,
              FcmTokenCollection.email: qp.email!,
              FcmTokenCollection.token: token,
              FcmTokenCollection.createdAt: DateTime.now().toUtc(),
              FcmTokenCollection.updatedAt: DateTime.now().toUtc(),
            });
          }
          hasCalledFCMRegistration = true;
          utils.saveBoo(PrefKey.hasCalledFCMRegistration, hasCalledFCMRegistration);
        }
      });
    }
  }

  Future<bool> createFirebaseAccount(String email, String password) async {
    try {
      firebaseUser = (await firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      )).user;
      return true;
    } on Exception catch (e) {
      debugPrint("Create firebase error: $e");
      getRoute.pop();
      utils.toast(TextData.duplicateEmail, isWarning: true);
      return false;
    }
  }

  Future<void> logAnalysis(String name) async {
    analytics.logEvent(
      name: name.trim(),
    );
  }

}