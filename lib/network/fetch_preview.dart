import 'package:flutter/cupertino.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart';
import 'package:storyteller/resources/values/fetch_link.dart';
import 'package:storyteller/utils/extensions/string_extensions.dart';

class FetchPreview {
  Future fetch(String url) async {
    try {
      if (url.isUrl) {
        url = url.startsWith("www.") ? "https://" + url : url;
        final client = Client();
        final response = await client.get(Uri.parse(url));
        final document = parse(response.body);
        String? image, appleIcon, favIcon;

        var elements = document.getElementsByTagName('meta');
        final linkElements = document.getElementsByTagName('link');

        if (elements.length > 0) {
          elements.forEach((tmp) {

            //fetch image
            if (tmp.attributes['property'] == 'og:image') {
              image = tmp.attributes['content'];
            }
            if (image == null || image!.isEmpty) {
              var imgElements = document.getElementsByTagName('img');
              if (imgElements != null && imgElements.isNotEmpty) {
                image = imgElements.first.attributes['src'];
                if (image != null) {
                  if (!image!.contains("https://") || !image!.contains("www.")) {
                    image = url + image!;
                  }
                }
              }
            }
          });
        }

        if (linkElements.length > 0) {
          linkElements.forEach((tmp) {
            if (tmp.attributes['rel'] == 'apple-touch-icon') {
              appleIcon = tmp.attributes['href'];
            }
            if (tmp.attributes['rel']?.contains('icon') == true) {
              favIcon = tmp.attributes['href'];
            }
          });
        }

        Map<String, String> metadataMap = {
          FetchLink.image: image ?? '',
          FetchLink.appleIcon: appleIcon ?? '',
          FetchLink.favIcon: favIcon ?? ''
        };
        return metadataMap;
      } else {
        return null;
      }
    } on Exception catch (e) {
      debugPrint("Fetch link exception: $e");
    }
  }
}