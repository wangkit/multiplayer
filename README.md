Running the app in different environments
Like that Seth Ladd guy answered in the StackOverflow question, we can run the different variants by running flutter run with the --target or -t argument for short.

So, in our case:

to run the development build, we call flutter run -t lib/init_dev.dart
to run the production build, we call flutter run -t lib/init_prod.dart
To create a release build on Android, we can run flu2tter build apk -t lib/init_<environment>.dart and we will get the correct APK for our environment. To do a release build on iOS, just replace apk with ios.

While this is pretty simple, wouldn’t it be nice if there’s some IDE option to toggle between different variants?

-Build ios-
dev: flutter build ios -t lib/apps/init_dev.dart --no-sound-null-safety
prod: flutter build ios -t lib/apps/init_prod.dart --no-sound-null-safety

-Clean xcode-
flutter clean && rm ios/Podfile.lock pubspec.lock && rm -rf ios/Pods ios/Runner.xcworkspace

-Build apk-
dev: flutter build apk -t lib/apps/init_dev.dart --no-sound-null-safety
prod: flutter build apk -t lib/apps/init_prod.dart --no-sound-null-safety

-Build web-
dev: flutter build web -t lib/web/init.dart --no-sound-null-safety
prod: flutter build web -t lib/web/init.dart --no-sound-null-safety

-Build release app-
dev: flutter run -t lib/apps/init_dev.dart --release --no-sound-null-safety
prod: flutter run -t lib/apps/init_prod.dart --release --no-sound-null-safety

-Build app bundle-
dev: flutter build appbundle --target-platform android-arm,android-arm64,android-x64 -t lib/apps/init_dev.dart --no-sound-null-safety
prod: flutter build appbundle --target-platform android-arm,android-arm64,android-x64 -t lib/apps/init_prod.dart --no-sound-null-safety

-Deploy web app-
firebase deploy --only hosting

-Facebook login hash-
echo -n sha-key | openssl dgst -binary -sha1 | openssl base64

