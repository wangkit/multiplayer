importScripts("https://www.gstatic.com/firebasejs/7.20.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/7.20.0/firebase-messaging.js");

//Using singleton breaks instantiating messaging()
// App firebase = FirebaseWeb.instance.app;


firebase.initializeApp({
  apiKey: "AIzaSyAIaDL4Wa94KgogDMR7oYHcjKfX2-eB78E",
  authDomain: "story-teller-3124b.firebaseapp.com",
  projectId: "story-teller-3124b",
  storageBucket: "story-teller-3124b.appspot.com",
  messagingSenderId: "833584003290",
  appId: "1:833584003290:web:1ee3f26f9ff124c6bc4de3",
  measurementId: "G-X062ZFYS15"
});

const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
    const promiseChain = clients
        .matchAll({
            type: "window",
            includeUncontrolled: true
        })
        .then(windowClients => {
            for (let i = 0; i < windowClients.length; i++) {
                const windowClient = windowClients[i];
                windowClient.postMessage(payload);
            }
        })
        .then(() => {
            return registration.showNotification("New Message");
        });
    return promiseChain;
});
self.addEventListener('notificationclick', function (event) {
    console.log('notification received: ', event)
});